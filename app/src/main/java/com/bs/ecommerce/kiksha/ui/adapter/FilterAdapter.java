package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.FilterItem;
import com.bs.ecommerce.kiksha.model.FilterOptions;
import com.bs.ecommerce.kiksha.model.FilterRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ashraful on 5/11/2016.
 */
public class FilterAdapter extends BaseExpandableListAdapter {

    List<FilterItem>filterItemList;
    Context context;
    HashMap<String,String>filterValuNameMap;
     HashMap<String,Boolean>checkedMap;

    public FilterAdapter(Context context, List<FilterItem>filterItemList)
    {
        this.context=context;
        this.filterItemList=filterItemList;
        this.filterValuNameMap=new HashMap<>();
        this.checkedMap=new HashMap<>();
    }

    @Override
    public int getGroupCount() {
        return filterItemList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return filterItemList.get(groupPosition).getOptions().size();
    }

    @Override
    public FilterItem getGroup(int groupPosition) {
        return filterItemList.get(groupPosition);
    }

    @Override
    public FilterOptions getChild(int groupPosition, int childPosition) {
        return filterItemList.get(groupPosition).getOptions().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        convertView = (LayoutInflater.from(context)).inflate(R.layout.item_filter_expandable_group, parent, false);
        TextView  filterItemNameTextView = (TextView)convertView.findViewById(R.id.textView_name);
        ImageView imageView=(ImageView)convertView.findViewById(R.id.expandableIcon);

        FilterItem current=getGroup(groupPosition);
        filterItemNameTextView.setText(current.getLabel());
        if(getChildrenCount(groupPosition)<1)
        {
            imageView.setVisibility(View.INVISIBLE);
            //onCategoryItemClick(current,textView_catName);
        }
        else {
            imageView.setVisibility(View.VISIBLE);
            if (isExpanded)
                imageView.setImageResource(R.drawable.ic_chevron_up);
            else
                imageView.setImageResource(R.drawable.ic_chevron_down);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        convertView = (LayoutInflater.from(context)).inflate(R.layout.item_filter_expandable_child, parent, false);
        CheckedTextView filterOptionTextView = (CheckedTextView)convertView.findViewById(R.id.textView_name);
        FilterOptions current = getChild(groupPosition, childPosition);
        filterOptionTextView.setText(current.getLabel());
        Boolean boolVal=checkedMap.get(current.getValue());
        if(boolVal!=null && boolVal)
            filterOptionTextView.setChecked(true);
        onFilterItemoptionClick(current,filterOptionTextView, groupPosition);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private void onFilterItemoptionClick(final FilterOptions filterOptions, final CheckedTextView textView, final int groupPosition)
    {

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textView.isChecked())
                {
                    textView.setChecked(false);
                    checkedMap.put(filterOptions.getValue(),false);
                    filterValuNameMap.remove(filterOptions.getValue());

                }
                else
                {
                    textView.setChecked(true);
                    checkedMap.put(filterOptions.getValue(),true);
                    filterValuNameMap.put(filterOptions.getValue(),getGroup(groupPosition).getName());

                }
             //   EventBus.getDefault().post(filterOptions);
            }
        });
    }

    public List<FilterRequest> getFilterList()
    {
        List<FilterRequest> filterRequestList=new ArrayList<>();
        for(String Value:filterValuNameMap.keySet())
        {
            FilterRequest filterRequest=new FilterRequest();
            filterRequest.setValue(Value);
            filterRequest.setName(filterValuNameMap.get(Value));
            filterRequestList.add(filterRequest);
        }
        return filterRequestList;
    }

    public void resetMapValue()
    {
        filterValuNameMap.clear();
        checkedMap.clear();
    }
}

package com.bs.ecommerce.kiksha.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.Address;
import com.bs.ecommerce.kiksha.model.AddressListResponse;
import com.bs.ecommerce.kiksha.model.AddressListResponseS;
import com.bs.ecommerce.kiksha.model.AllShippingMethodResponse;
import com.bs.ecommerce.kiksha.model.CartId;
import com.bs.ecommerce.kiksha.model.City;
import com.bs.ecommerce.kiksha.model.CityResponse;
import com.bs.ecommerce.kiksha.model.Country;
import com.bs.ecommerce.kiksha.model.CountryResponse;
import com.bs.ecommerce.kiksha.model.MessageResponse;
import com.bs.ecommerce.kiksha.model.SetAddressRequest;
import com.bs.ecommerce.kiksha.model.ShippingMethod;
import com.bs.ecommerce.kiksha.model.ShippingMethodSetRequest;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.view.FormViews;

import org.apache.commons.lang.WordUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_billing_address)
public class BillingAddressActivity extends BaseActivity implements AdapterView.OnItemSelectedListener,
        CompoundButton.OnCheckedChangeListener {


    @InjectView(R.id.sp_addresses_s)
    Spinner addressesSpinner_s;

    @InjectView(R.id.et_first_name_s)
    EditText firstNameEt_s;

    @InjectView(R.id.et_last_name_s)
    EditText lastNameEt_s;

    @InjectView(R.id.et_email_s)
    EditText emailEt_s;

    @InjectView(R.id.et_address_1_s)
    EditText address1Et_s;

    /*@InjectView(R.id.et_address_2)
    EditText address2Et;*/

    @InjectView(R.id.sp_country_s)
    Spinner countrySp_s;

    @InjectView(R.id.et_city_s)
    Spinner cityEt_s;

    @InjectView(R.id.et_area_s)
    Spinner spArea_s;

    @InjectView(R.id.et_postal_code_s)
    EditText postalCodeEt_s;

    @InjectView(R.id.et_telephone_s)
    EditText telephoneEt_s;
    //---------------------------

    @InjectView(R.id.sp_addresses)
    Spinner addressesSpinner;

    @InjectView(R.id.et_first_name)
    EditText firstNameEt;

    @InjectView(R.id.et_last_name)
    EditText lastNameEt;

    @InjectView(R.id.et_email)
    EditText emailEt;

    @InjectView(R.id.cb_create_account)
    CheckBox createAccountCb;

    @InjectView(R.id.ll_password_area)
    LinearLayout passwordAreaLl;

    @InjectView(R.id.et_password)
    EditText passwordEt;

    @InjectView(R.id.et_confirm_password)
    EditText confirmPasswordEt;

    @InjectView(R.id.et_address_1)
    EditText address1Et;

    /*@InjectView(R.id.et_address_2)
    EditText address2Et;*/

    @InjectView(R.id.sp_country)
    Spinner countrySp;

    @InjectView(R.id.et_city)
    Spinner cityEt;

    @InjectView(R.id.et_area)
    Spinner spArea;

    @InjectView(R.id.et_postal_code)
    EditText postalCodeEt;

    @InjectView(R.id.et_telephone)
    EditText telephoneEt;

    @InjectView(R.id.cb_shipment)
    CheckBox shipmentCb;

    @InjectView(R.id.btn_continue)
    Button continueBtn;
    //===================================
    //
    //===================================
    @InjectView(R.id.shipping_layout)
    View shipping_layout;


    private List<Address> addressListsS;
    private List<Address> addressLists;
    private int defaultAddressPosition = 0;
    private ArrayAdapter countryAdapter;
    private ArrayAdapter cityAdapter;
    private ArrayAdapter areaAdapter;
    private ArrayAdapter countryAdapterS;
    private ArrayAdapter cityAdapterS;
    private ArrayAdapter areaAdapterS;
    private List<Country> countryList;
    private List<String> countryListST;
    private List<String> areaList;
    private List<String> areaListST;
    private List<City> cityList;
    private List<String> cityListST;
    private Address address_billing;
    //private Address address_billingS;
    private int city_cl = 0;
    private List<ShippingMethod> dataShippingMethod;
    private String price;
    private Address address_shipping;
    ShippingMethodSetRequest shippingMethodSetRequest;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        callCountryListApi();
        callCityListApi();
        callAddressListApi();
        getShippingMethodApi();
        setToolbarTitle("Billing Address");
        createAccountCb.setOnCheckedChangeListener(this);
        createAccountCb.setVisibility(View.GONE);
        setPasswordFieldVisibility();
        shipping_layout.setVisibility(View.GONE);
        //---------------------------------------
        shipmentCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button,
                                         boolean isChecked) {
                if (isChecked) {
                    shipping_layout.setVisibility(View.GONE);
                } else {
                    callAddressListApi_s();
                    shipping_layout.setVisibility(View.VISIBLE);
                }
            }
        });


        //---------------------------------------

    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        continueBtn.setOnClickListener(this);
    }

    private CartId getCartIdObject(int cartId) {
        CartId cartIdRequest = new CartId();
        cartIdRequest.setCartId(cartId);
        return cartIdRequest;
    }

    private void callCountryListApi() {
        NetworkServiceHandler.processCallBack(RestClient.get().getCountryListForDropdown(), this);
    }


    private void callCityListApi() {
        NetworkServiceHandler.processCallBack(RestClient.get().getCityForDropDown(""), this);
    }

    private void callAddressListApi() {
        if (isLoggedIn()) {
            addressesSpinner.setVisibility(View.VISIBLE);
            String customerId = preferenceService.GetPreferenceValue(PreferenceService.ENTITY_KEY);
            Call<AddressListResponse> callback = RestClient.get().getAddressList(customerId, "");
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            addressesSpinner.setVisibility(View.GONE);
        }

    }

    private void getShippingMethodApi() {
        int cartId = getCartId();
        if (cartId != -1) {
            Call<AllShippingMethodResponse> callback = RestClient.get().getAllShippingMethod(getCartIdObject(cartId)); // changed
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }
    }

    public void onEvent(CountryResponse countryResponse) {
        countryList = new ArrayList<>();

        if (countryResponse.getResponse_code() == 100) {
            countryList = countryResponse.getData();
            countryListST = countryResponse.getStringList();
            countryAdapter = new ArrayAdapter
                    (this, android.R.layout.simple_spinner_item, countryListST);

            countryAdapter.setDropDownViewResource
                    (android.R.layout.simple_spinner_dropdown_item);

            countrySp.setAdapter(countryAdapter);
        }

    }

    public void onEvent(CityResponse countryResponse) {
        cityList = new ArrayList<>();
        Log.d("respose_f", "==" + countryResponse.toString());
        if (countryResponse.getResponse_code() == 100) {
            cityList = countryResponse.getData();
            cityListST = countryResponse.getStringList();
            cityAdapter = new ArrayAdapter
                    (this, android.R.layout.simple_spinner_item, cityListST);

            cityAdapter.setDropDownViewResource
                    (android.R.layout.simple_spinner_dropdown_item);

            cityEt.setAdapter(cityAdapter);//address_billing

        }

    }

    public void onEvent(AllShippingMethodResponse allShippingMethodResponse) {
        if (allShippingMethodResponse.getResponse_code() == 100) {
            dataShippingMethod = allShippingMethodResponse.getData();
        }
    }

    public void onEvent(AddressListResponse addressListResponse) {
        addressLists = new ArrayList<>();

        boolean hasAddress = false;
        Log.d("address_app", "Response_code" + addressListResponse.getResponse_code());
        if (addressListResponse.getResponse_code() == 100) {
            addressLists = addressListResponse.getData();
            if (addressListResponse.getData().size() > 0) {
                hasAddress = true;
            }


        } else if (addressListResponse.getResponse_code() == 201) {

            //  gotoNewActivity(AddNewAdressActivity.class);
            // finish();
        }

        populateAddressSpinner(hasAddress);

    }

    private void populateAddressSpinner(boolean hasAddress) {
        List<String> addressSpinnerList = new ArrayList<>();
        if (hasAddress) {
            for (int i = 0; i < addressLists.size(); i++) {
                if (addressLists.get(i).isDefaultBillingAddress()) {
                    defaultAddressPosition = i;
                }
                String spinnerAddressLabel = addressLists.get(i).getFirstname()
                        + " "
                        + addressLists.get(i).getLastname()
                        + ", "
                        + addressLists.get(i).getTelephone()
                        + ", "
                        + addressLists.get(i).getCity();
                addressSpinnerList.add(spinnerAddressLabel);
            }
        }

        addressSpinnerList.add("Add New Address");


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, addressSpinnerList);
        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        addressesSpinner.setAdapter(dataAdapter);

        if (hasAddress) {
            addressesSpinner.setSelection(defaultAddressPosition);

            populateAddressField(defaultAddressPosition);
            enabledView(false);
        } else {
            addressesSpinner.setSelection(0);
            enabledView(true);
            emptyAddressField();
        }

        addressesSpinner.setOnItemSelectedListener(this);
        cityEt.setOnItemSelectedListener(this);
        spArea.setOnItemSelectedListener(this);
    }

    public void enabledView(boolean isEditable) {
        firstNameEt.setEnabled(isEditable);
        firstNameEt.setFocusableInTouchMode(isEditable);

        lastNameEt.setEnabled(isEditable);
        lastNameEt.setFocusableInTouchMode(isEditable);

        emailEt.setEnabled(isEditable);
        emailEt.setFocusableInTouchMode(isEditable);

        createAccountCb.setEnabled(isEditable);

        address1Et.setEnabled(isEditable);
        address1Et.setFocusableInTouchMode(isEditable);

        /*address2Et.setEnabled(isEditable);
        address2Et.setFocusableInTouchMode(isEditable);*/

        countrySp.setEnabled(isEditable);
        countrySp.setFocusableInTouchMode(isEditable);
//
        cityEt.setEnabled(isEditable);
        cityEt.setFocusableInTouchMode(isEditable);
        spArea.setEnabled(isEditable);
        spArea.setFocusableInTouchMode(isEditable);

        postalCodeEt.setEnabled(isEditable);
        postalCodeEt.setFocusableInTouchMode(isEditable);

        telephoneEt.setEnabled(isEditable);
        telephoneEt.setFocusableInTouchMode(isEditable);
    }

    public void emptyAddressField() {
        firstNameEt.setText(preferenceService.GetPreferenceValue(PreferenceService.FIRST_NAME_KEY));
        lastNameEt.setText(preferenceService.GetPreferenceValue(PreferenceService.LAST_NAME_KEY));
        emailEt.setText(preferenceService.GetPreferenceValue(PreferenceService.EMAIL_KEY));
        address1Et.setText("");
        /*address2Et.setText("");*/
        countrySp.setSelection(0);
        cityEt.setSelection(0);
        postalCodeEt.setText("");
        telephoneEt.setText("");
        //-----------------------------------
        areaList = new ArrayList<>();
        areaList.add("Select Area");
        areaAdapter = new ArrayAdapter
                (this, android.R.layout.simple_spinner_item, areaList);
        areaAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        spArea.setAdapter(areaAdapter);
    }

    private void populateAddressField(int position) {
        address_billing = addressLists.get(position);

        firstNameEt.setText(address_billing.getFirstname());
        lastNameEt.setText(address_billing.getLastname());
        emailEt.setText(preferenceService.GetPreferenceValue(PreferenceService.EMAIL_KEY));

        address1Et.setText(address_billing.getStreet()[0]);

        try {
            countrySp.setSelection(countryAdapter.getPosition(address_billing.getCountry()));
        } catch (NullPointerException ex) {

        }
//        cityEt.setText(address.getCity());
        postalCodeEt.setText(address_billing.getPostcode());
        telephoneEt.setText(address_billing.getTelephone());

        cityEt.setSelection(cityAdapter.getPosition(address_billing.getCity()));
//        spArea.setSelection(areaAdapter.getPosition(address_billing.getArea()));
        Log.d("get_city", address_billing.getCity() + "===city & area==" + address_billing.getArea());
    }

    private void setPasswordFieldVisibility() {
        if (createAccountCb.isChecked()) {
            passwordAreaLl.setVisibility(View.VISIBLE);
        } else {
            passwordAreaLl.setVisibility(View.GONE);
        }
    }

    private void validateForm() {
        boolean isValid = true;

        if (!FormViews.isValidWithMark(firstNameEt, "First Name")) {
            isValid = false;
        }

        if (!FormViews.isValidWithMark(lastNameEt, "Last Name")) {
            isValid = false;
        }

        if (!FormViews.isValidEmail(emailEt, "Email")) {
            isValid = false;
        }


        if (createAccountCb.isChecked()) {
            if (!FormViews.isValidWithMark(passwordEt, "Password")) {
                isValid = false;
            }

            if (!FormViews.isValidWithMark(confirmPasswordEt, "Confirm Password")) {
                isValid = false;
            }

            if (!FormViews.isEmpty(passwordEt) && !FormViews.isEmpty(confirmPasswordEt) && !FormViews.isEqual(passwordEt, confirmPasswordEt)) {
                confirmPasswordEt.setError("Confirm your password");
                isValid = false;
            }
        }

        if (!FormViews.isValidWithMark(address1Et, "Address")) {
            isValid = false;
        }

        if (!FormViews.isValidWithMark(countrySp, "Select Country")) {
            isValid = false;
        }

        if (!FormViews.isValidWithMark(cityEt, "Select City")) {
            isValid = false;
        }

        if (!FormViews.isValidWithMark(spArea, "Select Area")) {
            isValid = false;
        }
        if (!FormViews.isValidWithMark(postalCodeEt, "Postal Code")) {
            isValid = false;
        }

        if (!FormViews.isValidWithMarkMobile(telephoneEt, "Phone Number")) {
            isValid = false;
        }
        if (!shipmentCb.isChecked()) {
            if (!FormViews.isValidWithMark(firstNameEt_s, "First Name")) {
//                Toast.makeText(BillingAddressActivity.this, "===="+firstNameEt_s.getText().toString(),
//                        Toast.LENGTH_LONG).show();
                isValid = false;
            }
            /*

            EditText ;

            Spinner countrySp_s;

            Spinner cityEt_s;


            Spinner spArea_s;

            EditText postalCodeEt_s;

            EditText telephoneEt_s;
            */
            if (!FormViews.isValidWithMark(lastNameEt_s, "Last Name")) {
                isValid = false;
            }

            if (!FormViews.isValidEmail(emailEt_s, "Email")) {
                isValid = false;
            }
            if (!FormViews.isValidWithMark(address1Et_s, "Address")) {
                isValid = false;
            }

            if (!FormViews.isValidWithMark(countrySp_s, "Select Country")) {
                isValid = false;
            }

            if (!FormViews.isValidWithMark(cityEt_s, "Select City")) {
                isValid = false;
            }

            if (!FormViews.isValidWithMark(spArea_s, "Select Area")) {
                isValid = false;
            }
            if (!FormViews.isValidWithMark(postalCodeEt_s, "Postal Code")) {
                isValid = false;
            }

            if (!FormViews.isValidWithMarkMobile(telephoneEt_s, "Phone Number")) {
                isValid = false;
            }
        }

        if (isValid) {
            if (!shipmentCb.isChecked()) {
                String addList = addressesSpinner.getSelectedItem().toString();
                String addListSP = addressesSpinner_s.getSelectedItem().toString();
                Address billingAddress = new Address();
                if (addList.equals("Add New Address"))
                    billingAddress = getBillingAddress(true);
                else billingAddress = getBillingAddress(false);
                if (addListSP.equals("Add New Address"))
                    callSetAddressApi_ship(true, billingAddress);
                else callSetAddressApi_ship(false, billingAddress);
                //ShippingAddressActivity.billingAddress = getBillingAddress(true);
                // gotoNewActivity(ShippingAddressActivity.class);

            } else {
                callSetAddressApi(getBillingAddress(true));
            }

        }


    }


    private void callSetAddressApi(Address billingAddress) {
        int cartId = getCartId();
        Log.d("Billing_add", "catrt==" + cartId);
        if (cartId != -1) {
            Call<MessageResponse> callback = RestClient.get().setCheckoutAddress(getAddressReqObject(cartId, billingAddress));
            NetworkServiceHandler.processCallBack(callback, this);

//             String responseString;
//            try {
//                responseString = new String( callback.execute().body().getData().);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            Log.d("Response: ", + responseString);
//
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }
    }

    public void onEvent(MessageResponse messageResponse) {

        String responseString = new String(messageResponse.getData().getMsg());

        Log.d("Response_f", "====" + responseString);


        if (messageResponse.getResponse_code() == 100) {
            //  showInfoSnackBar(messageResponse.getData().getMsg());
//            gotoNewActivity(ShippingMethodActivity.class);
            PaymentMethodActivity.shippingCode = shippingMethodSetRequest;
            gotoNewActivity(PaymentMethodActivity.class);


        }
    }

    private SetAddressRequest getAddressReqObject(int cartId, Address address) {
        if (dataShippingMethod.size() < 2) {
            addShippinData();
        }
        Log.d("shipping_chargeAA", "===" + address.getCity());
        SetAddressRequest setAddressRequest = new SetAddressRequest();
        String inSide = preferenceService.GetPreferenceValue(PreferenceService.SHIPPIN_IN_KEY);
        String outSide = preferenceService.GetPreferenceValue(PreferenceService.SHIPPING_OUT_KEY);
        Log.d("shipping_chargeAA", inSide + "==IN & OUT==" + outSide);
        for (ShippingMethod shp : dataShippingMethod) {
            Log.d("shipping_chargeAA", "===" + shp.getPrice());

            if (shp.getCarrier_title().equals("Dhaka"))
                setAddressRequest.setShipping_code(shp.getCode());
            if (Float.valueOf(inSide) > 40) {//if (Float.valueOf(inSide) > Float.valueOf(shp.getPrice()))
                setAddressRequest.setShipping_price(inSide);
            } else {
                setAddressRequest.setShipping_price("40.00");
            }
        }
        if (address.getCity().equals("Dhaka")) {
            for (ShippingMethod shp : dataShippingMethod) {
                if (shp.getCarrier_title().equals("Dhaka City")) {
                    setAddressRequest.setShipping_code(shp.getCode());
                    Log.d("shipping_chargeAA", "=Shipping_code==" + setAddressRequest.getShipping_code());

                }

                if (Float.valueOf(inSide) > 40) {
                    setAddressRequest.setShipping_price(inSide);
                } else {
                    setAddressRequest.setShipping_price("40.00");
                }
            }

        } else {
            for (ShippingMethod shp : dataShippingMethod) {
                if (shp.getCarrier_title().equals("Outside Dhaka City")) {
                    setAddressRequest.setShipping_code(shp.getCode());
                    Log.d("shipping_chargeAA", "=Shipping_code==" + setAddressRequest.getShipping_code());


                }

                if (Float.valueOf(outSide) > 60) {// Float.valueOf(shp.getPrice()
                    setAddressRequest.setShipping_price(outSide);
                } else {
                    setAddressRequest.setShipping_price("60.00");
                }
            }
        }
        setAddressRequest.setCartId(cartId);


        setAddressRequest.setBillingAddress(address);
        setAddressRequest.setShippingAddress(address);
        Log.d("shipping_chargeAA", setAddressRequest.getShipping_price() + "=price_code==" + setAddressRequest.getShipping_code());


//        Log.d("Billing_add", "address==" + setAddressRequest.getShipping_code());
//        Log.d("Billing_add_amount", "address==" + setAddressRequest.getShipping_price());
        preferenceService.SetPreferenceValue(PreferenceService.SHIPPING_CHARGE_KEY, setAddressRequest.getShipping_price());
        String shipping_charge = preferenceService.GetPreferenceValue(PreferenceService.SHIPPING_CHARGE_KEY);
        Log.d("shipping_chargeAA", "=shipping_charge cal==" + shipping_charge);

        shippingMethodSetRequest = new ShippingMethodSetRequest();
        shippingMethodSetRequest.setCartId(cartId);
        Log.d("shippingMethod", "cartId--" + cartId);
        Log.d("shippingMethod", "Code--" + setAddressRequest.getShipping_code());
        shippingMethodSetRequest.setMethod_code(setAddressRequest.getShipping_code());
        // setShippingMethodApi(shippingMethodSetRequest);


        return setAddressRequest;
    }

    //-----------------Shipping data--------------------
    public void addShippinData() {
        ShippingMethod sp = new ShippingMethod();
        sp.setCarrier_title("Dhaka City");
        sp.setCode("flatrate3_flatrate3");
        dataShippingMethod.add(sp);

        sp = new ShippingMethod();
        sp.setCarrier_title("Outside Dhaka City");
        sp.setCode("flatrate2_flatrate2");
        dataShippingMethod.add(sp);

        sp = new ShippingMethod();
        sp.setCarrier_title("Shipping Charge");
        sp.setCode("flatrate_flatrate");

        dataShippingMethod.add(sp);
    }

    //=============================================
    private SetAddressRequest getAddressReqObject_ship(int cartId, boolean isNewAddress, Address billingAddress) {
        SetAddressRequest setAddressRequest = new SetAddressRequest();
        setAddressRequest.setCartId(cartId);


        if (isNewAddress) {
            Address address = new Address();
            address.setFirstname(firstNameEt_s.getText().toString());
            address.setLastname(lastNameEt_s.getText().toString());
            // email nai
            String[] street = {address1Et_s.getText().toString()};
            address.setStreet(street);
            address.setCountry_code(countryList.get(countrySp_s.getSelectedItemPosition()).getValue());
//            address.setCity(cityEt.getText().toString());//Update
            address.setCity(cityList.get(cityEt_s.getSelectedItemPosition()).getCity());
            address.setRegion(areaListST.get(spArea_s.getSelectedItemPosition()));
            address.setPostcode(postalCodeEt_s.getText().toString());
            address.setTelephone(telephoneEt_s.getText().toString());

            setAddressRequest.setBillingAddress(billingAddress);
            setAddressRequest.setShippingAddress(address);


        } else {
            setAddressRequest.setBillingAddress(billingAddress);
            setAddressRequest.setShippingAddress(addressLists.get(addressesSpinner.getSelectedItemPosition()));
        }
//------------------------------------------------
        String inSide = preferenceService.GetPreferenceValue(PreferenceService.SHIPPIN_IN_KEY);
        String outSide = preferenceService.GetPreferenceValue(PreferenceService.SHIPPING_OUT_KEY);
        Log.d("shipping_chargeAA", inSide + "===" + outSide);
        if (dataShippingMethod.size() < 2) {
            addShippinData();
        }
        for (ShippingMethod shp : dataShippingMethod) {
            Log.d("shipping_chargeAA", "===" + shp.getPrice());

            if (shp.getCarrier_title().equals("Dhaka"))
                setAddressRequest.setShipping_code(shp.getCode());
            if (Float.valueOf(inSide) > 40) {//if (Float.valueOf(inSide) > Float.valueOf(shp.getPrice()))
                setAddressRequest.setShipping_price(inSide);
            } else {
                setAddressRequest.setShipping_price("40.00");
            }
        }
        if (setAddressRequest.getShippingAddress().getCity().equals("Dhaka")) {
            for (ShippingMethod shp : dataShippingMethod) {
                if (shp.getCarrier_title().equals("Dhaka City"))
                    setAddressRequest.setShipping_code(shp.getCode());
                if (Float.valueOf(inSide) > 40) {
                    setAddressRequest.setShipping_price(inSide);
                } else {
                    setAddressRequest.setShipping_price("40.00");
                }
            }

        } else {
            for (ShippingMethod shp : dataShippingMethod) {
                if (shp.getCarrier_title().equals("Outside Dhaka City"))
                    setAddressRequest.setShipping_code(shp.getCode());
                if (Float.valueOf(outSide) > 60) {// Float.valueOf(shp.getPrice()
                    setAddressRequest.setShipping_price(outSide);
                } else {
                    setAddressRequest.setShipping_price("60.00");
                }
            }
        }
        //---------------------------------------------
        Log.d("Billing_add", "address==" + setAddressRequest.getShipping_code());
        Log.d("Billing_add_amount", "address==" + setAddressRequest.getShipping_price());
        preferenceService.SetPreferenceValue(PreferenceService.SHIPPING_CHARGE_KEY, setAddressRequest.getShipping_price());
        shippingMethodSetRequest = new ShippingMethodSetRequest();
        shippingMethodSetRequest.setCartId(cartId);
        Log.d("shippingMethod", "cartId--" + cartId);
        Log.d("shippingMethod", "Code--" + setAddressRequest.getShipping_code());
        shippingMethodSetRequest.setMethod_code(setAddressRequest.getShipping_code());

        return setAddressRequest;
    }

    private void callSetAddressApi_ship(boolean isNewAddress, Address billingAddress) {
        int cartId = getCartId();
        if (cartId != -1) {
            Call<MessageResponse> callback = RestClient.get().setCheckoutAddress(getAddressReqObject_ship(cartId, isNewAddress, billingAddress));
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }
    }

    //---------------------------------------
    @Override
    public void onClick(View v) {
        super.onClick(v);
        int resId = v.getId();
        if (resId == R.id.btn_continue) {
            if (addressesSpinner.getSelectedItemPosition() < addressLists.size()) {
                if (!shipmentCb.isChecked()) {

                    // ShippingAddressActivity.billingAddress = getBillingAddress(false);
                    //gotoNewActivity(ShippingAddressActivity.class);
                    String addList = addressesSpinner.getSelectedItem().toString();
                    String addListSP = addressesSpinner_s.getSelectedItem().toString();
                    Address billingAddress = new Address();
                    if (addList.equals("Add New Address"))
                        billingAddress = getBillingAddress(true);
                    else billingAddress = getBillingAddress(false);
                    if (addListSP.equals("Add New Address"))
                        callSetAddressApi_ship(true, billingAddress);
                    else callSetAddressApi_ship(false, billingAddress);


                } else {
                    callSetAddressApi(getBillingAddress(false));
                }

            } else {
                validateForm();
            }
        }
    }

    private Address getBillingAddress(boolean isNewAddress) {
        if (isNewAddress) {
            Address address = new Address();
            address.setFirstname(firstNameEt.getText().toString());
            address.setLastname(lastNameEt.getText().toString());
            // email nai
            String[] street = {address1Et.getText().toString()};
            address.setStreet(street);
            address.setCountry_code(countryList.get(countrySp.getSelectedItemPosition()).getValue());
            address.setCity(cityList.get(cityEt.getSelectedItemPosition()).getCity());
            address.setRegion(areaList.get(spArea.getSelectedItemPosition()));
            address.setPostcode(postalCodeEt.getText().toString());
            address.setTelephone(telephoneEt.getText().toString());

            return address;
        } else {
            return addressLists.get(addressesSpinner.getSelectedItemPosition());
        }
    }

    public void altmessage() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(BillingAddressActivity.this);
        builder1.setMessage("Change Billing Address.");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        city_cl = 0;
                        AddressUpdateActivity.address = address_billing;
//                AddressUpdateActivity.cityList = cityList;
                        AddressUpdateActivity.area_ch = 0;
                        gotoNewActivity(AddressUpdateActivity.class);
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
//===========================================================
//
//                      Shipping Address
//
//===========================================================

    private void callAddressListApi_s() {
        if (isLoggedIn()) {
            addressesSpinner_s.setVisibility(View.VISIBLE);
            String customerId = preferenceService.GetPreferenceValue(PreferenceService.ENTITY_KEY);
            Call<AddressListResponseS> callback = RestClient.get().getAddressList1(customerId, "");
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            addressesSpinner_s.setVisibility(View.GONE);
        }

    }

    public void onEvent(AddressListResponseS addressListResponse) {

//============================================
        countryAdapterS = new ArrayAdapter
                (this, android.R.layout.simple_spinner_item, countryListST);
        countryAdapterS.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        countrySp_s.setAdapter(countryAdapterS);

        cityAdapterS = new ArrayAdapter
                (this, android.R.layout.simple_spinner_item, cityListST);

        cityAdapterS.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);

        cityEt_s.setAdapter(cityAdapterS);
        areaListST = new ArrayList<>();
        areaListST.add("Select Area");
        areaAdapterS = new ArrayAdapter
                (this, android.R.layout.simple_spinner_item, areaListST);
        areaAdapterS.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        spArea_s.setAdapter(areaAdapterS);
//============================================
        addressListsS = new ArrayList<>();
        boolean hasAddress = false;

        if (addressListResponse.getResponse_code() == 100) {
            addressListsS = addressListResponse.getData();
            if (addressListResponse.getData().size() > 0) {
                hasAddress = true;
            }
        }
        populateAddressSpinnerS(hasAddress);
    }


    private void populateAddressSpinnerS(boolean hasAddress) {
        List<String> addressSpinnerList = new ArrayList<>();
        if (hasAddress) {
            for (int i = 0; i < addressListsS.size(); i++) {
                if (addressLists.get(i).isDefaultShippingAddress()) {
                    defaultAddressPosition = i;
                }
                String spinnerAddressLabel = addressListsS.get(i).getFirstname()
                        + " "
                        + addressListsS.get(i).getLastname()
                        + ", "
                        + addressListsS.get(i).getTelephone()
                        + ", "
                        + addressListsS.get(i).getCity();
                addressSpinnerList.add(spinnerAddressLabel);
            }
        }

        addressSpinnerList.add("Add New Address");


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, addressSpinnerList);
        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        addressesSpinner_s.setAdapter(dataAdapter);

        if (hasAddress) {
            addressesSpinner_s.setSelection(defaultAddressPosition);
            populateAddressFieldS(defaultAddressPosition);
            enabledViewS(false);
        } else {
            addressesSpinner_s.setSelection(0);
            enabledViewS(true);
            emptyAddressFieldS();
        }

        addressesSpinner_s.setOnItemSelectedListener(this);
        cityEt_s.setOnItemSelectedListener(this);
    }

    private void populateAddressFieldS(int position) {

        Address address = addressListsS.get(position);
        address_shipping = address;
        firstNameEt_s.setText(address.getFirstname());
        lastNameEt_s.setText(address.getLastname());
        emailEt_s.setText(preferenceService.GetPreferenceValue(PreferenceService.EMAIL_KEY));
        /*if (address.getStreet().length > 1) {
            address1Et.setText(address.getStreet()[1]);
        }*/

        address1Et_s.setText(address.getStreet()[0]);

        try {
            countrySp_s.setSelection(countryAdapterS.getPosition(address.getCountry()));
            cityEt_s.setSelection(cityAdapter.getPosition(address.getCity()));
        } catch (NullPointerException ex) {

        }
//        cityEt.setText(address.getCity());//Update
        postalCodeEt_s.setText(address.getPostcode());
        telephoneEt_s.setText(address.getTelephone());
    }

    public void enabledViewS(boolean isEditable) {
        firstNameEt_s.setEnabled(isEditable);
        firstNameEt_s.setFocusableInTouchMode(isEditable);

        lastNameEt_s.setEnabled(isEditable);
        lastNameEt_s.setFocusableInTouchMode(isEditable);

        emailEt_s.setEnabled(isEditable);
        emailEt_s.setFocusableInTouchMode(isEditable);

        address1Et_s.setEnabled(isEditable);
        address1Et_s.setFocusableInTouchMode(isEditable);

        countrySp_s.setEnabled(isEditable);
        countrySp_s.setFocusableInTouchMode(isEditable);

        cityEt_s.setEnabled(isEditable);
        cityEt_s.setFocusableInTouchMode(isEditable);

        postalCodeEt_s.setEnabled(isEditable);
        postalCodeEt_s.setFocusableInTouchMode(isEditable);

        telephoneEt_s.setEnabled(isEditable);
        telephoneEt_s.setFocusableInTouchMode(isEditable);
    }

    public void emptyAddressFieldS() {
        firstNameEt_s.setText(preferenceService.GetPreferenceValue(PreferenceService.FIRST_NAME_KEY));
        lastNameEt_s.setText(preferenceService.GetPreferenceValue(PreferenceService.LAST_NAME_KEY));
        emailEt_s.setText(preferenceService.GetPreferenceValue(PreferenceService.EMAIL_KEY));
        address1Et_s.setText("");
        countrySp_s.setSelection(0);
        postalCodeEt_s.setText("");
        telephoneEt_s.setText("");
    }

    //======================END Shipping ============================
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int resId = parent.getId();

        if (resId == R.id.sp_addresses) {
            if (position < addressLists.size()) {
                populateAddressField(position);
                enabledView(false);
            } else {
                enabledView(true);
                emptyAddressField();
            }
        } else if (resId == R.id.sp_addresses_s) {
            if (position < addressListsS.size()) {
                populateAddressFieldS(position);
                enabledViewS(false);
            } else {
                enabledViewS(true);
                emptyAddressFieldS();
            }
        } else if (resId == R.id.et_city) {
            //-----------------------------
            //if (position == 0) {

            //}
            //-----------------------------
            areaList = new ArrayList<>();
            areaList.add("Select Area");
            if (cityList.get(position).getArea_list().length() > 1) {

                List<String> area = Arrays.asList(cityList.get(position).getArea_list().split(","));
                for (String ar : area) {
                    String aread = ar.substring(0, 1).toUpperCase() + ar.substring(1);
                    areaList.add(aread);
                }

                areaAdapter = new ArrayAdapter
                        (this, android.R.layout.simple_spinner_item, areaList);

                areaAdapter.setDropDownViewResource
                        (android.R.layout.simple_spinner_dropdown_item);

                spArea.setAdapter(areaAdapter);
//            Toast.makeText(BillingAddressActivity.this, address_billing.getArea() + "===" + city_cl + "====" + address_billing.getCity(),
//                    Toast.LENGTH_SHORT).show();
                if (city_cl < 1) {
                    if (address_billing == null)
                        Log.d("get_area", ":::::address_billing");
                    else if (address_billing.getArea() == null)
                        Log.d("get_area", ":::::getArea");
                    else {
                        if (address_billing.getArea().length() > 0)
                            spArea.setSelection(areaAdapter.getPosition(address_billing.getArea()));
                    }
                }
                // else altmessage();
                city_cl++;
            }
        } else if (resId == R.id.et_city_s) {
            //--------------------------------
            //if (position == 0) {

            //}

            //--------------------------------
            areaListST = new ArrayList<>();
            areaListST.add("Select Area");
            if (cityList.get(position).getArea_list().length() > 1) {
                List<String> area = Arrays.asList(cityList.get(position).getArea_list().split(","));
                for (String ar : area) {
                    String aread = ar.substring(0, 1).toUpperCase() + ar.substring(1);
                    areaListST.add(aread);
                }
                areaAdapterS = new ArrayAdapter
                        (this, android.R.layout.simple_spinner_item, areaListST);
                areaAdapterS.setDropDownViewResource
                        (android.R.layout.simple_spinner_dropdown_item);
                spArea_s.setAdapter(areaAdapterS);
                if (address_shipping == null)
                    Log.d("get_area", ":::::address_billing");
                else if (address_shipping.getArea() == null)
                    Log.d("get_area", ":::::getArea");
                else {
                    if (address_shipping.getArea().length() > 0)
                        spArea_s.setSelection(areaAdapterS.getPosition(address_shipping.getArea()));
                }

            }
        } else if (resId == R.id.et_area) {
//                        Toast.makeText(BillingAddressActivity.this, address_billing.getArea() + "===" + city_cl + "====" + address_billing.getCity(),
//                                Toast.LENGTH_SHORT).show();
            if (city_cl > 1) {
                // altmessage();
            }
            city_cl++;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int resId = buttonView.getId();

        if (resId == R.id.cb_create_account) {
            setPasswordFieldVisibility();
        }
    }

}

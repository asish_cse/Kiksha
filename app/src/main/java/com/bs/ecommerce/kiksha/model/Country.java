package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/4/2016.
 */
public class Country {
    private String value;
    private String label;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}

package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/9/2016.
 */
public class CartItems {
    private String item_id;
    private String product_id;
    private String parent_item_id;
    private String sku;
    private String name;
    private int qty;
    private String price;
    private String discount_amount;
    private String thumbnail;
    private String parent_product_id;

    private String catgory_id;

    public String getCatgory_id() {
        return catgory_id;
    }

    public void setCatgory_id(String catgory_id) {
        this.catgory_id = catgory_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getParent_item_id() {
        return parent_item_id;
    }

    public void setParent_item_id(String parent_item_id) {
        this.parent_item_id = parent_item_id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getParent_product_id() {
        return parent_product_id;
    }

    public void setParent_product_id(String parent_product_id) {
        this.parent_product_id = parent_product_id;
    }
}

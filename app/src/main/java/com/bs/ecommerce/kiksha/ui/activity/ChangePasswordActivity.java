package com.bs.ecommerce.kiksha.ui.activity;

import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.PasswordChangeRequest;
import com.bs.ecommerce.kiksha.model.PasswordChangeResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.view.FormViews;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/27/2016.
 */
@ContentView(R.layout.activity_change_password)
public class ChangePasswordActivity extends BaseActivity {

    @InjectView(R.id.et_new_password)
    EditText newPasswordEditText;

    @InjectView(R.id.et_confirm_new_password)
    EditText confirmNewPasswordEditText;

    @InjectView(R.id.et_current_password)
    EditText currentPasswordEditText;

    @InjectView(R.id.btn_save)
    Button saveBtn;

    String currentEmailStr;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
    }

    @Override
    public void setClicklistener() {
        setToolbarTitle("Change Password");
        super.setClicklistener();
        saveBtn.setOnClickListener(this);

    }

    private void validateForm() {
        boolean isValid = true;

        if (!FormViews.isValidWithMark(newPasswordEditText, "New Password"))
            isValid = false;
        if (!FormViews.isValidWithMark(confirmNewPasswordEditText, "Confirm New Password"))
            isValid = false;
        if (!FormViews.isValidWithMark(currentPasswordEditText, "CurrentPassword "))
            isValid = false;
        if (!FormViews.isEmpty(newPasswordEditText) && !FormViews.isEmpty(confirmNewPasswordEditText)) {
            if (!FormViews.isEqual(newPasswordEditText, confirmNewPasswordEditText)) {
                confirmNewPasswordEditText.setError("Confirm your Password");
                isValid = false;
            }
        }

        if (isValid) {
            sendPasswordChangeRequest();
        }
    }

    private void sendPasswordChangeRequest() {
        currentEmailStr = preferenceService.GetPreferenceValue(PreferenceService.EMAIL_KEY);
        PasswordChangeRequest passwordChangeRequest = new PasswordChangeRequest();
        passwordChangeRequest.setCurrentPassword(FormViews.getTexBoxFieldValue(currentPasswordEditText));
        passwordChangeRequest.setEmail(currentEmailStr);
        passwordChangeRequest.setNewPassword(FormViews.getTexBoxFieldValue(newPasswordEditText));
        NetworkServiceHandler.processCallBack(RestClient.get().changePassword(passwordChangeRequest), this);
    }

    public void onEvent(PasswordChangeResponse passwordChangeResponse)
    {
        if(passwordChangeResponse.getResponse_code()==100)
        {
           // showInfoSnackBar("Password Changed Successfully");
            toastMsg("Password Changed Successfully");
            finish();
           /* preferenceService.SetPreferenceValue(PreferenceService.EMAIL_KEY,
                    passwordChangeResponse.getCustomer().getEmail());*/
        }
    }

    private void toastMsg(String str){
        Toast toast = Toast.makeText(this, Html.fromHtml("<font size='12' color='#FFFFFF'  ><b>" + str + "</b></font>"), Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM|Gravity.FILL_HORIZONTAL, -20, -20);
        // toast.setGravity(Gravity.CENTER, 50, 50);
        toast.getView().setPadding(10, 25, 10, 35);

//        toast.getView().setBackgroundColor(this.getResources().getColor(R.color.listdivider));
        toast.show();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resourceId = v.getId();

        if (resourceId == R.id.btn_save) {
            validateForm();

        }


    }
}

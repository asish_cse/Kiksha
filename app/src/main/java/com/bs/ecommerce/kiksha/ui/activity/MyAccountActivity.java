package com.bs.ecommerce.kiksha.ui.activity;

import android.content.IntentFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.CartItemChangeResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.util.BroadCastUtils;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/21/2016.
 */
@ContentView(R.layout.activity_my_account)
public class MyAccountActivity extends BaseActivity {

    @InjectView(R.id.ll_logged_in_my_account)
    ViewGroup loggedInMyAccountLayout;

    @InjectView(R.id.ll_logout_my_account)
    ViewGroup loggedOutMyAccountLayout;

    @InjectView(R.id.tv_register)
    TextView registerTextView;

    @InjectView(R.id.tv_login)
    TextView loginTextView;

    @InjectView(R.id.tv_checkout)
    TextView myCheckoutTextView;

    @InjectView(R.id.tv_my_cart)
    TextView myCartTextView;

    @InjectView(R.id.tv_my_orders)
    TextView myOrdersTextView;

    @InjectView(R.id.tv_personal_details)
    TextView personalDetailTextView;

    @InjectView(R.id.tv_logout)
    TextView logoutTextView;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();

        generateView();
        setToolbarTitle("My Account");
    }


    private void generateView()
    {
        if(isLoggedIn())
            setVisibility(loggedInMyAccountLayout,loggedOutMyAccountLayout);
        else
            setVisibility(loggedOutMyAccountLayout,loggedInMyAccountLayout);
    }

    @Override
    protected void registerBroadcastReceiver() {
        super.registerBroadcastReceiver();

        registerReceiver(broadcast_reciever, new IntentFilter(BroadCastUtils.RECREATE_VIEW));

    }

    protected void setVisibility(View visibleView, View inVisibleView)
    {
        visibleView.setVisibility(View.VISIBLE);
        inVisibleView.setVisibility(View.GONE);
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        registerTextView.setOnClickListener(this);
        loginTextView.setOnClickListener(this);
        personalDetailTextView.setOnClickListener(this);
        logoutTextView.setOnClickListener(this);
        myOrdersTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int resourceId=v.getId();
        if(resourceId==R.id.tv_register)
        {
          gotoNewActivity(UserRegisterActivity.class);
        }
        else if(resourceId==R.id.tv_login)
        {
             gotoNewActivity(UserLoginActivity.class);
        }
        else if(resourceId==R.id.tv_personal_details)
        {
                  gotoNewActivity(PersonalDetailsActivity.class);
        }

        else if(resourceId==R.id.tv_logout)
        {
            performLogout();
        }
        else if(resourceId==R.id.tv_my_orders)
        {
            gotoNewActivity(OrderHistoryActivity.class);
        }
    }

    private void performLogout() {
        /*int cart_id = getCartId();
        if (cart_id != -1) {
            Call<CartItemChangeResponse> callback = RestClient.get().emptyCart(String.valueOf(cart_id));
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            // showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }*/
        preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY,false);
        preferenceService.SetPreferenceValue(PreferenceService.CART_KEY, -1);
        badgeCount = 0;
        updateCartItemCount(badgeCount);
        gotoHomeActivity(HomePageActivity2.class);
      //  generateView();
      //  sendBroadcast(BroadCastUtils.RECREATE_VIEW);
    }

    /*public void onEvent(CartItemChangeResponse cartItemChangeResponse) {
        if (cartItemChangeResponse.getResponse_code() == 100) {
            // showInfoSnackBar(cartItemChangeResponse.getData().getMsg());
            preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY,false);
            //preferenceService.SetPreferenceValue(PreferenceService.CART_KEY, -1);
            badgeCount = Integer.valueOf(Integer.valueOf(cartItemChangeResponse.getData().getCart().getData().getItems_count()));
            updateCartItemCount(badgeCount);
            gotoHomeActivity(HomePageActivity.class);
        }
    }*/

    @Override
    protected void unregisterBroadcast() {
        super.unregisterBroadcast();
        try {
            if(broadcast_reciever!=null)
                unregisterReceiver(broadcast_reciever);

        }
        catch (IllegalArgumentException ex)
        {

        }
    }
}

package com.bs.ecommerce.kiksha.ui.activity;

import com.bs.ecommerce.kiksha.model.CartDetailsResponse;
import com.bs.ecommerce.kiksha.model.CartMargeRequest;
import com.bs.ecommerce.kiksha.model.CartMargeResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;

import retrofit2.Call;

/**
 * Created by arifn on 02-Nov-16.
 */

public class UserAuthenticationActivity extends BaseActivity {
    protected void callCartMargeApi() {
        Call<CartMargeResponse> callback = RestClient.get().margeCart(getCartMargeReqObj());
        NetworkServiceHandler.processCallBack(callback, this);
    }

    protected CartMargeRequest getCartMargeReqObj() {
        CartMargeRequest cartMargeRequestObject = new CartMargeRequest();
        cartMargeRequestObject.setCartId(getCartId());
        cartMargeRequestObject.setCustomerId(Integer.valueOf(preferenceService.GetPreferenceValue(PreferenceService.ENTITY_KEY)));
        return cartMargeRequestObject;
    }

    public void onEvent(CartMargeResponse cartMargeResponse) {
        if (cartMargeResponse.getResponse_code() == 100) {
            int cartId = Integer.valueOf(cartMargeResponse.getData().getCartId());
            preferenceService.SetPreferenceValue(PreferenceService.CART_KEY, cartId);
            callCartDetailsApi(cartId);
        }

    }

    protected void callCartDetailsApi(int cartId) {
        Call<CartDetailsResponse> callback = RestClient.get().getCartDetails(String.valueOf(cartId),"");
        NetworkServiceHandler.processCallBack(callback, this);
    }

    public void onEvent(CartDetailsResponse cartDetailsResponse) {
        if (cartDetailsResponse.getResponse_code() == 100 && cartDetailsResponse.getData() != null) {
            badgeCount = Integer.valueOf(cartDetailsResponse.getData().getItems_count());
            updateCartItemCount(badgeCount);
            refresh();
        }
    }

    protected void refresh() {

    }
}

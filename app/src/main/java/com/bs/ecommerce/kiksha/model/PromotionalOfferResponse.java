package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by BS62 on 5/24/2016.
 */
public class PromotionalOfferResponse extends BaseResponse {
    private List<PromotionalOffer> data;

    public List<PromotionalOffer> getData() {
        return data;
    }

    public void setData(List<PromotionalOffer> data) {
        this.data = data;
    }
}

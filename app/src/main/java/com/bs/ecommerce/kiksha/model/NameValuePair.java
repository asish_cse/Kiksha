package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 5/9/2016.
 */
public class NameValuePair  {
    private String name;
    private int value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

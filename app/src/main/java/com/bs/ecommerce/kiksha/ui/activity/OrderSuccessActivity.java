package com.bs.ecommerce.kiksha.ui.activity;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.CartCreateResponse;
import com.bs.ecommerce.kiksha.model.CartDetailsResponse;
import com.bs.ecommerce.kiksha.model.CartItemChangeResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 5/3/2016.
 */

@ContentView(R.layout.activity_order_success)
public class OrderSuccessActivity extends BaseActivity {

    @InjectView(R.id.tv_success_msg)
    TextView successMsgTv;
    @InjectView(R.id.btn_continue_shopping)
    Button continueShoppingBtn;

    public static String successMsg = "";
    public static String orderId;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Order Success");
        //callClearCartApi();
        setMessage();

        badgeCount = 0;
        updateCartItemCount(0);
        preferenceService.SetPreferenceValue(PreferenceService.CART_KEY, -1);
    }

    private void callClearCartApi() {
        int cart_id = getCartId();
        if (cart_id != -1) {
            Call<CartItemChangeResponse> callback = RestClient.get().emptyCart(String.valueOf(cart_id),"");
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }

    }

    public void onEvent(CartItemChangeResponse cartItemChangeResponse) {
        if (cartItemChangeResponse.getResponse_code() == 100) {
            badgeCount = Integer.valueOf(Integer.valueOf(cartItemChangeResponse.getData().getCart().getData().getItems_count()));
            updateCartItemCount(badgeCount);
            setMessage();
        }
    }

    private void setMessage() {
        successMsgTv.setText(successMsg);
    }

    /*private void createNewCart() {
        Call<CartCreateResponse> callback = RestClient.get().createNewCart();
        NetworkServiceHandler.processCallBack(callback, this);
    }*/

    /*public void onEvent(CartCreateResponse cartCreateResponse) {
        int cartID = cartCreateResponse.getData().getCart_id();
        if (cartCreateResponse.getResponse_code() == 100) {
            preferenceService.SetPreferenceValue(PreferenceService.CART_KEY, cartID);
            preferenceService.SetPreferenceValue(PreferenceService.GUEST_CART_KEY, cartID);

            badgeCount = 0;
            updateCartItemCount(badgeCount);

            *//*if (isLoggedIn()) {
                preferenceService.SetPreferenceValue(PreferenceService.CART_KEY, cartID);
            } else {
                preferenceService.SetPreferenceValue(PreferenceService.GUEST_CART_KEY, cartID);
            }*//*
        }
    }*/


    @Override
    public void setClicklistener() {
        super.setClicklistener();
        continueShoppingBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int resId = v.getId();
        if (resId == R.id.btn_continue_shopping) {
            gotoHomeActivity(HomePageActivity2.class);
        }
    }
}

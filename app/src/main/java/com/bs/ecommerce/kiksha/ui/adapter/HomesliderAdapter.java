package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.model.HomeSlider;
import com.bs.ecommerce.kiksha.model.Homedata2;
import com.bs.ecommerce.kiksha.model.Homesliderlist2;
import com.bs.ecommerce.kiksha.ui.activity.ProductListActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomesliderAdapter extends ArrayAdapter<HomeSlider> {

    List<HomeSlider> contactList;
    Context context;
    private LayoutInflater mInflater;
    GridLayoutManager bottomGridLayoutManager;
  //  protected OnItemClickListener mItemClickListener;

    // Constructors
    public HomesliderAdapter(Context context, List<HomeSlider> objects) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        contactList = objects;
    }

    @Override
    public HomeSlider getItem(int position) {
        return contactList.get(position);
    }

//    public interface OnItemClickListener {
//        public void onItemClick(View view, int position);
//    }
//
//    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
//        this.contactList = mItemClickListener;
//    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
       bottomGridLayoutManager = new GridLayoutManager(context, 2);
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.layout_row_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        HomeSlider item = getItem(position);



        Log.d("===slider=","----");
        Log.d("===slider="+item.getImage_file(),"----");
//        vh.textViewEmail.setText(item.getEmail());
//        Picasso.with(context).load(item.getProfilePic()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(vh.imageView);
   //     vh.textViewName.setTag(vh);
   //     convertView.setTag(vh);
//        vh.textViewName.setOnClickListener(new View.OnClickListener() {
//            int count=0;
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                View p = (View) v.getParent();
//                ViewHolder holder1 = (ViewHolder) v.getTag();
//              //  count = Integer.valueOf(holder1.textViewName.getText().toString());
//                //count++;
//                holder1.textViewName.setText("okay");
//
//
//
//            }
//        });





  //      vh.textViewEmail.setText(item.getParent_id());
    //    Picasso.with(context).load(item.getChildren().placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(vh.imageView);

        Picasso.with(context).load(item.getImage_file()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(vh.imageView);

        Log.d("===image file="+item.getImage_file(),"----");
     //   Picasso.with(context).load(item.getItems().get(0).getImage_file()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(vh.imageView11);



        vh.imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub




            }
        });



        return vh.rootView;
    }




    private static class ViewHolder  {
        public final RelativeLayout rootView;
        public final ImageView imageView;

 //       public final TextView textViewEmail;
        public final View separator;




    //    public final RecyclerView featuredCategoryRecyclerViewBottom;

        private ViewHolder(RelativeLayout rootView, ImageView imageView,
                          View separator) {
            this.rootView = rootView;
            this.imageView = imageView;

            this.separator = separator;

           // this.featuredCategoryRecyclerViewBottom = featuredCategoryRecyclerViewBottom;
        }

        public static ViewHolder create(RelativeLayout rootView) {

            ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);

            View separator = (View) rootView.findViewById(R.id.separator);

            return new ViewHolder(rootView, imageView,separator);
        }

//        @Override
//        public void onClick(View v) {
//
//            if (mItemClickListener != null) {
//                mItemClickListener.onItemClick(v, getAdapterPosition());
//            }
//        }
    }
}

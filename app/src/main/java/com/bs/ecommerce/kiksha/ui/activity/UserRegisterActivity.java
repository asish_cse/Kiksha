package com.bs.ecommerce.kiksha.ui.activity;

import android.content.IntentFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.RegistrationRequest;
import com.bs.ecommerce.kiksha.model.UserAuthenticationResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.view.FormViews;
import com.bs.ecommerce.kiksha.util.BroadCastUtils;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/21/2016.
 */
@ContentView(R.layout.activity_user_registration)
public class UserRegisterActivity extends UserAuthenticationActivity {

    @InjectView(R.id.et_first_name)
    EditText firstNameEditText;

    @InjectView(R.id.et_last_name)
    EditText lastNameEditText;

    @InjectView(R.id.et_password)
    EditText passwordEdittextText;

    @InjectView(R.id.et_email)
    EditText emailEditText;

    @InjectView(R.id.btn_sign_in)
    Button signInBtn;

    @InjectView(R.id.btn_continue)
    Button continueBtn;

    @InjectView(R.id.page_container)
    View pageContainer;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Registration");
    }

    @Override
    protected void registerBroadcastReceiver() {
        registerReceiver(broadcast_reciever, new IntentFilter(BroadCastUtils.RECREATE_VIEW));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLoggedIn()) {
            if (this instanceof AboutYouActivity) {

            } else {
                this.finish();
            }
        }
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        continueBtn.setOnClickListener(this);
        signInBtn.setOnClickListener(this);
    }

    protected void validateForm() {
        boolean isValid = true;
        if (!FormViews.isValidWithMark(firstNameEditText, "First Name")) {
            isValid = false;
        }
        if (!FormViews.isValidWithMark(lastNameEditText, "Last Name")) {
            isValid = false;
        }
        if (!FormViews.isValidEmail(emailEditText, "Email")) {
            isValid = false;
        }
        if (!FormViews.isValidWithMark(passwordEdittextText, "Password")) {
            isValid = false;
        }
        if (isValid) {
            sendRegistrationRequest();
        }
    }

    private void sendRegistrationRequest() {
        Call<UserAuthenticationResponse> callback = RestClient.get().registerUser(getRegistrationRequestObject());
        NetworkServiceHandler.processCallBack(callback, this);
    }

    private RegistrationRequest getRegistrationRequestObject() {

        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setFirstname(FormViews.getTexBoxFieldValue(firstNameEditText));
        registrationRequest.setLastname(FormViews.getTexBoxFieldValue(lastNameEditText));
        registrationRequest.setEmail(FormViews.getTexBoxFieldValue(emailEditText));
        registrationRequest.setPassword(FormViews.getTexBoxFieldValue(passwordEdittextText));

        return registrationRequest;
    }

    public void onEvent(UserAuthenticationResponse baseResponse) {
        Log.d("=====base theke===","----"+baseResponse.toString());
        if (baseResponse.getResponse_code() == 100) {
            showInfoSnackBar("Registration Successful.");
            preferenceService.SetPreferenceValue(PreferenceService.ENTITY_KEY,
                    baseResponse.getData().getEntity_id());
            preferenceService.SetPreferenceValue(PreferenceService.EMAIL_KEY,
                    baseResponse.getData().getEmail());
            preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, true);
            preferenceService.SetPreferenceValue(PreferenceService.FIRST_NAME_KEY, baseResponse.getData().getFirstname());
            preferenceService.SetPreferenceValue(PreferenceService.LAST_NAME_KEY, baseResponse.getData().getLastname());
            callCartMargeApi();
            pageContainer.setVisibility(View.GONE);

        }
    }

    @Override
    protected void unregisterBroadcast() {
        try {
            if (broadcast_reciever != null) {
                unregisterReceiver(broadcast_reciever);
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resourceId = v.getId();

        if (resourceId == R.id.btn_sign_in) {
            gotoNewActivity(UserLoginActivity.class);
        } else if (resourceId == R.id.btn_continue) {
            validateForm();
        }
    }

    @Override
    protected void refresh() {
        super.refresh();
        sendBroadcast(BroadCastUtils.RECREATE_VIEW);
    }
}

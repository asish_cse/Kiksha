package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by BS62 on 5/12/2016.
 */
public class OrderHistoryResponse extends BaseResponse {
    private List<OrderHistory> data;

    public List<OrderHistory> getData() {
        return data;
    }

    public void setData(List<OrderHistory> data) {
        this.data = data;
    }
}

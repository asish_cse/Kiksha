package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by Ashraful on 4/13/2016.
 */
public class ProductListResponse extends BaseResponse {

    public List<Product> getData() {
        return data;
    }

    public void setData(List<Product> data) {
        this.data = data;
    }

    private List<Product>data;
}

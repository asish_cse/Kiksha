package com.bs.ecommerce.kiksha.model;

/**
 * Created by Asish on 10/10/2015.
 */
public class CategoryOfferzoneResponse extends BaseResponse{
    public CategoryOfferZone getData() {
        return data;
    }

    public void setData(CategoryOfferZone data) {
        this.data = data;
    }

    private CategoryOfferZone data;

}

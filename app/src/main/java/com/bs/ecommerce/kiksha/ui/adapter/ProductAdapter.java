package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.NumberFormat;
import com.bs.ecommerce.kiksha.constant.ViewType;
import com.bs.ecommerce.kiksha.model.Product;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static org.acra.ACRA.log;

/**
 * Created by Ashraful on 11/12/2015.
 */
public class ProductAdapter extends RecyclerView.Adapter {
    private static final int VIEW_PROG = 4;
    public List<Product> products = new ArrayList<>();
    public int ViewFormat = ViewType.GRID;
    protected Context context;
    protected OnItemClickListener mItemClickListener;


    public ProductAdapter(Context context, List productsList) {
        try {

            this.products = new ArrayList<>();
            this.products = productsList;
            notifyDataSetChanged();

            this.context = context;
            Log.d("adapterSize", "" + productsList.size());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void swap(Context context, List<Product> productsList) {
        try {
            this.products.clear();
            this.products.addAll(productsList);
            /*this.products = new ArrayList<>();
            this.products = productsList;*/
            notifyDataSetChanged();

            this.context = context;
            Log.d("adapterSize", "" + productsList.size());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ProductAdapter(Context context, List productsList, int viewType) {
        this(context, productsList);
        ViewFormat = viewType;
    }

    public void clearData() {
        int size = this.products.size();
        this.products.clear();
        this.notifyItemRangeRemoved(0, size);

    }

    public void addAll(List<Product> products) {
        this.products.addAll(products);
    }

    public Product getItem(int position) {
        return products.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout = 0;
        if (viewType == ViewType.GRID)
            layout = R.layout.item_products_grid;
        else if (viewType == ViewType.GRID_FIRST_ROW)
            layout = R.layout.item_product_grid_first_row;
// android:background="@drawable/discount_text_border"
      /*  else if(viewType == ViewType.THREE_COLUMN_GRID)
            layout = R.layout.item_products_grid;
        else if(viewType == ViewType.THREE_COLUMN_GRID_FIRST_ROW)
            layout=R.layout.item_product_grid_first_row;*/
        else if (viewType == ViewType.THREE_COLUMN_GRID)
            layout = R.layout.item_products_three_column_grid;
        else if (viewType == ViewType.THREE_COLUMN_GRID_FIRST_ROW)
            layout = R.layout.item_products_three_grid_frist_row;


        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(layout, parent, false);

        return new ProductSummaryHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {


        return products.get(position) != null ? getViewType(position) : VIEW_PROG;


    }

    private int getViewType(int position) {
        if (ViewFormat == ViewType.GRID) {
            if (position < 2)
                return ViewType.GRID_FIRST_ROW;
            else
                return ViewType.GRID;
        } else {
            if (position < 3)
                return ViewType.THREE_COLUMN_GRID_FIRST_ROW;
            else
                return ViewType.THREE_COLUMN_GRID;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder bindViewHolder, final int position) {
        try {
            if (bindViewHolder instanceof ProductSummaryHolder) {
                Product product = products.get(position);
                final ProductSummaryHolder holder = (ProductSummaryHolder) bindViewHolder;
                holder.productName.setText(product.getName());
                // System.out.println(holder.productName.getText().toString() + "," + Product.getName());
                if (holder.getItemViewType() == ViewType.THREE_COLUMN_GRID || holder.getItemViewType() == ViewType.THREE_COLUMN_GRID_FIRST_ROW) {
                    holder.productPrice.setVisibility(View.GONE);
                }

                if (product.getPrice().equals(product.getFinal_price())) {
                    holder.productPrice.setVisibility(View.GONE);
                }
                holder.productPrice.setText("৳" + product.getPrice());
                String my_new_str = "";
                log.d("-->>" + product.getFinal_price(), "==||==");
//                my_new_str = product.getFinal_price().replaceAll("\"", " ");
//
//
//               double  d = 0.0 ;//Integer.parseInt(my_new_str);
//
//                try {
//                    d = NumberFormat.Format2Double(my_new_str);
//                    log.d("-->>"+d,"==||==");
//                } catch(NumberFormatException nfe) {
//                    log.d("-->>"+nfe,"==||==");
//                }
//                log.d("-->>"+d,"==||=="+product.getFinal_price());
//                if(d>=5000.00)
//                    holder.finalPriceTextView.setText("৳" + product.getFinal_price() +"\n"+"\nmonth on EMI");
//                else
                holder.finalPriceTextView.setText("৳" + product.getFinal_price());
                //=====================Discount===============================

                Double dis = 0.0, sp_price = 0.0, sp_dis = 0.0;

//
                Double priceM = NumberFormat.Format2Double(product.getPrice());

                Double gp_dis=0.0;
                if(product.getGroup_price()==null)
                     gp_dis = priceM ;
                else
                    gp_dis = priceM - NumberFormat.Format2Double(product.getGroup_price());
                sp_price = NumberFormat.Format2Double(product.getSpecial_price());
                if (sp_price > 0)
                    sp_dis = priceM - sp_price;
                if (gp_dis > 0 && sp_dis > 0) {
                    if (gp_dis > sp_dis) dis = (gp_dis / priceM) * 100;
                    else dis = (sp_dis / priceM) * 100;
                } else if (gp_dis > 0 && sp_dis <= 0) {
                    dis = (gp_dis / priceM) * 100;
                } else if (gp_dis <= 0 && sp_dis > 0) {
                    dis = (sp_dis / priceM) * 100;
                }



                if (dis > 0) {
                    Log.d("////", "///===");
                    if( holder.discount_item==null)
                        Log.d("////", "///==");
                    Log.d("////", ">>>>"+priceM);

                    holder.discount_item.setVisibility(View.VISIBLE);
                    holder.discount_item.setText("-" + NumberFormat.round(dis, 0) + "%");

                } else holder.discount_item.setVisibility(View.GONE);
                if(dis==100)holder.discount_item.setVisibility(View.GONE);

// /                holder.discount_item.setText(sp_dis+"");
                    /*holder.productShortDescriptionTextView.setText(

                        Html.fromHtml(product.getShort_description()));*/

                Picasso.with(context).load(product.getImage()).
                        fit().centerInside().into(holder.productImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.progressBar.setVisibility(View.GONE);

                    }
                });


            }


        } catch (ClassCastException ex) {

        }


    }


    @Override
    public int getItemCount() {
        if (products == null)
            return 0;
        return products.size();
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    public class ProductSummaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView productImage;
        protected TextView productPrice;
        protected TextView productName;
        protected TextView productShortDescriptionTextView;
        protected ProgressBar progressBar;
        protected TextView finalPriceTextView;
        protected TextView discount_item;

        public ProductSummaryHolder(View itemView) {
            super(itemView);
            productImage = (ImageView) itemView.findViewById(R.id.img_productImage);
            productPrice = (TextView) itemView.findViewById(R.id.tv_productPrice);
            productName = (TextView) itemView.findViewById(R.id.tv_productName);
            discount_item = (TextView) itemView.findViewById(R.id.discounttk);
            productShortDescriptionTextView = (TextView) itemView.findViewById(R.id.tv_product_short_description);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);
            finalPriceTextView = (TextView) itemView.findViewById(R.id.tv_productFinalPrice);
            productPrice.setPaintFlags(productPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }


        }

    }

}


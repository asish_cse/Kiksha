package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 4/27/2016.
 */
public class EmailChangeResponse extends BaseResponse {
    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

    private UserData data;
}

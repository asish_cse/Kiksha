package com.bs.ecommerce.kiksha.ui.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.CartDetailsResponse;
import com.bs.ecommerce.kiksha.model.CartItemChangeResponse;
import com.bs.ecommerce.kiksha.model.CartItems;
import com.bs.ecommerce.kiksha.model.CouponCodeAddRequest;
import com.bs.ecommerce.kiksha.model.CustomerSetRequest;
import com.bs.ecommerce.kiksha.model.CustomerSetResponse;
import com.bs.ecommerce.kiksha.model.MessageResponse;
import com.bs.ecommerce.kiksha.model.Product;
import com.bs.ecommerce.kiksha.model.SpecialCharge;
import com.bs.ecommerce.kiksha.model.SpecialChargeResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.adapter.ShoppingCartAdapter;
import com.bs.ecommerce.kiksha.ui.view.FormViews;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 5/2/2016.
 */
@ContentView(R.layout.activity_shopping_cart)
public class MyCartActivity extends BaseActivity {

    @InjectView(R.id.rclv_cart_product)
    RecyclerView cartProductRecyclerView;

    @InjectView(R.id.tv_cart_grand_total)
    TextView cartGrandTotal;

    @InjectView(R.id.tv_cart_grand_total_discount)
    TextView cartGrandDiscount;

    @InjectView(R.id.cost_id)
    CardView cost_layout;

    @InjectView(R.id.discount_title)
    TextView discount_price;

    List<CartItems> cartItemsList;
    private ShoppingCartAdapter shoppingCartAdapter;

    @InjectView(R.id.btn_clear_cart)
    Button clearCartBtn;

    @InjectView(R.id.btn_con_shopping)
    Button continueBtn;

    @InjectView(R.id.btn_checkout)
    Button checkoutBtn;

    @InjectView(R.id.total_cost_lay)
    RelativeLayout total_cost_layout;

    @InjectView(R.id.msg_id)
    TextView msg_txt;

    @InjectView(R.id.btn_apply_coupon)
    Button applyCouponBtn;
    @InjectView(R.id.et_coupon_code)
    EditText couponCodeEt;
    @InjectView(R.id.btn_call_to_order)
    Button callToOrderBtn;

    private static boolean needToReload = false;
    public  float discount_amount = 0;
    private List<SpecialCharge> specialChargeList;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Bag");
        initRecyclerView();
        getSpecialCharge();

    }

    @Override
    protected void onResume() {
        super.onResume();


        if (needToReload) {
            needToReload = false;
            onViewCreated();
        }
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        checkoutBtn.setOnClickListener(this);
        clearCartBtn.setOnClickListener(this);
        applyCouponBtn.setOnClickListener(this);
        callToOrderBtn.setOnClickListener(this);
        continueBtn.setOnClickListener(this);
    }

    protected void initRecyclerView() {

        cartProductRecyclerView.setLayoutManager(getLinearLayoutManager(LinearLayoutManager.VERTICAL));
        cartProductRecyclerView.setHasFixedSize(true);
        cartItemsList = new ArrayList<>();
        getCartDetails();
    }

    private void getCartDetails() {
        int cartId = getCartId();

        if (cartId != -1) {
            Call<CartDetailsResponse> callback = RestClient.get().getCartDetails(String.valueOf(cartId), "");
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }
    }

    private void getSpecialCharge() {
        NetworkServiceHandler.processCallBack(RestClient.get().getSpecialCharge(""), this);
    }

    public void onEvent(SpecialChargeResponse specialChargeResponse) {
        specialChargeList = new ArrayList<>();

        if (specialChargeResponse.getResponse_code() == 100) {
            specialChargeList = specialChargeResponse.getData();


        }

    }

    public void onEvent(CartDetailsResponse cartDetailsResponse) {
        updateRecyclerView(cartDetailsResponse);
    }

    public void showInfo() {
        AlertDialog alertDialog = new AlertDialog.Builder(MyCartActivity.this).create();
        alertDialog.setTitle("Cart");
        alertDialog.setMessage("There are no items in your cart");

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void showclearcart() {
//        AlertDialog alertDialog = new AlertDialog.Builder(MyCartActivity.this).create();
//        alertDialog.setTitle("Cart");
//        alertDialog.setMessage("Do you want to clear cart?");
//
//        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Yes",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                        callClearCartApi();
//                    }
//                });
//        alertDialog.show();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete Confirmation");
        builder.setMessage("Are you sure you want to clear cart?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                       // MyCartActivity.this.finish();
                        dialog.dismiss();
                        callClearCartApi();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void updateRecyclerView(CartDetailsResponse cartDetailsResponse) {
        if (cartDetailsResponse.getResponse_code() == 100 && cartDetailsResponse.getData().getItems() != null) {
            cartItemsList = cartDetailsResponse.getData().getItems();
            //
            float shippingCHAmountInside = 0;
            float shippingCHAmountOutside = 0;
            for (SpecialCharge spc : specialChargeList) {
                for (CartItems cartItem : cartItemsList) {
                    if (cartItem.getCatgory_id().contains(spc.getCategory_id())) {
                        if(shippingCHAmountInside < Float.valueOf(spc.getInside_dhaka()))
                        shippingCHAmountInside = Float.valueOf(spc.getInside_dhaka());
                        if(shippingCHAmountOutside < Float.valueOf(spc.getOutside_dhaka()))
                        shippingCHAmountOutside = Float.valueOf(spc.getOutside_dhaka());
                    }
                }

            }
            preferenceService.SetPreferenceValue(PreferenceService.SHIPPIN_IN_KEY, String.valueOf(shippingCHAmountInside) );
            preferenceService.SetPreferenceValue(PreferenceService.SHIPPING_OUT_KEY,String.valueOf(shippingCHAmountOutside) );
            Log.d("SpecialCharge", shippingCHAmountOutside+"SpecialCharge==" + shippingCHAmountInside);

            if (cartItemsList.size() < 1) {
                //showInfo();
                msg_txt.setVisibility(View.VISIBLE);
                applyCouponBtn.setVisibility(View.GONE);
                couponCodeEt.setVisibility(View.GONE);
                total_cost_layout.setVisibility(View.GONE);
                continueBtn.setVisibility(View.VISIBLE);

                cost_layout.setVisibility(View.GONE);
            } else {
                msg_txt.setVisibility(View.GONE);
               // cartGrandDiscount.setVisibility(View.VISIBLE);
              //  discount_price.setVisibility(View.VISIBLE);
                cost_layout.setVisibility(View.VISIBLE);
            }
            shoppingCartAdapter = new ShoppingCartAdapter(this, cartDetailsResponse.getData().getCart_id(), cartItemsList);
            cartProductRecyclerView.setAdapter(shoppingCartAdapter);
            badgeCount = Integer.valueOf(cartDetailsResponse.getData().getItems_count());
            updateCartItemCount(badgeCount);
            //           cartGrandTotal.setText("\u09f3 " + cartDetailsResponse.getData().getGrand_total());
            cartGrandTotal.setText("\u09f3 " + cartDetailsResponse.getData().getSubtotal_with_discount());
            for(CartItems cart:cartItemsList){
                discount_amount=discount_amount+Float.valueOf(cart.getDiscount_amount());
            }
            cartGrandDiscount.setText("-\u09f3 " + discount_amount);
            discount_amount=0;
            Log.d("--->>", "--total->" + cartDetailsResponse.getData().getGrand_total());
            CheckoutActivity.cartDetails = cartDetailsResponse.getData();
            if (cartDetailsResponse.getData().getItems().size() > 0) {
                checkoutBtn.setVisibility(View.VISIBLE);
                clearCartBtn.setVisibility(View.VISIBLE);
                callToOrderBtn.setVisibility(View.VISIBLE);
            } else {
                checkoutBtn.setVisibility(View.GONE);
                clearCartBtn.setVisibility(View.GONE);
                callToOrderBtn.setVisibility(View.GONE);
            }
        }
    }


    private void requestCoupon() {
        boolean isValid = true;
        if (!FormViews.isValidWithMark(couponCodeEt, "Coupon Code")) {
            isValid = false;
        }

        if (isValid) {
            callAddCouponCodeApi();
        }
    }

    private void callAddCouponCodeApi() {
        int cartId = getCartId();
        if (cartId != -1) {
            Call<MessageResponse> callback = RestClient.get().addCouponCode(getCouponCodeRequestObject(cartId));
            NetworkServiceHandler.processCallBack(callback, this);
        }

    }

    private CouponCodeAddRequest getCouponCodeRequestObject(int cartId) {
        CouponCodeAddRequest couponCodeAddRequest = new CouponCodeAddRequest();
        couponCodeAddRequest.setCartId(cartId);
        couponCodeAddRequest.setCode(couponCodeEt.getText().toString());
        return couponCodeAddRequest;
    }

    public void onEvent(MessageResponse messageResponse) {
        if (messageResponse.getResponse_code() == 100) {
            showInfoSnackBar(messageResponse.getData().getMsg());

        }
    }

    public void onEvent(CartItemChangeResponse cartItemChangeResponse) {
        if (cartItemChangeResponse.getResponse_code() == 100) {
            showInfoSnackBar(cartItemChangeResponse.getData().getMsg());
            updateRecyclerView(cartItemChangeResponse.getData().getCart());
        }
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resourceId = v.getId();

        if (resourceId == R.id.btn_con_shopping) {
            // callClearCartApi();
            //Toast.makeText(getApplication(),"Continue ",Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, HomePageActivity2.class);

            startActivity(i);
            this.finish();
        }

        if (resourceId == R.id.btn_clear_cart) {
            showclearcart();

        }

        if (resourceId == R.id.btn_checkout) {
            if (isLoggedIn()) {
                callSetCustomerApi();
            } else {
                needToReload = true;
                gotoNewActivity(UserRegisterActivity.class);
            }
            // gotoNewActivity(CheckoutActivity.class);
        }

        if (resourceId == R.id.btn_call_to_order) {
            callToOrderDialog();
        }


        if (resourceId == R.id.btn_apply_coupon) {
            requestCoupon();
        }
    }

    private void callClearCartApi() {
        int cart_id = getCartId();
        if (cart_id != -1) {
            Call<CartItemChangeResponse> callback = RestClient.get().emptyCart(String.valueOf(cart_id), "");
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }

    }

    private void callSetCustomerApi() {
        int cartId = getCartId();

        if (cartId != -1) {
            Call<CustomerSetResponse> callback = RestClient.get().setCustomer(getCustomerSetReqObject(cartId));
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }
    }

    private CustomerSetRequest getCustomerSetReqObject(int cartId) {
        CustomerSetRequest customerSetRequest = new CustomerSetRequest();
        customerSetRequest.setCartId(cartId);
        customerSetRequest.setCustomerId(Integer.valueOf(preferenceService.GetPreferenceValue(PreferenceService.ENTITY_KEY)));
        return customerSetRequest;
    }

    public void onEvent(CustomerSetResponse customerSetResponse) {
        if (customerSetResponse.getResponse_code() == 100) {
            gotoNewActivity(BillingAddressActivity.class);
        }
    }

    //=======================================================
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case 123:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onCall();
                } else {
                    Log.d("TAG", "Call Permission Not Granted");
                }
                break;

            default:
                break;
        }
    }

    public void onCallVer() {
        if (Build.VERSION.SDK_INT >= 23) {
            onCall();
        }
    }

    public void onCall() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.CALL_PHONE}, Integer.parseInt("123"));
        } else {
//        startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:12345678901")));
        }
    }


    //========================================================
    private void callToOrderDialog() {
        onCallVer();
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.call_to_order_dialog);
        final Button telephone = (Button) dialog.findViewById(R.id.btn_call_telephone);
        telephone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callToNumber(telephone.getText().toString());
            }
        });

        final Button mobile = (Button) dialog.findViewById(R.id.btn_call_mobile);
        mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callToNumber(mobile.getText().toString());
            }
        });

        final Button cancel = (Button) dialog.findViewById(R.id.btn_dialog_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void callToNumber(String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(callIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void gotoProductDetailsActivity(String name, String productId, int qty) {
        Product product = new Product();
        product.setName(name);
        product.setProductId(productId);
        ProductDetailsActivity.product = product;
        ProductDetailsActivity.qty = qty;
        gotoNewActivity(ProductDetailsActivity.class);
    }


}


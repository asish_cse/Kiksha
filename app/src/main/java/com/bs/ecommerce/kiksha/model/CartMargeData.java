package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/31/2016.
 */
public class CartMargeData {
    private String cartId;
    private String customerId;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}

package com.bs.ecommerce.kiksha.ui.adapter.interface_pack;

import android.view.View;

/**
 * Created by BS62 on 23-Sep-16.
 */

public interface OnItemClickListener {
    void onItemClick(View view, int position);
}

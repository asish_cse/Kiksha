package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 4/22/2016.
 */
public class UserInfoUpdateRequest {
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }



    private String  firstname;
    private String  lastname;
    private String  email;
    public String getEntity_id() {
        return entity_id;
    }

    public void setEntity_id(String entity_id) {
        this.entity_id = entity_id;
    }

    private String entity_id;
}

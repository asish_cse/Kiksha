package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/5/2016.
 */
public class Message {
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

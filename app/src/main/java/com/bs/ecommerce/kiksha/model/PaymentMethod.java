package com.bs.ecommerce.kiksha.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BS62 on 5/13/2016.
 */
public class PaymentMethod {
    private String code;
    private String title;
    private CcTypes cc_types;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CcTypes getCc_types() {
        return cc_types;
    }

    public void setCc_types(CcTypes cc_types) {
        this.cc_types = cc_types;
    }

    public List<String> getCcTypeList() {
        List<String> list = new ArrayList<>();
        list.add(cc_types.getAE());
        list.add(cc_types.getVI());
        list.add(cc_types.getMC());
        list.add(cc_types.getDI());
        return list;
    }
}

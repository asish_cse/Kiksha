package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/12/2016.
 */
public class OrderBillingAddress extends Address {
    private String billing_email;

    public String getBilling_email() {
        return billing_email;
    }

    public void setBilling_email(String billing_email) {
        this.billing_email = billing_email;
    }
}

package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 4/21/2016.
 */
public class RegistrationRequest {

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String  firstname;
    private String  lastname;
    private String  email;
    private String  password;
}

package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.NumberFormat;
import com.bs.ecommerce.kiksha.model.OrderDetailsItem;

import java.util.List;

/**
 * Created by BS62 on 5/12/2016.
 */
public class OrderDetailsAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    List<OrderDetailsItem> orderDetailsItems;
    Context context;

    public OrderDetailsAdapter(Context context, List<OrderDetailsItem> orderDetailsItems) {
        this.orderDetailsItems = orderDetailsItems;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return orderDetailsItems.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        View rowView = convertView;
        if (convertView == null) {
            rowView = inflater.inflate(R.layout.item_order_review, null);
            holder = new Holder();

            holder.productNameTv = (TextView) rowView.findViewById(R.id.tv_product_name);
            holder.productPriceTv = (TextView) rowView.findViewById(R.id.tv_product_price);
            holder.productQtyTv = (TextView) rowView.findViewById(R.id.tv_product_qty);
            holder.productSubtotalTv = (TextView) rowView.findViewById(R.id.tv_product_subtotal);

            rowView.setTag(holder);
        } else {
            holder = (Holder) rowView.getTag();
        }


        holder.productNameTv.setText(orderDetailsItems.get(position).getItem_name());
        holder.productPriceTv.setText("\u09f3 " + orderDetailsItems.get(position).getItem_price());
        holder.productQtyTv.setText(orderDetailsItems.get(position).getQty() + "");
        Double productSubtotal= NumberFormat.Format2Double(orderDetailsItems.get(position).getItem_price())*orderDetailsItems.get(position).getQty();
        holder.productSubtotalTv.setText("\u09f3 " + NumberFormat.Double2Format(productSubtotal));

        return rowView;
    }

    public static class Holder {
        TextView productNameTv;
        TextView productPriceTv;
        TextView productQtyTv;
        TextView productSubtotalTv;
    }
}

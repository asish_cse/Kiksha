package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/4/2016.
 */
public class City {
    private String city;
    private String area_list;
    private String shipping_charge;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea_list() {
        return area_list;
    }

    public void setArea_list(String area_list) {
        this.area_list = area_list;
    }

    public String getShipping_charge() {
        return shipping_charge;
    }

    public void setShipping_charge(String shipping_charge) {
        this.shipping_charge = shipping_charge;
    }
}

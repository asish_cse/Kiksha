package com.bs.ecommerce.kiksha.constant;

/**
 * Created by Ashraful on 11/12/2015.
 */
public class ViewType {
    public static final int GRID=0;
    public static final int GRID_FIRST_ROW=10;
    public static final int THREE_COLUMN_GRID=1;
    public static final int THREE_COLUMN_GRID_FIRST_ROW=11;
    public static final int LIST=2;

    public static final int SINGLE=3;
    public static final int HOMEPAGEVIEW=4;


}

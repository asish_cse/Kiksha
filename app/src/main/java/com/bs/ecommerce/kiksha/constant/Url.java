package com.bs.ecommerce.kiksha.constant;
/**
 * Created by Ashraful on 3/31/2016.
 */

public class Url {
    public static String BASE_URL = "https://kiksha.com/restapi/index.php/";
   public static String homeslider_URL = "https://kiksha.com/restapi/index.php/home_category/getHomeslider";
    public static String BESTSELLER_URL = "https://kiksha.com/restapi/index.php/home_category/getBestSoldProducts";

    public static String CATEGORY_URL = "https://kiksha.com/restapi/index.php/search";

    public static String OFFERZONE_URL = "https://kiksha.com/restapi/index.php/category_offer/offer_zone";

    public static final String HOME_DELIVERY = "https://kiksha.com/delivery.html";
    public static final String RETURN_REFUND = "https://kiksha.com/return.html";
    public static final String APP_INTRO = "https://kiksha.com/appintro.html";
    public static final String FAQ = "https://kiksha.com/app-page/faq.html";
    public static final String PRIVACY = "https://kiksha.com/app-page/privacy.html";
     public static final String TERMS = "https://kiksha.com/app-page/terms.html";
    //public static final String TERMS = "https://kiksha.com/terms/";

    public static final String CONTACT_US = "https://kiksha.com/app-page/contact-us.html";

    public static final String FB_NAME = "kikshadotcom";
    public static final String KIKSHA_FB = "https://www.facebook.com/" + FB_NAME;
}

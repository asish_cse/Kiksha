package com.bs.ecommerce.kiksha.network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ashraful on 11/5/2015.
 */

public class RestError
{
    @SerializedName("code")
    private Integer code;

    @SerializedName("error_message")
    private String strMessage;

    public RestError(String strMessage)
    {
        this.strMessage = strMessage;
    }

    //Getters and setters
}
package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by BS62 on 5/9/2016.
 */
public class CartDetails {
    private String cart_id;
    private String items_qty;
    private String items_count;
    private String store_currency_code;
    private String quote_currency_code;
    private String subtotal;
    private String subtotal_with_discount;
    // private Object coupon_code;
    private String grand_total;
    private List<CartItems> items;


    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public String getItems_qty() {
        return items_qty;
    }

    public void setItems_qty(String items_qty) {
        this.items_qty = items_qty;
    }

    public String getItems_count() {
        return items_count;
    }

    public void setItems_count(String items_count) {
        this.items_count = items_count;
    }

    public String getStore_currency_code() {
        return store_currency_code;
    }

    public void setStore_currency_code(String store_currency_code) {
        this.store_currency_code = store_currency_code;
    }

    public String getQuote_currency_code() {
        return quote_currency_code;
    }

    public void setQuote_currency_code(String quote_currency_code) {
        this.quote_currency_code = quote_currency_code;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getSubtotal_with_discount() {
        return subtotal_with_discount;
    }

    public void setSubtotal_with_discount(String subtotal_with_discount) {
        this.subtotal_with_discount = subtotal_with_discount;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }

    public List<CartItems> getItems() {
        return items;
    }

    public void setItems(List<CartItems> items) {
        this.items = items;
    }
}

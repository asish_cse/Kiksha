package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by Ashraful on 5/11/2016.
 */
public class FilterItemListResponse extends BaseResponse {

    public List<FilterItem> getData() {
        return data;
    }

    public void setData(List<FilterItem> data) {
        this.data = data;
    }

    private List<FilterItem>data;
}

package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 5/10/2016.
 */
public class PasswordResetRequest {
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private  String email;
}

package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.SharedPreferencesHelper;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.model.Homedata2;
import com.bs.ecommerce.kiksha.model.Product;
import com.bs.ecommerce.kiksha.ui.activity.BaseActivity;
import com.bs.ecommerce.kiksha.ui.activity.ProductDetailsActivity;
import com.bs.ecommerce.kiksha.ui.activity.ProductListActivity;
import com.bs.ecommerce.kiksha.ui.activity.SearchableActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyContactAdapter extends ArrayAdapter<Homedata2> {

    List<Homedata2> contactList;
    Context context;
    private LayoutInflater mInflater;
    GridLayoutManager bottomGridLayoutManager;
  //  protected OnItemClickListener mItemClickListener;

    // Constructors
    public MyContactAdapter(Context context, List<Homedata2> objects) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        contactList = objects;
    }

    @Override
    public Homedata2 getItem(int position) {
        return contactList.get(position);
    }

//    public interface OnItemClickListener {
//        public void onItemClick(View view, int position);
//    }
//
//    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
//        this.contactList = mItemClickListener;
//    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
       bottomGridLayoutManager = new GridLayoutManager(context, 2);
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.layout_row_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        Homedata2 item = getItem(position);

        vh.textViewName.setText(item.getName().trim());
        vh.tv1.setText(item.getItems().get(0).getName());
        vh.tv11.setText(item.getItems().get(0).getTitle());
        vh.tv2.setText(item.getItems().get(1).getName());
        vh.tv22.setText(item.getItems().get(1).getTitle());

        vh.tv3.setText(item.getItems().get(2).getName());
        vh.tv33.setText(item.getItems().get(2).getTitle());
        vh.tv4.setText(item.getItems().get(3).getName());
        vh.tv44.setText(item.getItems().get(3).getTitle());

        Log.d("===name="+item.getName(),"----");
//        vh.textViewEmail.setText(item.getEmail());
//        Picasso.with(context).load(item.getProfilePic()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(vh.imageView);
        vh.textViewName.setTag(vh);
   //     convertView.setTag(vh);
//        vh.textViewName.setOnClickListener(new View.OnClickListener() {
//            int count=0;
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                View p = (View) v.getParent();
//                ViewHolder holder1 = (ViewHolder) v.getTag();
//              //  count = Integer.valueOf(holder1.textViewName.getText().toString());
//                //count++;
//                holder1.textViewName.setText("okay");
//
//
//
//            }
//        });
        int i =position;
        if(i==0) {
            vh.separator.setBackgroundResource(R.color.cat1);
    //        vh.textViewName.setBackgroundResource(R.color.cat1);
            vh.textViewName.setTextColor(context.getResources().getColor(R.color.cat1));
        }
        else if(i==1) {
            vh.separator.setBackgroundResource(R.color.cat2);
         //   vh.textViewName.setBackgroundResource(R.color.cat2);
            vh.textViewName.setTextColor(context.getResources().getColor(R.color.cat2));
        }
        else if(i==2) {
            vh.separator.setBackgroundResource(R.color.cat3);
          //  vh.textViewName.setBackgroundResource(R.color.cat3);
            vh.textViewName.setTextColor(context.getResources().getColor(R.color.cat3));
        }
        else if(i==3) {
            vh.separator.setBackgroundResource(R.color.cat4);
          //  vh.textViewName.setBackgroundResource(R.color.cat4);
            vh.textViewName.setTextColor(context.getResources().getColor(R.color.cat4));
        }
        else if(i==4) {
            vh.separator.setBackgroundResource(R.color.cat5);
           // vh.textViewName.setBackgroundResource(R.color.cat5);
            vh.textViewName.setTextColor(context.getResources().getColor(R.color.cat5));
        }
        else if(i==5) {
            vh.separator.setBackgroundResource(R.color.cat6);
     //       vh.textViewName.setBackgroundResource(R.color.cat6);
            vh.textViewName.setTextColor(context.getResources().getColor(R.color.cat6));
        }
        else if(i==6) {
            vh.separator.setBackgroundResource(R.color.cat7);
    //        vh.textViewName.setBackgroundResource(R.color.cat7);
            vh.textViewName.setTextColor(context.getResources().getColor(R.color.cat7));
        }
        else if(i==7) {
            vh.separator.setBackgroundResource(R.color.cat8);
  //          vh.textViewName.setBackgroundResource(R.color.cat8);
            vh.textViewName.setTextColor(context.getResources().getColor(R.color.cat8));
        }





  //      vh.textViewEmail.setText(item.getParent_id());
    //    Picasso.with(context).load(item.getChildren().placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(vh.imageView);

        Picasso.with(context).load(item.getBanner().getImage_file()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(vh.imageView);

        Log.d("===image file="+item.getItems().get(0).getImage_file(),"----");
        Picasso.with(context).load(item.getItems().get(0).getImage_file()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(vh.imageView11);
        Picasso.with(context).load(item.getItems().get(1).getImage_file()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(vh.imageView12);
        Picasso.with(context).load(item.getItems().get(2).getImage_file()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(vh.imageView21);
        Picasso.with(context).load(item.getItems().get(3).getImage_file()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(vh.imageView22);



        vh.imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Homedata2 item = getItem(position);
                Log.d("===image click ok="+item.getBanner().getCategory_id(),"--prnt id--"+item.getBanner().getParent_id());
                Log.d("===position pos="+position,"-- id--");
                if(position == 4){
                    SharedPreferencesHelper.setMenu(context, "8");
                }else{
                    SharedPreferencesHelper.setMenu(context, "99");
                }

                if(item.getBanner().getBanner_type().trim().equalsIgnoreCase("search")){
                    Log.d("===image click ok="+item.getBanner().getBanner_type(),"--prnt id--");
//                    Category category = new Category();
//                    category.setCategory_id(item.getBanner().getCategory_id());
//                    category.setName(item.getBanner().getName());
//                    ProductListActivity.category = category;
//                    //gotoNewActivity(ProductListActivity.class);
//                    Intent intent = new Intent(context, ProductListActivity.class);
//                    context.startActivity(intent);

                    Intent intent = new Intent(context, SearchableActivity.class);
                    intent.putExtra("query", item.getBanner().getCategory_id());
                    context.startActivity(intent);


                }
                else if(item.getBanner().getBanner_type().trim().equalsIgnoreCase("product")){
                    Log.d("===image click ok="+item.getBanner().getBanner_type(),"--prnt id--");

                   // ProductDetailsActivity.product=item.getBanner().getID();
//                    Category category = new Category();
//                    category.setCategory_id(item.getBanner().getID());
//                    category.setName(item.getBanner().getName());
//                    ProductListActivity.category = category;
//                    //gotoNewActivity(ProductListActivity.class);
//                    Intent intent = new Intent(context, ProductListActivity.class);
//                    context.startActivity(intent);

                    Product product = new Product();
                    product.setName(item.getBanner().getName());
                    product.setProductId(item.getBanner().getID());
                    ProductDetailsActivity.product = product;
                    ProductDetailsActivity.qty = 1;
                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    context.startActivity(intent);

                }
                else {
                    Category category = new Category();
                    category.setCategory_id(item.getBanner().getCategory_id());
                    category.setName(item.getBanner().getName());
                    ProductListActivity.category = category;
                    //gotoNewActivity(ProductListActivity.class);
                    Intent intent = new Intent(context, ProductListActivity.class);
                    context.startActivity(intent);
                }

            }
        });

        vh.imageView11.setOnClickListener(new View.OnClickListener() {
            int count=0;

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//                View p = (View) v.getParent();
//                ViewHolder holder1 = (ViewHolder) v.getTag();
//                //  count = Integer.valueOf(holder1.textViewName.getText().toString());
//                //count++;
                Homedata2 item = getItem(position);
                Log.d("===image click ok="+item.getItems().get(0).getCategory_id(),"--prnt id--"+item.getItems().get(0).getParent_id());


//                Category category = new Category();
//                category.setCategory_id(item.getItems().get(0).getCategory_id());
//                category.setName(item.getItems().get(0).getTitle());
//                ProductListActivity.category = category;
//                //gotoNewActivity(ProductListActivity.class);
//                Intent intent = new Intent(context, ProductListActivity.class);
//                context.startActivity(intent);
               //  context.overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);

                nextActivity(position,0);

            }
        });
        vh.imageView12 .setOnClickListener(new View.OnClickListener() {
            int count=0;

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//                View p = (View) v.getParent();
//                ViewHolder holder1 = (ViewHolder) v.getTag();
//                //  count = Integer.valueOf(holder1.textViewName.getText().toString());
//                //count++;
                Homedata2 item = getItem(position);
                Log.d("===image click ok="+item.getItems().get(1).getCategory_id(),"--prnt id--"+item.getItems().get(0).getParent_id());


//                Category category = new Category();
//                category.setCategory_id(item.getItems().get(1).getCategory_id());
//                category.setName(item.getItems().get(1).getTitle());
//                ProductListActivity.category = category;
//                //gotoNewActivity(ProductListActivity.class);
//                Intent intent = new Intent(context, ProductListActivity.class);
//                context.startActivity(intent);
                //  context.overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);

                nextActivity(position,1);

            }
        });

        vh.imageView21 .setOnClickListener(new View.OnClickListener() {
            int count=0;

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                nextActivity(position,2);

            }
        });
        vh.imageView22 .setOnClickListener(new View.OnClickListener() {
            int count=0;

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                nextActivity(position,3);

            }
        });

        return vh.rootView;
    }


    private void nextActivity(int position, int pos){
        Homedata2 item = getItem(position);
        Log.d("===image click ok="+item.getItems().get(pos).getCategory_id(),"--prnt id--"+item.getItems().get(pos).getParent_id());
        SharedPreferencesHelper.setMenu(context, "99");

        Category category = new Category();
        category.setCategory_id(item.getItems().get(pos).getCategory_id());
        category.setName(item.getItems().get(pos).getName());
        ProductListActivity.category = category;
        //gotoNewActivity(ProductListActivity.class);
        Intent intent = new Intent(context, ProductListActivity.class);
        context.startActivity(intent);
    }

    private static class ViewHolder  {
        public final RelativeLayout rootView;
        public final ImageView imageView;
        public final ImageView imageView11;
        public final ImageView imageView12;
        public final ImageView imageView21;
        public final ImageView imageView22;
        public final TextView textViewName;
 //       public final TextView textViewEmail;
        public final View separator;
        public final TextView tv1;
        public final TextView tv2;
        public final TextView tv11;
        public final TextView tv22;
        public final TextView tv3;
        public final TextView tv4;
        public final TextView tv33;
        public final TextView tv44;



    //    public final RecyclerView featuredCategoryRecyclerViewBottom;

        private ViewHolder(RelativeLayout rootView, ImageView imageView, TextView textViewName,
                           TextView tv1, TextView tv2, TextView tv11, TextView tv22,
                           TextView tv3, TextView tv4, TextView tv33, TextView tv44,
                           ImageView imageView11, ImageView imageView12, ImageView imageView21, ImageView imageView22, View separator) {
            this.rootView = rootView;
            this.imageView = imageView;
            this.textViewName = textViewName;
 //           this.textViewEmail = textViewEmail;
            this.imageView11 = imageView11;
            this.imageView21 = imageView21;
            this.imageView12 = imageView12;
            this.imageView22 = imageView22;
            this.separator = separator;
            this.tv1 = tv1;
            this.tv2 = tv2;
            this.tv11 = tv11;
            this.tv22 = tv22;
            this.tv3 = tv3;
            this.tv4 = tv4;
            this.tv33 = tv33;
            this.tv44 = tv44;
           // this.featuredCategoryRecyclerViewBottom = featuredCategoryRecyclerViewBottom;
        }

        public static ViewHolder create(RelativeLayout rootView) {

            ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
            TextView textViewName = (TextView) rootView.findViewById(R.id.textViewName);
          //  TextView textViewEmail = (TextView) rootView.findViewById(R.id.textViewEmail);
          //  RecyclerView featuredCategoryRecyclerViewBottom=(RecyclerView)rootView.findViewById(R.id.featured_category_rclv_bottom);
            ImageView imageView11 = (ImageView) rootView.findViewById(R.id.imageView11);
            ImageView imageView12 = (ImageView) rootView.findViewById(R.id.imageView12);
            ImageView imageView21 = (ImageView) rootView.findViewById(R.id.imageView21);
            ImageView imageView22 = (ImageView) rootView.findViewById(R.id.imageView22);
            View separator = (View) rootView.findViewById(R.id.separator);
            TextView tv1 = (TextView) rootView.findViewById(R.id.tv1);
            TextView tv2 = (TextView) rootView.findViewById(R.id.tv2);
            TextView tv11 = (TextView) rootView.findViewById(R.id.tv11);
            TextView tv22 = (TextView) rootView.findViewById(R.id.tv22);
            TextView tv3 = (TextView) rootView.findViewById(R.id.tv3);
            TextView tv4 = (TextView) rootView.findViewById(R.id.tv4);
            TextView tv33 = (TextView) rootView.findViewById(R.id.tv33);
            TextView tv44 = (TextView) rootView.findViewById(R.id.tv44);
            return new ViewHolder(rootView, imageView, textViewName,
                    tv1,  tv2,  tv11,  tv22,
                     tv3,  tv4,  tv33, tv44,
                    imageView11,imageView12,imageView21,imageView22,separator);
        }

//        @Override
//        public void onClick(View v) {
//
//            if (mItemClickListener != null) {
//                mItemClickListener.onItemClick(v, getAdapterPosition());
//            }
//        }
    }
}

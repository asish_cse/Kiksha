package com.bs.ecommerce.kiksha.ui.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.SharedPreferencesHelper;
import com.bs.ecommerce.kiksha.model.CartAddRequest;
import com.bs.ecommerce.kiksha.model.CartCreateResponse;
import com.bs.ecommerce.kiksha.model.CartItemChangeResponse;
import com.bs.ecommerce.kiksha.model.Product;
import com.bs.ecommerce.kiksha.model.ProductDetail;
import com.bs.ecommerce.kiksha.model.ProductDetailResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.adapter.RelatedProductAdapter;
import com.bs.ecommerce.kiksha.ui.view.ProductDynamicAttribute;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by Ashraful on 4/15/2016.
 */
@ContentView(R.layout.activity_product_detail)
public class ProductDetailsActivity extends BaseActivity {

    PhotoViewAttacher photoViewAttacher ;

    @InjectView(R.id.ll_attribute)
    android.widget.LinearLayout attributeLinearLayout;

    @InjectView(R.id.tv_product_name)
    TextView productNameTextView;

    @InjectView(R.id.tv_product_price)
    TextView productPriceTextView;

    @InjectView(R.id.tv_productFinalPrice)
    TextView finalPriceTextView;

    @InjectView(R.id.tv_flower_conditions)
    TextView tv_flower_conditions;

    @InjectView(R.id.slider)
    SliderLayout sliderLayout;

    @InjectView(R.id.custom_indicator)
    PagerIndicator pagerIndicator;

    @InjectView(R.id.ll_related_product)
    ViewGroup relatedProductLayout;

    @InjectView(R.id.rclv_related_products)
    RecyclerView relatedProductRecyclerView;

    @InjectView(R.id.btn_description)
    Button descriptionBtn;

//    @InjectView(R.id.btn_emi)
//    Button emi;
//
//    @InjectView(R.id.list_emi)
//    ListView list_emi;

    /*@InjectView(R.id.tv_description)
    TextView descriptionTextView;*/

    @InjectView(R.id.add_to_bag_btn)
    Button addToBagBtn;


    @InjectView(R.id.btn_con_shopping)
    Button conBtnShopping;

    @InjectView(R.id.btn_privacy_policy)
    Button privacyPolicyBtn;

    @InjectView(R.id.ll_quantity)
    LinearLayout quantityLayout;

    @InjectView(R.id.sp_quantity_selector)
    Spinner quantitySelector;

    @InjectView(R.id.wv_description)
    WebView descriptionWv;

    @InjectView(R.id.btn_home_delivery)
    Button homeDeliverButton;
    @InjectView(R.id.btn_refund)
    Button refundButton;

    @InjectView(R.id.tv_out_of_stock)
    TextView outOfStockTextView;

    public static Product product;

    boolean isDescriptionShown=false;
    boolean isEmiShown=false;
    private ProductDynamicAttribute dynamicAttribute;
    private RelatedProductAdapter relatedProductAdapter;

    public static int qty = 1;

    private int emi_price = 0;
    private static String[] MOBILE_MODELS = {"EMI","EMI","EMI","EMI","EMI","EMI","EMI"};

    @Override
    protected void onViewCreated() {
        super.onViewCreated();

        relatedProductRecyclerView.setLayoutManager(getLinearLayoutManager());
        String str = product.getName();
        if (product.getName() == null) {
            setToolbarTitle("Kiksha");
        }
        else if (product.getName() != null) {
            setToolbarTitle(product.getName());
           // GalleryActivity2.product = product.getName();
        } else {
            setToolbarTitle("Kiksha");
        }
        descriptionWv.setBackgroundColor(0);
        //setProductNamePrice();
        callProductDetailsApi();
        showQuantityLayout(false);



    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        descriptionBtn.setOnClickListener(this);
        //emi.setOnClickListener(this);
        addToBagBtn.setOnClickListener(this);
        privacyPolicyBtn.setOnClickListener(this);
        homeDeliverButton.setOnClickListener(this);
        refundButton.setOnClickListener(this);
        conBtnShopping.setOnClickListener(this);
    }

    @Override
    protected void onStop() {
        //sliderLayout.removeAllSliders();
        sliderLayout.stopAutoCycle();
        super.onStop();
    }

    private void setProductNamePrice(String name, String price, String finalPrice, String description) {
        setToolbarTitle(name);
        productNameTextView.setText(name);
        productPriceTextView.setText("৳" + price);
        productPriceTextView.setPaintFlags(productPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

       String temp = SharedPreferencesHelper.getMenu(this);

        if(temp.equalsIgnoreCase("8")) {
            tv_flower_conditions.setVisibility(View.VISIBLE);
            tv_flower_conditions.setText("Flower Delivery Timing" +
                    "\nSame day : Please, place your order from Saturday to Thursday" +
                    "\nbetween 08:00 AM - 04:00 PM" +
                    "\nAn order might take minimum 04 hours to be delivered." +
                    "\nDelivery Hour : 10.00 am - 08.00 pm");
        }
//        else
            finalPriceTextView.setText("৳" + finalPrice);


//        emi_price = Integer.parseInt(price);
 //       emi_price = emi_price/3;

        if (price.equals(finalPrice)) {
            productPriceTextView.setVisibility(View.GONE);
        }


        /*Spanned htmlToSpanned = Html.fromHtml(description);
        descriptionTextView.setText(htmlToSpanned);*/
        descriptionWv.loadDataWithBaseURL(null, description, "text/html", "utf-8", null);
    }

    private void setProductNamePrice()
    {
        productNameTextView.setText(product.getName());
        productPriceTextView.setText("\u09f3" + product.getPrice());
        /*descriptionTextView.setText(Html.fromHtml(
                product.getShort_description()));*/
        descriptionWv.loadDataWithBaseURL(null, product.getShort_description(), "text/html", "utf-8", null);

    }
    private void callProductDetailsApi()
    {
        NetworkServiceHandler.processCallBack(
                RestClient.get().getProductDetail(product.getProductId()), this
        );
    }

    public void onEvent(ProductDetailResponse productDetailResponse)
    {
        ProductDetail productDetail=productDetailResponse.getData();
        if (productDetail != null) {
            if (productDetail.getQty() == 0) {
                addToBagBtn.setVisibility(View.GONE);
                outOfStockTextView.setVisibility(View.VISIBLE);
            } else {
                addToBagBtn.setVisibility(View.VISIBLE);
                outOfStockTextView.setVisibility(View.GONE);
            }
            setProductNamePrice(productDetail.getName(), productDetail.getPrice(), productDetail.getFinal_price(), productDetail.getDescription());
            dynamicAttribute = new ProductDynamicAttribute(this, attributeLinearLayout,
                    productDetail.getDynamic_attribute());
            attributeLinearLayout.removeAllViews();
            dynamicAttribute.generateView();
            showQuantityLayout(true);

            setImageSlider(productDetail.getImages());

            if (productDetail.getRelated_product().size() > 0)
                setRelatedProduct(productDetail);
            /*descriptionTextView.setText(Html.fromHtml(
                productDetail.getDescription()));*/
            descriptionWv.loadDataWithBaseURL(null, productDetail.getDescription(), "text/html", "utf-8", null);
        }
    }

    private void showQuantityLayout(boolean isShowQuantity) {
        if (isShowQuantity) {
            quantityLayout.setVisibility(View.VISIBLE);
        } else {
            quantityLayout.setVisibility(View.GONE);
        }
    }

    private void setImageSlider(List<String>imageList)
    {
        sliderLayout.removeAllSliders();
        sliderLayout.setCustomIndicator(pagerIndicator);
        sliderLayout.setDuration(5000);
        sliderLayout.stopAutoCycle();

        final ArrayList<String> images = new ArrayList<String>();

        Log.d("----->>"+imageList.size(), "image list");
        if(imageList.size()>0)
            imageList.remove(0);

        int i = 0;
        for(String url:imageList)
        {
            images.add(url);
            Log.d("===>>>"+imageList.get(i),"66");
//            if(i!=0)
//                images.add(url);
//            else
//                i++;

        }

        for(String url:imageList)
        {
            DefaultSliderView textSliderView=new DefaultSliderView (this);
            textSliderView.image(url)
                    .setScaleType(BaseSliderView.ScaleType.CenterInside);

            Log.d("==>>"+url,"");
          //  images.add(url);

            textSliderView.image(url)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                           // Toast.makeText(getApplicationContext(), "clicked image=", Toast.LENGTH_SHORT).show();

//                            Intent imgzoom = new Intent(getApplicationContext(),ImagezoomActivity.class);
//                            startActivity(imgzoom);


                            Intent intent = new Intent(ProductDetailsActivity.this, GalleryActivity2.class);
                            intent.putStringArrayListExtra(GalleryActivity2.EXTRA_NAME, images);
                            startActivity(intent);

                        }
                    });

            sliderLayout.addSlider(textSliderView);
            sliderLayout.setDuration(5000);
         //   photoViewAttacher = new PhotoViewAttacher(pagerIndicator);

           // photoViewAttacher.update();
        }
    }

    private void setRelatedProduct(ProductDetail productDetail)
    {
        relatedProductLayout.setVisibility(View.VISIBLE);
        relatedProductAdapter = new RelatedProductAdapter(this,productDetail.getRelated_product());
        relatedProductRecyclerView.setAdapter(relatedProductAdapter);

        relatedProductAdapter.SetOnItemClickListener(new RelatedProductAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                ProductDetailsActivity.product = relatedProductAdapter.getItem(position);
                onViewCreated();
            }
        });
    }

    @Override
    public void onClick(View v)
    {
       super.onClick(v);
        int resourceId=v.getId();
        if(resourceId==R.id.btn_description)
        {
           if(!isDescriptionShown)
           {
               //descriptionTextView.setVisibility(View.VISIBLE);
               descriptionWv.setVisibility(View.VISIBLE);
               isDescriptionShown=true;
           }
            else {
               //descriptionTextView.setVisibility(View.GONE);
               descriptionWv.setVisibility(View.GONE);
               isDescriptionShown = false;
           }
        }

//        if(resourceId==R.id.btn_emi)
//        {
//           // emi.setText(""+emi_price);
//            if(!isEmiShown)
//            {
//                //descriptionTextView.setVisibility(View.VISIBLE);
//                list_emi.setVisibility(View.VISIBLE);
//                isEmiShown=true;
//                list_emi.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,MOBILE_MODELS));
//            }
//            else {
//                //descriptionTextView.setVisibility(View.GONE);
//                list_emi.setVisibility(View.GONE);
//                isEmiShown=false;
//            }
//        }

        if (resourceId == R.id.btn_con_shopping) {
           this.finish();
            //callAddToCartApi();
        }

        if (resourceId == R.id.add_to_bag_btn) {
            addToBag();
            //callAddToCartApi();
        }

        if (resourceId == R.id.btn_privacy_policy) {
            Intent intent = new Intent(this, WebActivity.class);
            intent.putExtra(WebActivity.INTENT_EXTRA_WEB_TYPE, WebActivity.TYPE_PRIVACY_POLICY);
            startActivity(intent);
        }

        if (resourceId == homeDeliverButton.getId()) {
            Intent intent = new Intent(this, WebActivity.class);
            intent.putExtra(WebActivity.INTENT_EXTRA_WEB_TYPE, WebActivity.TYPE_HOME_DELIVERY);
            startActivity(intent);
        }

        if (resourceId == refundButton.getId()) {
            Intent intent = new Intent(this, WebActivity.class);
            intent.putExtra(WebActivity.INTENT_EXTRA_WEB_TYPE, WebActivity.TYPE_RETURN_REFUND);
            startActivity(intent);
        }
    }

    private void addToBag() {
        int cart_id = getCartId();
        Log.d("-->.cart id",">>>add to cart in product details activity"+cart_id);
        if (cart_id != -1) {
            callAddToCartApi(cart_id);
        } else {
            //createNewCart();
            Call<CartCreateResponse> callback = RestClient.get().createNewCart("");
            NetworkServiceHandler.processCallBack(callback, this);
        }
    }

    private void callAddToCartApi(int cart_id) {
        Log.d("-->.add to cart",">>>add to cart here");
        Call<CartItemChangeResponse> callback = RestClient.get().addToCart(getCartAddRequestObj(cart_id));
        NetworkServiceHandler.processCallBack(callback, this);
        Log.d("-->.add to cart",">>>add to cart here");
    }

    public void onEvent(CartCreateResponse cartCreateResponse) {

        if (cartCreateResponse.getResponse_code() == 100) {
            int cartID = cartCreateResponse.getData().getCart_id();
            preferenceService.SetPreferenceValue(PreferenceService.CART_KEY, cartID);
            callAddToCartApi(cartID);
        }
    }


    private CartAddRequest getCartAddRequestObj(int cart_id) {
        CartAddRequest cartAddRequest = new CartAddRequest();
        cartAddRequest.setCart_id(cart_id);
        cartAddRequest.setProduct_id(Integer.valueOf(product.getProductId()));
        cartAddRequest.setQty(Integer.valueOf(quantitySelector.getSelectedItem().toString()));
        cartAddRequest.setAttribute(dynamicAttribute.getAttributeList());

        return cartAddRequest;
    }

    public void onEvent(CartItemChangeResponse cartItemChangeResponse) {
        if (cartItemChangeResponse.getResponse_code() == 100) {
            showInfoSnackBar(cartItemChangeResponse.getData().getMsg());
            badgeCount = Integer.valueOf(cartItemChangeResponse.getData().getCart().getData().getItems_count());
            updateCartItemCount(badgeCount);
        }
    }

    /*public void onEvent(MessageResponse messageResponse) {
        if (messageResponse.getResponse_code() == 100) {
            showInfoSnackBar(messageResponse.getData().getMsg());
            callCartInfoApi();
        }
    }*/

    /*private void callCartInfoApi() {
        int cartId = getCartId();

        if (cartId != -1) {
            Call<CartDetailsResponse> callback = RestClient.get().getCartInfo(String.valueOf(cartId));
            NetworkServiceHandler.processCallBackWithoutProgressDialog(callback, this);
        }
    }*/

    /*public void onEvent(CartDetailsResponse cartDetailsResponse) {
        if (cartDetailsResponse.getResponse_code() == 100 && cartDetailsResponse.getData().getItems() == null) {
            badgeCount = Integer.valueOf(cartDetailsResponse.getData().getItems_count());
            updateCartItemCount(badgeCount);
        }
    }*/

}

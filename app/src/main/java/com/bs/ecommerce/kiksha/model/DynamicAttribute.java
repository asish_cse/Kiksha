package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by Ashraful on 4/15/2016.
 */
public class DynamicAttribute  {
    public String getAttribute_code() {
        return attribute_code;
    }

    public void setAttribute_code(String attribute_code) {
        this.attribute_code = attribute_code;
    }

    public String getAttribute_id() {
        return attribute_id;
    }

    public void setAttribute_id(String attribute_id) {
        this.attribute_id = attribute_id;
    }

    public String getFrontend_label() {
        return frontend_label;
    }

    public void setFrontend_label(String frontend_label) {
        this.frontend_label = frontend_label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getStore_label() {
        return store_label;
    }

    public void setStore_label(String store_label) {
        this.store_label = store_label;
    }

    public String getUse_default() {
        return use_default;
    }

    public void setUse_default(String use_default) {
        this.use_default = use_default;
    }

    public List<DynamicAttributeValue> getValues() {
        return values;
    }

    public void setValues(List<DynamicAttributeValue> values) {
        this.values = values;
    }

    private String id;
    private String label;
    private String use_default;
    private String position;
    private String attribute_id;
    private String attribute_code;
    private String frontend_label;
    private String store_label;
    private List<DynamicAttributeValue>values;

}

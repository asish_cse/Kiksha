package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 4/22/2016.
 */
public class UserInfoUpdateResponse {
    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    private int response_code;
}

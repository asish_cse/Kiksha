package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by Ashraful on 5/12/2016.
 */
public class FilterAndSortRequest {

    private String category_id;
    private int page;
    private int sort_param;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public List<FilterRequest> getFilter() {
        return filter;
    }

    public void setFilter(List<FilterRequest> filter) {
        this.filter = filter;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSort_param() {
        return sort_param;
    }

    public void setSort_param(int sort_param) {
        this.sort_param = sort_param;
    }

    private List<FilterRequest> filter;
}

package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.OrderReviewItem;

import java.util.List;

/**
 * Created by BS62 on 5/16/2016.
 */
public class OrderReviewAdapter extends BaseAdapter {

    Context context;
    List<OrderReviewItem> orderReviewItemList;
    private static LayoutInflater inflater = null;

    public OrderReviewAdapter(Context context, List<OrderReviewItem> orderReviewItemList) {
        this.context = context;
        this.orderReviewItemList = orderReviewItemList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return orderReviewItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        View rowView = convertView;
        if (convertView == null) {
            rowView = inflater.inflate(R.layout.item_order_review, null);
            holder = new Holder();

            holder.productNameTv = (TextView) rowView.findViewById(R.id.tv_product_name);
            holder.productPriceTv = (TextView) rowView.findViewById(R.id.tv_product_price);
            holder.productQtyTv = (TextView) rowView.findViewById(R.id.tv_product_qty);
            holder.productSubtotalTv = (TextView) rowView.findViewById(R.id.tv_product_subtotal);

            rowView.setTag(holder);
        } else {
            holder = (Holder) rowView.getTag();
        }


        holder.productNameTv.setText(orderReviewItemList.get(position).getName());
        holder.productPriceTv.setText("\u09f3 " + orderReviewItemList.get(position).getPrice());
        holder.productQtyTv.setText(orderReviewItemList.get(position).getQty() + "");
        holder.productSubtotalTv.setText("\u09f3 " + orderReviewItemList.get(position).getSubtotal_price());

        return rowView;
    }

    public static class Holder {
        TextView productNameTv;
        TextView productPriceTv;
        TextView productQtyTv;
        TextView productSubtotalTv;
    }
}

package com.bs.ecommerce.kiksha.network;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.Url;
import com.bs.ecommerce.kiksha.model.BaseResponse;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import de.greenrobot.event.EventBus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * Created by Ashraful on 11/5/2015.
 */
public class CustomCallback<T> implements Callback<T> {
    RelativeLayout layout;
    Context context;

    public ProgressDialog pDialog;

    public  void setProgressDialog() {
        pDialog= new ProgressDialog(context);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(true);
        pDialog.show();

        Log.d("--network srvce hndlr ","--");
    }
    public  void dismissDialog() {
        try {
            //pDialog.setCancelable(true);
            //pDialog.setCanceledOnTouchOutside(true);
            if ((pDialog != null) && pDialog.isShowing()) {
                pDialog.dismiss();
            }
        } catch (final IllegalArgumentException e) {
            e.printStackTrace();

        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            pDialog = null;
        }


    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        dismissDialog();



        if (t instanceof IOException) {

            ConnectivityManager cm =
                    (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();

            if (!isConnected) {
             //   Toast.makeText(context, "Check your internet connection", Toast.LENGTH_SHORT).show();
                toastMsg("Check your internet connection");
            }
        }
   //     EventBus.getDefault().post(t);

    }

    private void toastMsg(String str){
        Toast toast = Toast.makeText(context, Html.fromHtml("<font size='12' color='#FFFFFF'  ><b>" + str + "</b></font>"), Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM|Gravity.FILL_HORIZONTAL, 0, 0);
        // toast.setGravity(Gravity.CENTER, 50, 50);
        toast.getView().setPadding(10, 25, 10, 35);

//        toast.getView().setBackgroundColor(this.getResources().getColor(R.color.listdivider));
 //       toast.show();
    }

    @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        String url = call.request().url().toString();
        Log.d("---->>"+url,"===");
       if (!url.equals("https://kiksha.com/restapi/index.php/search")) {  // "https://kiksha.com/restapi/index.php/search"
 //       if (!url.equals("http://staging.kiksha.com/restapi/")) {

           if(response.body()!=null)
            manipulateBaseResponse(response.body());
        }

            Log.d("--response--", "response ==: "+new Gson().toJson(response.body()) );


        if(response.body()!=null)
        EventBus.getDefault().post(response.body());
        dismissDialog();
    }



    /*
    public CustomCallback(RelativeLayout layout)
    {


    }
    public CustomCallback(View layout)
    {
        this((RelativeLayout) layout);
    }*/

    public CustomCallback( Context context)
    {
        this.context=context;
        setProgressDialog();
    }
    public CustomCallback( )
    {

    }



    /*@Override
    public void success(T t, Response response) {



    }

    @Override
    public void failure(Call<T> call, Response<T> response) {
        dismissDialog();
        EventBus.getDefault().post(error);


    }*/


    public void manipulateBaseResponse(T t)
    {
        try{
            BaseResponse baseResponse=(BaseResponse)t;
            if(baseResponse.getErrors().length>0 && !baseResponse.getErrors()[0].isEmpty())
                if (!baseResponse.getErrors()[0].equalsIgnoreCase("No more product in this category.") &&
                        !baseResponse.getErrors()[0].equalsIgnoreCase("Your search returns no results.")) {
                   // Toast.makeText(context, baseResponse.getErrors()[0], Toast.LENGTH_SHORT).show();
                    toastMsg(baseResponse.getErrors()[0]);

//                    if(baseResponse.getErrors()[0].equalsIgnoreCase("Your search returns no results.")){
//                        Log.d("--response--", "my msg ==: " );
//
//                    }
                } {

                }

        }
        catch (Exception ex)
        {
       ex.printStackTrace();
        }

    }
}

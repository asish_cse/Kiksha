package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/25/2016.
 */
public class RemoveItemRequest {
    private int cartId;
    private int itemId;

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }
}

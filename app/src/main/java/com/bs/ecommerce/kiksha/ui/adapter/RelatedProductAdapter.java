package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bs.ecommerce.kiksha.R;

import java.util.List;

/**
 * Created by Ashraful on 4/20/2016.
 */
public class RelatedProductAdapter extends ProductAdapter {
    public RelatedProductAdapter(Context context, List productsList) {
        super(context, productsList);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

         int   layout = R.layout.item_related_product;

        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(layout, parent, false);

        return new ProductSummaryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder bindViewHolder, int position) {
        super.onBindViewHolder(bindViewHolder, position);
        ((ProductSummaryHolder)bindViewHolder).productShortDescriptionTextView.
                setVisibility(View.GONE);
    }
}

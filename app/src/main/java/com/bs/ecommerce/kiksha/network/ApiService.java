package com.bs.ecommerce.kiksha.network;

import com.bs.ecommerce.kiksha.model.AddressAddRequest;
import com.bs.ecommerce.kiksha.model.AddressAddResponse;
import com.bs.ecommerce.kiksha.model.AddressListResponse;
import com.bs.ecommerce.kiksha.model.AddressListResponseS;
import com.bs.ecommerce.kiksha.model.AddressRemoveResponse;
import com.bs.ecommerce.kiksha.model.AddressUpdateRequest;
import com.bs.ecommerce.kiksha.model.BestsellerList2;
import com.bs.ecommerce.kiksha.model.CartId;
import com.bs.ecommerce.kiksha.model.AllShippingMethodResponse;
import com.bs.ecommerce.kiksha.model.BaseResponse;
import com.bs.ecommerce.kiksha.model.CartAddRequest;
import com.bs.ecommerce.kiksha.model.CartCreateResponse;
import com.bs.ecommerce.kiksha.model.CartDetailsResponse;
import com.bs.ecommerce.kiksha.model.CartItemChangeResponse;
import com.bs.ecommerce.kiksha.model.CartMargeRequest;
import com.bs.ecommerce.kiksha.model.CartMargeResponse;
import com.bs.ecommerce.kiksha.model.CartShippingCharge;
import com.bs.ecommerce.kiksha.model.CategoryOfferzoneResponse;
import com.bs.ecommerce.kiksha.model.CategoryResponse;
import com.bs.ecommerce.kiksha.model.CityResponse;
import com.bs.ecommerce.kiksha.model.CountryResponse;
import com.bs.ecommerce.kiksha.model.CouponCodeAddRequest;
import com.bs.ecommerce.kiksha.model.CustomerSetRequest;
import com.bs.ecommerce.kiksha.model.CustomerSetResponse;
import com.bs.ecommerce.kiksha.model.EmailChangeRequest;
import com.bs.ecommerce.kiksha.model.EmailChangeResponse;
import com.bs.ecommerce.kiksha.model.FilterAndSortRequest;
import com.bs.ecommerce.kiksha.model.FilterItemListResponse;
import com.bs.ecommerce.kiksha.model.HomePageModelResponse;
import com.bs.ecommerce.kiksha.model.HomeSlider;
import com.bs.ecommerce.kiksha.model.Homedatalist2;
import com.bs.ecommerce.kiksha.model.Homesliderlist2;
import com.bs.ecommerce.kiksha.model.LoginRequest;
import com.bs.ecommerce.kiksha.model.MessageResponse;
import com.bs.ecommerce.kiksha.model.OrderDetailsResponse;
import com.bs.ecommerce.kiksha.model.OrderHistoryResponse;
import com.bs.ecommerce.kiksha.model.OrderPlaceResponse;
import com.bs.ecommerce.kiksha.model.OrderReviewResponse;
import com.bs.ecommerce.kiksha.model.PasswordChangeRequest;
import com.bs.ecommerce.kiksha.model.PasswordChangeResponse;
import com.bs.ecommerce.kiksha.model.PasswordResetRequest;
import com.bs.ecommerce.kiksha.model.PaymentMethodResponse;
import com.bs.ecommerce.kiksha.model.PaymentMethodSetRequest;
import com.bs.ecommerce.kiksha.model.ProductDetailResponse;
import com.bs.ecommerce.kiksha.model.ProductListResponse;
import com.bs.ecommerce.kiksha.model.ProductSortParameterResponse;
import com.bs.ecommerce.kiksha.model.PromotionalOfferResponse;
import com.bs.ecommerce.kiksha.model.RegistrationRequest;
import com.bs.ecommerce.kiksha.model.RemoveItemRequest;
import com.bs.ecommerce.kiksha.model.SetAddressRequest;
import com.bs.ecommerce.kiksha.model.ShippingMethodSetRequest;
import com.bs.ecommerce.kiksha.model.SearchRequest;
import com.bs.ecommerce.kiksha.model.SpecialChargeResponse;
import com.bs.ecommerce.kiksha.model.UpdateQuantityRequest;
import com.bs.ecommerce.kiksha.model.UserAuthenticationResponse;
import com.bs.ecommerce.kiksha.model.UserInfoUpdateRequest;
import com.bs.ecommerce.kiksha.model.UserInfoUpdateResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Ashraful on 3/31/2016.
 */
public interface ApiService {

//    @GET("category")
//    Call<CategoryResponse> getCategories();


    @POST("category_menu")
    Call<CategoryResponse> getCategories(@Body String body);

// @POST("category_menu")
// Call<CategoryOfferzoneResponse>getOfferzonemenu(@Body CategoryOfferzoneResponse categoryOfferzoneResponse);


    @GET("category/getAllProductByCategory/{category_id}/{page_number}")
    Call<ProductListResponse> getProductList(@Path("category_id") String id,
                                             @Path("page_number") String page_number);

// @GET("all_product_category/getAllProductByCategory/{category_id}/{page_number}")
// Call<ProductListResponse> getProductListforoffer(@Path("category_id") String id,
//                                                  @Path("page_number") String page_number);

    @GET("all_product_category/getAllProductByCategory/{category_id}/{page_number}")
    Call<ProductListResponse> getProductListforoffer(@Path("category_id") String id);

    @POST("search")
    Call<ProductListResponse> getProductListBySearch(@Body SearchRequest searchRequest);

    @POST("category/getAllProductByCategoryWithSortingAndFilter")
    Call<ProductListResponse> getProductListBySortAndFilter(@Body FilterAndSortRequest filterAndSortRequest);


    @GET("utils/getProductSortParam")
    Call<ProductSortParameterResponse> getSortParameter();

    @GET("utils/getFilterableAttributesByCategory/{category_id}")
    Call<FilterItemListResponse> getfilterItemList(@Path("category_id") String category_id);


    @GET("product/details/{id}")
    Call<ProductDetailResponse> getProductDetail(@Path("id") String id);

    @GET("home/getHomePageData")
    Call<HomePageModelResponse> getHomePageData();

    @POST("register")
    Call<UserAuthenticationResponse> registerUser(@Body RegistrationRequest registrationRequest);

    @POST("login")
    Call<UserAuthenticationResponse> login(@Body LoginRequest loginRequest);


    @GET("customer/details/{entity_id}")
    Call<UserAuthenticationResponse> getCustomerDetail(@Path("entity_id") String id);

    @POST("customer/updateInfo")
    Call<UserInfoUpdateResponse> updateUserInfo(@Body UserInfoUpdateRequest userInfoUpdateRequest);

    @POST("customer/changeEmail")
    Call<EmailChangeResponse> changeEmail(@Body EmailChangeRequest emailChangeRequest);

    @POST("customer/changePassword")
    Call<PasswordChangeResponse> changePassword(@Body PasswordChangeRequest passwordChangeRequest);

    //==================================================
    @POST("cartBillingM/getSpecialCharge")
    Call<SpecialChargeResponse> getSpecialCharge(@Body String body);

    //--------------------
    @POST("customer/getAddresses/{customer_id}")
    Call<AddressListResponse> getAddressList(@Path("customer_id") String id, @Body String body);

    @POST("customer/getAddresses/{customer_id}")
    Call<AddressListResponseS> getAddressList1(@Path("customer_id") String id, @Body String body);

    @GET("utils/getAllCountryForDropDown")
    Call<CountryResponse> getCountryListForDropdown();

    //    @Headers("user-key: 9900a9720d31dfd5fdb4352700c")
    @Headers("auth_token:123")
    @POST("utils/getAllCityForDropDown1")
    Call<CityResponse> getCityForDropDown(@Body String body);

    @POST("customer/addAddress")
    Call<AddressAddResponse> addNewAddress(@Body AddressAddRequest addressAddRequest);

    @POST("customer/updateAddress")
    Call<AddressAddResponse> updateAddress(@Body AddressUpdateRequest addressUpdateRequest);

    @POST("customer/forgotPassword")
    Call<BaseResponse> resetPassword(@Body PasswordResetRequest passwordResetRequest);

    @POST("cart/create")
    Call<CartCreateResponse> createNewCart(@Body String body);

    @POST("cart2/addProduct")
    Call<CartItemChangeResponse> addToCart(@Body CartAddRequest cartAddRequest);

    @GET("cart/info/{cart_id}")
    Call<CartDetailsResponse> getCartInfo(@Path("cart_id") String cart_id);

//    @GET("home_category/getHomePageData")
//    Call<Homedatalist2> getMyJSON();

    @GET("home_data/getHomePageData")
    Call<Homedatalist2> getMyJSON();


    @GET("home_data/getBestSoldProducts")
    Call<BestsellerList2> getBestSoldProducts();

    @GET("home_data/getHomeslider")
    Call<Homesliderlist2> getHomeSlider();


    @POST("cart/details/{cart_id}")
    Call<CartDetailsResponse> getCartDetails(@Path("cart_id") String cart_id, @Body String body);

    @POST("customer/ordersHistory/{customer_id}")
    Call<OrderHistoryResponse> getOrderHistory(@Path("customer_id") String customer_id, @Body String body);

    @POST("customer/orderDetails/{order_id}")
    Call<OrderDetailsResponse> getOrderDetails(@Path("order_id") String order_id, @Body String body);

    @POST("cart/addPromoCode")
    Call<MessageResponse> addCouponCode(@Body CouponCodeAddRequest couponCodeAddRequest);

    @POST("cart/getAllShippingMethod")
    Call<AllShippingMethodResponse> getAllShippingMethod(@Body CartId cartId);

    @POST("cart/setShippingMethod")
    Call<MessageResponse> setShippingMethod(@Body ShippingMethodSetRequest shippingMethodSetRequest);

    @POST("cartBillingP/setPaymentMethod/{cartId}/{code}")
    Call<MessageResponse> setShippingMethodR(@Path("cartId") int cartId,
                                             @Path("code") String code,
                                             @Body ShippingMethodSetRequest shippingMethodSetRequest);

    @POST("cartBillingM/getPaymentMethods")
//    Call<PaymentMethodResponse> getAllPaymentMethod(@Body CartId cartId);
    Call<PaymentMethodResponse> getAllPaymentMethod(@Body CartShippingCharge cartId);

    @POST("cartBillingM/setCodAsPaymentMethod_bkash/{bkas}")
    Call<MessageResponse> setPaymentMethod_bkas(@Path("bkas") String bkas, @Body PaymentMethodSetRequest paymentMethodSetRequest);

    @POST("cartBillingM/setCodAsPaymentMethod")
    Call<MessageResponse> setPaymentMethod(@Body PaymentMethodSetRequest paymentMethodSetRequest);

    //==================Checkout Address===========================
    @POST("cartBillingM/setAddresses")
    Call<MessageResponse> setCheckoutAddress(@Body SetAddressRequest setAddressRequest);

    @POST("cartBillingM/orderReview/{cart_id}")
    Call<OrderReviewResponse> getOrderData(@Path("cart_id") String cart_id, @Body String body);

    @POST("cart/placeOrder")
    Call<OrderPlaceResponse> placeOrder(@Body CartId cartId);

    @POST("cart/setCustomer")
    Call<CustomerSetResponse> setCustomer(@Body CustomerSetRequest customerSetRequest);

    @GET("utils/getAllPromotionalOffer")
    Call<PromotionalOfferResponse> getPromotionalOffer();

    @POST("cart/update")
    Call<CartItemChangeResponse> updateQuantity(@Body UpdateQuantityRequest updateQuantityRequest);

    @POST("cart/removeItem")
    Call<CartItemChangeResponse> removeItemFromCart(@Body RemoveItemRequest removeItemRequest);

    @POST("cart/emptyCart/{cart_id}")
    Call<CartItemChangeResponse> emptyCart(@Path("cart_id") String cart_id, @Body String body);

    @POST("cart/cartMerge")
    Call<CartMargeResponse> margeCart(@Body CartMargeRequest cartMargeRequest);

    @POST("customer/removeAddress/{address_id}")
    Call<AddressRemoveResponse> removeAddress(@Path("address_id") String address_id, @Body String body);

}


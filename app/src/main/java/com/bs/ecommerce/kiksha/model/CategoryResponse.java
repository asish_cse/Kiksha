package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 11/6/2015.
 */
public class CategoryResponse extends BaseResponse{
    public Category getData() {
        return data;
    }

    public void setData(Category data) {
        this.data = data;
    }

    private Category data;

}

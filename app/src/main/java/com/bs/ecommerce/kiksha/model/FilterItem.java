package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by Ashraful on 5/11/2016.
 */
public class FilterItem {
    private String value;
    private String label;
    private String name;
    private List<FilterOptions>options;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FilterOptions> getOptions() {
        return options;
    }

    public void setOptions(List<FilterOptions> options) {
        this.options = options;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

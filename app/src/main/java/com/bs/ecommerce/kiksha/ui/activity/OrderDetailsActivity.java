package com.bs.ecommerce.kiksha.ui.activity;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.OrderInfo;
import com.bs.ecommerce.kiksha.model.OrderDetailsItem;
import com.bs.ecommerce.kiksha.model.OrderDetailsResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.ui.adapter.OrderDetailsAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_order_details)
public class OrderDetailsActivity extends BaseActivity {
    @InjectView(R.id.tv_header)
    TextView headerTv;
    @InjectView(R.id.tv_order_date)
    TextView orderDateTv;
    @InjectView(R.id.tv_shipping_address)
    TextView shippingAddressTv;
    @InjectView(R.id.tv_billing_address)
    TextView billingAddressTv;
    @InjectView(R.id.tv_shipping_method)
    TextView shippingMethodTv;
    @InjectView(R.id.tv_payment_method)
    TextView paymentMethodTv;
    @InjectView(R.id.lv_order_item)
    ListView orderItemLv;
    @InjectView(R.id.tv_order_subtotal)
    TextView orderSubtotalTv;
    @InjectView(R.id.tv_order_discount)
    TextView orderDiscountTv;

    @InjectView(R.id.tv_grand_total)
    TextView grandTotalTv;


    @InjectView(R.id.tv_order_shipping_amount)
    TextView shippingAmountTv;


    @InjectView(R.id.btn_place_order)
    Button placeOrderBtn;

    public static String order_id = "";
    private List<OrderDetailsItem> orderDetailsItems;
    private OrderDetailsAdapter orderDetailsAdapter;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Order Details");
        initListView();
        callOrderDetailsApi();
    }

    private void callOrderDetailsApi() {
        Call<OrderDetailsResponse> callback = RestClient.get().getOrderDetails(order_id, "");
        NetworkServiceHandler.processCallBack(callback, this);
    }

    private void initListView() {
        orderDetailsItems = new ArrayList<>();
        orderDetailsAdapter = new OrderDetailsAdapter(this, orderDetailsItems);
        orderItemLv.setAdapter(orderDetailsAdapter);
    }

    public void onEvent(OrderDetailsResponse orderDetailsResponse) {
        if (orderDetailsResponse.getResponse_code() == 100) {
            setTextField(orderDetailsResponse.getData());
        }
    }

    private void setTextField(OrderInfo orderInfo) {
        headerTv.setText("ORDER - " + orderInfo.getStatus().toUpperCase());
        String date = getFormattedDate(orderInfo.getCreated_at());
        // dateString = new SimpleDateFormat("MMM dd, yyyy").format(dateString);
        orderDateTv.setText(date);
        String shippingAddress = orderInfo.getShipping_address().getFirstname() + " "
                + orderInfo.getShipping_address().getLastname() + " \n"
                + orderInfo.getShipping_address().getStreet()[0] + " \n"
                + orderInfo.getShipping_address().getArea() + ", "
                + orderInfo.getShipping_address().getCity() + ", "
                + orderInfo.getShipping_address().getPostcode() + " \n"
                + orderInfo.getShipping_address().getCountry() + " \n"
                + "T: " + orderInfo.getShipping_address().getTelephone();
        shippingAddressTv.setText(shippingAddress);

        String billingAddress = orderInfo.getBilling_address().getFirstname() + " "
                + orderInfo.getBilling_address().getLastname() + " \n"
                + orderInfo.getBilling_address().getStreet()[0] + " \n"
                + orderInfo.getBilling_address().getArea() + ", "
                + orderInfo.getBilling_address().getCity() + ", "
                + orderInfo.getBilling_address().getPostcode() + " \n"
                + orderInfo.getBilling_address().getCountry() + " \n"
                + "T: " + orderInfo.getBilling_address().getTelephone();
        billingAddressTv.setText(billingAddress);
        shippingMethodTv.setText(orderInfo.getShipping_description());
        paymentMethodTv.setText(orderInfo.getPayment_method());

        orderDetailsItems = orderInfo.getItems();
        // orderDetailsAdapter.notifyDataSetChanged();
        orderDetailsAdapter = new OrderDetailsAdapter(this, orderDetailsItems);
        orderItemLv.setAdapter(orderDetailsAdapter);


        orderSubtotalTv.setText("\u09f3 " + orderInfo.getSubtotal());
        orderDiscountTv.setText("\u09f3 " + orderInfo.getDiscount_amount());
        grandTotalTv.setText("\u09f3 " + orderInfo.getGrand_total());
        shippingAmountTv.setText("\u09f3 " + orderInfo.getShipping_amount());
    }

    private String getFormattedDate(String dateString) {
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);
            return new SimpleDateFormat("MMM dd, yyyy").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

    }


}

package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/17/2016.
 */
public class CustomerSetRequest {
    private int cartId;
    private int customerId;

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}

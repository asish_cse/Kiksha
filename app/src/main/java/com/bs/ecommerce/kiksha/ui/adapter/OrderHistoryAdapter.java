package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.OrderHistory;

import java.util.List;

/**
 * Created by BS62 on 5/12/2016.
 */
public class OrderHistoryAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    List<OrderHistory> orderHistoryList;
    Context context;

    public OrderHistoryAdapter(Context context, List<OrderHistory> orderHistoryList) {
        this.orderHistoryList = orderHistoryList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return orderHistoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;
        View rowView = convertView;
        if (convertView == null) {
            rowView = inflater.inflate(R.layout.item_order_history, null);
            holder = new Holder();

            holder.nameTv = (TextView) rowView.findViewById(R.id.tv_name);
            holder.shipToTv = (TextView) rowView.findViewById(R.id.tv_ship_to);
            holder.orderIdTv = (TextView) rowView.findViewById(R.id.tv_order_id);
            holder.totalItemCountTv = (TextView) rowView.findViewById(R.id.tv_total_item_count);
            holder.grandTotalTv = (TextView) rowView.findViewById(R.id.tv_grand_total);
            holder.statusTv = (TextView) rowView.findViewById(R.id.tv_status);

            rowView.setTag(holder);
        } else {
            holder = (Holder) rowView.getTag();
        }


        holder.nameTv.setText("Name: " + orderHistoryList.get(position).getCustomer_name());
        holder.shipToTv.setText("Ship to: " + orderHistoryList.get(position).getShip_to());
        holder.orderIdTv.setText("Order id: " + orderHistoryList.get(position).getOrder_id());
        holder.totalItemCountTv.setText("Total item: " + orderHistoryList.get(position).getTotal_item_count());
        holder.grandTotalTv.setText("Grand total: " + orderHistoryList.get(position).getGrand_total());
        holder.statusTv.setText("Status: " + orderHistoryList.get(position).getStatus());

        return rowView;
    }

    public static class Holder {
        TextView nameTv;
        TextView shipToTv;
        TextView orderIdTv;
        TextView totalItemCountTv;
        TextView grandTotalTv;
        TextView statusTv;
    }
}

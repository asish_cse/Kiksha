package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/13/2016.
 */
public class CartId {
    private int cartId;

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }
}

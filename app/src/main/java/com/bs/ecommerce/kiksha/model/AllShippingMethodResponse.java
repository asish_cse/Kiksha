package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by BS62 on 5/13/2016.
 */
public class AllShippingMethodResponse extends BaseResponse {
    private List<ShippingMethod> data;

    public List<ShippingMethod> getData() {
        return data;
    }

    public void setData(List<ShippingMethod> data) {
        this.data = data;
    }
}

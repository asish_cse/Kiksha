package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/25/2016.
 */
public class CartItemChangeResponse extends BaseResponse {
    private CartItemOperation data;

    public CartItemOperation getData() {
        return data;
    }

    public void setData(CartItemOperation data) {
        this.data = data;
    }
}

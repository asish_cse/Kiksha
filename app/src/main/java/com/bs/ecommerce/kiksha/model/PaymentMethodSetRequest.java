package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/16/2016.
 */
public class PaymentMethodSetRequest {
    private int cartId;
    private String method_code;
    private String shipping_charge;

    public String getShipping_charge() {
        return shipping_charge;
    }

    public void setShipping_charge(String shipping_charge) {
        this.shipping_charge = shipping_charge;
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public String getMethod_code() {
        return method_code;
    }

    public void setMethod_code(String method_code) {
        this.method_code = method_code;
    }
}

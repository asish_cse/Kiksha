package com.bs.ecommerce.kiksha.ui.activity;

import android.util.Log;

import com.bs.ecommerce.kiksha.model.Address;
import com.bs.ecommerce.kiksha.model.AddressAddResponse;
import com.bs.ecommerce.kiksha.model.AddressUpdateRequest;
import com.bs.ecommerce.kiksha.model.City;
import com.bs.ecommerce.kiksha.model.CityResponse;
import com.bs.ecommerce.kiksha.model.CountryResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.view.FormViews;

import java.util.List;

import retrofit2.Call;

/**
 * Created by BS62 on 5/6/2016.
 */
public class AddressUpdateActivity extends AddNewAdressActivity {
    public static Address address;
    //     List<City> cityList;
    public static String area = "";
    public static int area_ch = 0;


    /*
when c computer can be seen wverywhere

     */

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setTextInView();
        setToolbarTitle("Update Address");
    }

    private void setTextInView() {
        firstNameEditText.setText(address.getFirstname());
        lastNameEditText.setText(address.getLastname());
        companyEditText.setText(address.getCompany());
        telephoneEditText.setText(address.getTelephone());
        faxEditText.setText(address.getFax());
        streetAddress1EditText.setText(address.getStreet()[0]);

        /*if (address.getStreet().length > 1) {
            streetAddress2EditText.setText(address.getStreet()[1]);
        }*/
//...............update........................

        postalCodeEditText.setText(address.getPostcode());
        addAsDefaultBillingCheckBox.setChecked(address.isDefaultBillingAddress());
        addAsDefaultShippingCheckBox.setChecked(address.isDefaultShippingAddress());
    }

    @Override
    protected void sendNewAddressRequest() {
        Call<AddressAddResponse> callback = RestClient.get().updateAddress(getAddressUpdateObject());
        NetworkServiceHandler.processCallBack(callback, this);
    }

    private AddressUpdateRequest getAddressUpdateObject() {
        AddressUpdateRequest addressUpdateRequest = new AddressUpdateRequest();
        addressUpdateRequest.setAddress_id(address.getAddress_id());
        addressUpdateRequest.setCustomer_id(preferenceService.GetPreferenceValue(PreferenceService.ENTITY_KEY));

        addressUpdateRequest.setFirstname(FormViews.getTexBoxFieldValue(firstNameEditText));
        addressUpdateRequest.setLastname(FormViews.getTexBoxFieldValue(lastNameEditText));
        addressUpdateRequest.setCompany(FormViews.getTexBoxFieldValue(companyEditText));
        addressUpdateRequest.setTelephone(FormViews.getTexBoxFieldValue(telephoneEditText));
        addressUpdateRequest.setFax(FormViews.getTexBoxFieldValue(faxEditText));

        String[] streetAddress = new String[1];
        streetAddress[0] = FormViews.getTexBoxFieldValue(streetAddress1EditText);
        //streetAddress[1] = FormViews.getTexBoxFieldValue(streetAddress2EditText);
        addressUpdateRequest.setStreet(streetAddress);
//..............update................
        addressUpdateRequest.setCity(cityList.get(cityEditText.getSelectedItemPosition()).getCity());
//        addressUpdateRequest.setRegion(FormViews.getTexBoxFieldValue(stateEditText));
        addressUpdateRequest.setArea(areaList.get(stateEditText.getSelectedItemPosition()));
        addressUpdateRequest.setPostcode(FormViews.getTexBoxFieldValue(postalCodeEditText));

        /*String countryLabel = countrySpinner.getSelectedItem().toString();
        addressUpdateRequest.setCountry_id(countryMap.get(countryLabel));*/
        addressUpdateRequest.setCountry_id(countryList.get(countrySpinner.getSelectedItemPosition()).getValue());


        addressUpdateRequest.setDefaultBillingAddress(addAsDefaultBillingCheckBox.isChecked());
        addressUpdateRequest.setDefaultShippingAddress(addAsDefaultShippingCheckBox.isChecked());
        return addressUpdateRequest;
    }

    @Override
    public void onEvent(CountryResponse countryResponse) {
        super.onEvent(countryResponse);
        countrySpinner.setSelection(countryAdapter.getPosition(address.getCountry()));
    }

    @Override
    public void onEvent(CityResponse countryResponse) {
        super.onEvent(countryResponse);
        int index = cityAdapter.getPosition(address.getCity());
        cityEditText.setSelection(index);
        area = address.getArea();
        area_ch = 1;


    }
}

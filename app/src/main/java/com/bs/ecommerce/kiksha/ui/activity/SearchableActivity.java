package com.bs.ecommerce.kiksha.ui.activity;

import android.app.SearchManager;
import android.content.Context;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;

import com.bs.ecommerce.kiksha.constant.SharedPreferencesHelper;
import com.bs.ecommerce.kiksha.model.ProductListResponse;
import com.bs.ecommerce.kiksha.model.SearchRequest;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Ashraful on 5/10/2016.
 */
public class SearchableActivity extends ProductListActivity {

    private int searchPageNumber=1;
    SearchRequest searchRequest;

    private Context con;

    @Override
    protected void onViewCreated() {
        filterImgBtn.setVisibility(View.GONE);
        sortByImgBtn.setVisibility(View.GONE);

        con = this;
         super.onViewCreated();


        setToolbarTitle("Search");
        searchRequest=new SearchRequest();
        search();

      //  con = this;
    }

    private void search() {
        String query = getIntent().getStringExtra("query");
        setToolbarTitle(query);
        Log.d("SEARCH",""+ SharedPreferencesHelper.getsearchId(con));
//        if("1".equalsIgnoreCase(SharedPreferencesHelper.getsearchId(con))){
//            search_not_found.setText("SEARCH RESULTS FOR "+query+"\n Your search returns no results.");
//        }
       // search_not_found.setText("There are no products matching the selection.");
        /*SearchRecentSuggestions suggestions = new SearchRecentSuggestions (
                getApplicationContext(),
                SearchSuggestionsProvider.AUTHORITY,
                SearchSuggestionsProvider.MODE
        );
        suggestions.saveRecentQuery(query, null);*/

  //      Toast.makeText(this,""+query,Toast.LENGTH_LONG).show();

        productList=new ArrayList<>();
        searchRequest.setQuery(query);
        callSearchApi();

//        if(searchPageNumber==1){
//            search_not_found.setVisibility(View.VISIBLE);
//            search_not_found.setText("visible search not available.");
//        }
    }



    @Override
    public void callFilterItemListApi() {}

    @Override
    public void callSortListApi() {}

    private void callSearchApi()
    {
        searchPageNumber=1;
        searchRequest.setPage(searchPageNumber);
        NetworkServiceHandler.processCallBack(RestClient.get().getProductListBySearch(searchRequest),this);


    //    Log.d("====>>>","search no"+(RestClient.get().getProductListBySearch(searchRequest)));

    }

    private void callSearchApiForMoreProduct()
    {
        searchRequest.setPage(searchPageNumber);
        NetworkServiceHandler.processCallBackWithoutProgressDialog(
                RestClient.get().getProductListBySearch(searchRequest),this);

    }

    public void callMoreProductListApi()
    {
        ++searchPageNumber;
        callSearchApiForMoreProduct();
    }

    public void callFilterEnableMoreProductApi()
    {
        callMoreProductListApi();
    }

    public void onEvent(ProductListResponse productListResponse)
    {
        super.onEvent(productListResponse);

//        String str = productListResponse.toString();
//
//        Boolean found = Arrays.asList(str.split(" ")).contains("results.");
//        if(found){
//            Log.d("--response--", "response2 ==: "+productListResponse.toString() );
//        }
//        Log.d("--response--", "response2 ==: "+productListResponse.toString() );

        if(!isFilterEnabled)
            isFilterEnabled=true;
    }



    @Override
    protected  void callProductListApi() {}

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search,menu);
       initSearch(menu);
        return true;
    }*/


   /* private void initSearch(Menu menu)
    {
        MenuItem searchItem = menu.findItem(R.id.menu_item_search_);
        SearchView searchView = (SearchView) searchItem.getActionView();


        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if(null!=searchManager ) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(false);
        doActionOnQueryTextChanged(searchView);
    }*/

    private void doActionOnQueryTextChanged(final SearchView sv)
    {
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                System.out.println("search query submit");
                  productList=new ArrayList<>();
                 searchRequest.setQuery(query);
                 callSearchApi();
                 sv.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                System.out.println("tap");
                return false;
            }
        });

        sv.setIconified(false);
    }

    /*@Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        try {
            MenuItem searchViewMenuItem = menu.findItem(R.id.menu_item_search);
            SearchView mSearchView = (SearchView) MenuItemCompat.getActionView(searchViewMenuItem);
            int searchImgId = getResources().getIdentifier("android:id/search_button", null, null);
            ImageView v = (ImageView) mSearchView.findViewById(searchImgId);
            v.setImageResource(R.drawable.ic_menu_search);
        }
        catch (Exception ex)
        {

        }

        return super.onPrepareOptionsMenu(menu);
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.menu_item_search_)
        {

        }
        return super.onOptionsItemSelected(item);
    }*/
}

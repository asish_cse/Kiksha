package com.bs.ecommerce.kiksha.ui.activity;

import android.content.IntentFilter;
import android.graphics.Paint;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.LoginRequest;
import com.bs.ecommerce.kiksha.model.UserAuthenticationResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.view.FormViews;
import com.bs.ecommerce.kiksha.util.BroadCastUtils;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/21/2016.
 */
@ContentView(R.layout.activity_user_login)
public class UserLoginActivity extends UserAuthenticationActivity {

    @InjectView(R.id.et_password)
    EditText passwordEdittextText;

    @InjectView(R.id.et_email)
    EditText emailEditText;

    @InjectView(R.id.sign_btn)
    Button continueBtn;

    @InjectView(R.id.tv_forgotten_password)
    TextView forgottenPaswwordTextView;

    @InjectView(R.id.page_container)
    View pageContainer;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Log In");
        forgottenPaswwordTextView.setPaintFlags(forgottenPaswwordTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLoggedIn()) {
            this.finish();
        }
    }

    @Override
    protected void registerBroadcastReceiver() {
        registerReceiver(broadcast_reciever, new IntentFilter(BroadCastUtils.RECREATE_VIEW));
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        continueBtn.setOnClickListener(this);
        forgottenPaswwordTextView.setOnClickListener(this);

    }

    private void validateForm() {
        boolean isValid = true;

        if (!FormViews.isValidEmail(emailEditText, "Email")) {
            isValid = false;
        }
        if (!FormViews.isValidWithMark(passwordEdittextText, "Password")) {
            isValid = false;
        }

        if (isValid) {
            sendLoginRequest();
        }
    }

    private void sendLoginRequest() {
        Call<UserAuthenticationResponse> callback = RestClient.get().login(getLoginRequestObject());
        NetworkServiceHandler.processCallBack(callback, this);
    }

    private LoginRequest getLoginRequestObject() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail(FormViews.getTexBoxFieldValue(emailEditText));
        loginRequest.setPassword(FormViews.getTexBoxFieldValue(passwordEdittextText));
        return loginRequest;
    }

    public void onEvent(UserAuthenticationResponse baseResponse) {
        if (baseResponse.getResponse_code() == 100) {
            showInfoSnackBar("Login Successful.");
            preferenceService.SetPreferenceValue(PreferenceService.ENTITY_KEY,
                    baseResponse.getData().getEntity_id());
            preferenceService.SetPreferenceValue(PreferenceService.EMAIL_KEY,
                    baseResponse.getData().getEmail());
            preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, true);
            preferenceService.SetPreferenceValue(PreferenceService.FIRST_NAME_KEY, baseResponse.getData().getFirstname());
            preferenceService.SetPreferenceValue(PreferenceService.LAST_NAME_KEY, baseResponse.getData().getLastname());
            //sendBroadcast(BroadCastUtils.RECREATE_VIEW);
            //this.finish();
            /*HomePageActivity.needToMerge = true;
            gotoHomeActivity(HomePageActivity.class);*/

            callCartMargeApi();
            pageContainer.setVisibility(View.GONE);
        }

    }

    @Override
    protected void refresh() {
        super.refresh();
        sendBroadcast(BroadCastUtils.RECREATE_VIEW);
    }

    @Override
    protected void unregisterBroadcast() {
        try {
            if (broadcast_reciever != null) {
                unregisterReceiver(broadcast_reciever);
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resourceId = v.getId();

        if (resourceId == R.id.tv_forgotten_password) {
            gotoNewActivity(PasswordResetActivity.class);
        } else if (resourceId == R.id.sign_btn) {
            validateForm();
        }
    }
}

package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 4/27/2016.
 */
public class UserData {
    public UserAuthentication getCustomer() {
        return customer;
    }

    public void setCustomer(UserAuthentication customer) {
        this.customer = customer;
    }

    private UserAuthentication customer;
}

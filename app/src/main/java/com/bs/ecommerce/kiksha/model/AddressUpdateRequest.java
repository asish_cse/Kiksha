package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/6/2016.
 */
public class AddressUpdateRequest extends AddressAddRequest {

    private String address_id;

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }
}

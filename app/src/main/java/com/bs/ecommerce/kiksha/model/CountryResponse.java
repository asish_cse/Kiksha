package com.bs.ecommerce.kiksha.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BS62 on 5/4/2016.
 */
public class CountryResponse extends BaseResponse {
    private List<Country> data = new ArrayList<>();

    public List<Country> getData() {
        return data;
    }

    public void setData(List<Country> data) {
        this.data = data;
    }

    public List<String> getStringList() {
        List<String> list = new ArrayList<>();
        for (Country country : data) {
            list.add(country.getLabel());
        }
        return list;
    }
}

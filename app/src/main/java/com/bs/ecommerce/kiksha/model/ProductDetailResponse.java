package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 4/15/2016.
 */
public class ProductDetailResponse extends BaseResponse {
    public ProductDetail getData() {
        return data;
    }

    public void setData(ProductDetail data) {
        this.data = data;
    }

    private ProductDetail data;
}

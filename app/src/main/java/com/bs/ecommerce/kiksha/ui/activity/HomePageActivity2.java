package com.bs.ecommerce.kiksha.ui.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.AutoScrollViewPager;
import com.bs.ecommerce.kiksha.constant.PageTransformer;
import com.bs.ecommerce.kiksha.constant.SharedPreferencesHelper;
import com.bs.ecommerce.kiksha.event.CloseDrawer;
import com.bs.ecommerce.kiksha.model.BestSeller;
import com.bs.ecommerce.kiksha.model.BestsellerList2;
import com.bs.ecommerce.kiksha.model.CartCreateResponse;
import com.bs.ecommerce.kiksha.model.CartDetailsResponse;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.model.CategoryResponse;
import com.bs.ecommerce.kiksha.model.FeaturedCategory;
import com.bs.ecommerce.kiksha.model.Helper;
import com.bs.ecommerce.kiksha.model.HomePageModel;
import com.bs.ecommerce.kiksha.model.HomePageModelResponse;
import com.bs.ecommerce.kiksha.model.HomeSlider;
import com.bs.ecommerce.kiksha.model.Homedata2;
import com.bs.ecommerce.kiksha.model.Homedatalist2;
import com.bs.ecommerce.kiksha.model.Homesliderlist2;
import com.bs.ecommerce.kiksha.model.Product;
import com.bs.ecommerce.kiksha.model.ProductList;
import com.bs.ecommerce.kiksha.model.SliderImage;
import com.bs.ecommerce.kiksha.network.ApiService;
import com.bs.ecommerce.kiksha.network.BestSliderURL;
import com.bs.ecommerce.kiksha.network.HomeSliderURL;
import com.bs.ecommerce.kiksha.network.InternetConnection;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.network.RetroClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.adapter.FeaturedCategoryAdapter;
import com.bs.ecommerce.kiksha.ui.adapter.HomesliderAdapter;
import com.bs.ecommerce.kiksha.ui.adapter.MyContactAdapter;
import com.bs.ecommerce.kiksha.ui.adapter.ViewPagerAdapter;
import com.bs.ecommerce.kiksha.ui.adapter.ViewPagerAdapterBestSeller;
import com.bs.ecommerce.kiksha.ui.custom_view.CustomSliderView;
import com.bs.ecommerce.kiksha.ui.fragment.CategoryExpandableFragment;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import static com.bs.ecommerce.kiksha.ui.view.FormViews.view;

/**
 * Created by Ashraful on 4/4/2016.
 */
@ContentView(R.layout.activity_home_page3)
public class HomePageActivity2 extends BaseActivity {

//    @InjectView(R.id.swipe_layout)
//    SwipeRefreshLayout swipeRefreshLayout;

    String[] rank;

    PagerAdapter pageradapter;
//    @InjectView(R.id.pager2)
//    ViewPager viewPager;

    ViewPagerAdapterBestSeller pageradapter_bestseller;
    @InjectView(R.id.pagerbest)
    ViewPager viewPager_best;





    @InjectView(R.id.tabDots)
    public TabLayout tabLayout;

    @InjectView(R.id.drawer_layout)
    public DrawerLayout drawerLayout;

    @InjectView(R.id.cv_category)
    CardView categoryCardView;

    @InjectView(R.id.fragment_navigation_drawer)
    FrameLayout navigationDrawer;

//    @InjectView(R.id.featured_category_rclv_top)
//    RecyclerView featuredCategoryRecyclerViewTop;
//
//    @InjectView(R.id.featured_category_rclv_bottom)
//    RecyclerView featuredCategoryRecyclerViewBottom;

//    @InjectView(R.id.custom_indicator)
//    PagerIndicator pagerIndicator;
//
//    @InjectView(R.id.slider)
//    SliderLayout sliderLayout;

//    @InjectView(R.id.sv_home_page)
//    ScrollView scrollView;

    @InjectView(R.id.listView)
    private ListView listView;

    @InjectView(R.id.home_cat_data)
    LinearLayout home_cat_data;

    AutoScrollViewPager viewPagerauto;
    ActionBarDrawerToggle mDrawerToggle;
    GridLayoutManager topGridLayoutManager;
    GridLayoutManager bottomGridLayoutManager;

    Category category;
    int screenWidth;
    private int viewWidth;
    public static final String MERGE_EXTRA = "needToMerge";
    public static boolean needToMerge = false;
    private Context con;

    private ArrayList<Homedata2> contactList2;
    private MyContactAdapter adapter;
    public final static ArrayList<String> sliderlist = new ArrayList<String>();

    private ArrayList<HomeSlider> homesliderlist2;
    private HomesliderAdapter slideradapter2;
    private ArrayList<BestSeller> bestSellers2;



    @Override
    protected void onViewCreated() {
        super.onViewCreated();

        try {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setLogo(R.mipmap.ic_launcher);
        } catch (Exception ex) {
            //
        }

//        swipeRefreshLayout.setOnRefreshListener(this);
//        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged() {
//                int scrollY = scrollView.getScrollY();
//                if (scrollY == 0) {
//                    swipeRefreshLayout.setEnabled(true);
//                } else {
//                    swipeRefreshLayout.setEnabled(false);
//                }
//            }
//        });
//        topGridLayoutManager = new GridLayoutManager(this, 2);
//        bottomGridLayoutManager = new GridLayoutManager(this, 2);

        contactList2 = new ArrayList<>();

        homesliderlist2 =  new ArrayList<>();

        bestSellers2 = new ArrayList<>();

        con = this;
        SharedPreferencesHelper.setsearchId(con,"66");
        drawerSetup();
        getHeightWidth();
        callHomePageApi();
        callCategoryListApi();
     //   callBannerApi();


        //callCartDetailsApi();
        checkNotificationBundle();
  //      swipeRefreshLayout.setRefreshing(false);


        /*
        view pager
         */
        rank = new String[] { "1", "2", "3","4","5", "6","7","8" };

        // viewPager = (ViewPager) findViewById(R.id.pager);
//        viewPager.setClipToPadding(false);
//        viewPager.setPadding(1, 0, 1, 0);

        viewPagerauto= (AutoScrollViewPager) findViewById(R.id.pager2);
        viewPagerauto.startAutoScroll();
        viewPagerauto.setInterval(3000);
        viewPagerauto.setCycle(true);
        viewPagerauto.setStopScrollWhenTouch(true);

//        pageradapter = new ViewPagerAdapter(HomePageActivity2.this, flag,rank);
//
//        viewPager.setAdapter(pageradapter);
        // view pager done ***************

//        LinearLayout ll = (LinearLayout) findViewById(R.id.topll);
//        ScrollView sv= new ScrollView(this);
//
////https://stackoverflow.com/questions/19301458/the-specified-child-already-has-a-parent-you-must-call-removeview-on-the-chil
//
//        ll.setOrientation(LinearLayout.VERTICAL);
//
//        sv.addView(ll);


    }


    public void left(View v){
//        viewPager.arrowScroll(View.FOCUS_LEFT);
//
//        int tab = viewPager.getCurrentItem();
//        if (tab > 0) {
//            tab--;
//            viewPager.setCurrentItem(tab);
//        } else if (tab == 0) {
//            viewPager.setCurrentItem(tab);
//        }

        viewPagerauto.arrowScroll(View.FOCUS_LEFT);

        int tab = viewPagerauto.getCurrentItem();
        if (tab > 0) {
            tab--;
            viewPagerauto.setCurrentItem(tab);
        } else if (tab == 0) {
            viewPagerauto.setCurrentItem(tab);
        }
    }
    public void right(View v){

//        viewPager.arrowScroll(View.FOCUS_RIGHT);
//        int tab = viewPager.getCurrentItem();
//        tab++;
//        viewPager.setCurrentItem(tab);
        viewPagerauto.arrowScroll(View.FOCUS_RIGHT);
        int tab = viewPagerauto.getCurrentItem();
        tab++;
        viewPagerauto.setCurrentItem(tab);
    }

    public void left2(View v){
        viewPager_best.arrowScroll(View.FOCUS_LEFT);

        int tab = viewPager_best.getCurrentItem();
        if (tab > 0) {
            tab--;
            viewPager_best.setCurrentItem(tab);
        } else if (tab == 0) {
            viewPager_best.setCurrentItem(tab);
        }
    }
    public void right2(View v){

        viewPager_best.arrowScroll(View.FOCUS_RIGHT);
        int tab = viewPager_best.getCurrentItem();
        tab++;
        viewPager_best.setCurrentItem(tab);
    }

    private void checkNotificationBundle() {
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                String value = getIntent().getExtras().getString(key);
                Log.d("HomePageActivity", key + " : " + value);

            }
        }
    }


//    private void callBannerApi() {
//        NetworkServiceHandler.processCallBack(RestClient.get().getHomeSlider(), this);
//
//    }

    private void callCategoryListApi() {
        NetworkServiceHandler.processCallBack(RestClient.get().getCategories(""), this);

//       // NetworkServiceHandler.processCallBack(RestClient.get().getOfferzonemenu(), this);
//        Call<CategoryOfferzoneResponse> callback = RestClient.get().getOfferzonemenu();
//        NetworkServiceHandler.processCallBack(callback, this);
    }

    public void onEvent(CategoryResponse categoryResponse) {
        category = categoryResponse.getData();
        replacfragment();
    }


    private void replacfragment() {
        CategoryExpandableFragment drawerFragment = new CategoryExpandableFragment();
        drawerFragment.categoryList = category;
        getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment_navigation_drawer, drawerFragment).commit();

    }


    private void callHomePageApi() {
//        NetworkServiceHandler.processCallBack(
//                RestClient.get().getHomePageData(), this);

        /**
         * Checking Internet Connection
         */
        if (InternetConnection.checkConnection(getApplicationContext())) {
            final ProgressDialog dialog;
            /**
             * Progress Dialog for User Interaction
             */
            dialog = new ProgressDialog(HomePageActivity2.this);
            dialog.setTitle(getString(R.string.string_getting_json_title));
            dialog.setMessage(getString(R.string.string_getting_json_message));
            dialog.show();

            //Creating an object of our api interface
            ApiService api = RetroClient.getApiService();

            Call<Homedatalist2> call = api.getMyJSON();



            call.enqueue(new Callback<Homedatalist2>() {
                @Override
                public void onResponse(Call<Homedatalist2> call, Response<Homedatalist2> response) {
                    //Dismiss Dialog
                    dialog.dismiss();

                    if(response.isSuccessful()) {

                        contactList2 = response.body().getContacts();

                        if(contactList2!=null)
                            adapter = new MyContactAdapter( HomePageActivity2.this, contactList2);
                        Log.d("<<contactList2<<", ">>>>"+contactList2.size());
                        if(adapter==null){
                            showInfoSnackBar("Server is not responding.Please wait..");
                        }else
                        listView.setAdapter(adapter);
                        Helper.getListViewSize(listView);


                        // mRecyclerView.setAdapter(adapter);


                    } else {
                       // Snackbar.make(parentView, R.string.string_some_thing_wrong, Snackbar.LENGTH_LONG).show();
                        showInfoSnackBar("Something wrong");
                    }
                }

                @Override
                public void onFailure(Call<Homedatalist2> call, Throwable t) {
                    dialog.dismiss();
                }
            });

            ////////////////////// home slider parsing here
            ApiService api2 = HomeSliderURL.getApiService();

            Call<Homesliderlist2> call2 = api2.getHomeSlider();

            call2.enqueue(new Callback<Homesliderlist2>() {
                @Override
                public void onResponse(Call<Homesliderlist2> call, Response<Homesliderlist2> response) {
                    //Dismiss Dialog
                    dialog.dismiss();

                    if(response.isSuccessful()) {

                        homesliderlist2 = response.body().getimageList();

                        if(homesliderlist2==null) {
                            Log.d("<<<<", ">>>>");
                            //       Log.d("===null contactlist2="+homesliderlist2.get(0).getImage_file(),"----");
                        }else {
                            slideradapter2 = new HomesliderAdapter(HomePageActivity2.this, homesliderlist2);

                            for(int i=0;i<homesliderlist2.size();i++){
                                sliderlist.add(homesliderlist2.get(i).getImage_file());
                              //  Log.d("===null contactlist2="+homesliderlist2.get(i).getImage_file(),"----");
                            }
                         //   Log.d("<<<<", ">>>>"+homesliderlist2.size());
                      //      pageradapter = new ViewPagerAdapter(HomePageActivity2.this, sliderlist,rank,  homesliderlist2);
                            pageradapter = new ViewPagerAdapter(HomePageActivity2.this, homesliderlist2);

                          //  viewPager.setAdapter(pageradapter);
                            viewPagerauto.setAdapter(pageradapter);
                            viewPagerauto.startAutoScroll();
                            viewPagerauto.setInterval(5000);

                         //   viewPagerauto.setPageTransformer(false, new PageTransformer());
                         //   final float normalizedposition = Math.abs(Math.abs(0) - 1);
                          //   viewPagerauto.setAlpha(normalizedposition);

//                            viewPagerauto.setPageTransformer(true, new ViewPager.PageTransformer() {
//                                @Override
//                                public void transformPage(View view, float position) {
//                                    // Ensures the views overlap each other.
//                                    view.setTranslationX(view.getWidth() * -position);
//
//                                    final float normalizedposition = Math.abs(Math.abs(position) - 1);
//                                    viewPagerauto.setAlpha(normalizedposition);
//
//                                    // Alpha property is based on the view position.
////                                    if(position <= -1.0F || position >= 1.0F) {
////                                        view.setAlpha(0.0F);
////                                    } else if( position == 0.0F ) {
////                                        view.setAlpha(1.0F);
////                                    } else { // position is between -1.0F & 0.0F OR 0.0F & 1.0F
////                                        view.setAlpha(1.0F - Math.abs(position));
////                                    }
//
//                                    // TextView transformation
//                                   // view.findViewById(R.id.flag).setTranslationX(view.getWidth() * position);
//                                }
//                            });

                            viewPagerauto.setAutoScrollDurationFactor(5);

                            tabLayout.setupWithViewPager(viewPagerauto, true);

                           // viewPager.invalidate();


                        }



                    } else {
                        // Snackbar.make(parentView, R.string.string_some_thing_wrong, Snackbar.LENGTH_LONG).show();
                        showInfoSnackBar("Something wrong");
                    }
                }

                @Override
                public void onFailure(Call<Homesliderlist2> call, Throwable t) {
                    dialog.dismiss();
                }
            });

            //////////////////////////
            ////////////////////// best slider parsing
            ApiService api3 = BestSliderURL.getApiService();

            Call<BestsellerList2> call3 = api3.getBestSoldProducts();

            call3.enqueue(new Callback<BestsellerList2>() {
                @Override
                public void onResponse(Call<BestsellerList2> call, Response<BestsellerList2> response) {
                    //Dismiss Dialog
                    dialog.dismiss();

                    if(response.isSuccessful()) {

                        bestSellers2 = null;
                        bestSellers2 = response.body().getimageList();
                        Log.d("<<<<", ">>>>");
                        if(bestSellers2==null) {
                            Log.d("<<<<", ">>>>");
                            //       Log.d("===null contactlist2="+homesliderlist2.get(0).getImage_file(),"----");
                        }else {
                           // slideradapter2 = new HomesliderAdapter(HomePageActivity2.this, homesliderlist2);


                            for(int i=0;i<bestSellers2.size();i++){
                               // sliderlist.add(bestSellers2.get(i).getImage_file());
                                Log.d("=== best seller="+bestSellers2.get(i).getId(),"----");
                               // Log.d("<<<<", ">>>>"+bestSellers2.get(0).getImage());

                            }

                            pageradapter_bestseller = new ViewPagerAdapterBestSeller(HomePageActivity2.this,  bestSellers2);

                            viewPager_best.setAdapter(pageradapter_bestseller);
                            viewPager_best.setCurrentItem(2);
                            viewPager_best.invalidate();

                        }


                    } else {
                        // Snackbar.make(parentView, R.string.string_some_thing_wrong, Snackbar.LENGTH_LONG).show();
                        showInfoSnackBar("Something wrong");
                    }
                }

                @Override
                public void onFailure(Call<BestsellerList2> call, Throwable t) {
                    dialog.dismiss();
                }
            });


        } else {
            //Snackbar.make(parentView, R.string.string_internet_connection_not_available, Snackbar.LENGTH_LONG).show();

            showInfoSnackBar("Internet connection is not available");
        }

    }

    //=========================Home Data Load============================
    private void homeDataView(Homedata2 data) {
        LayoutInflater mInflater = LayoutInflater.from(HomePageActivity2.this);
        View view = mInflater.inflate(R.layout.layout_row_view, home_cat_data, false);
        home_cat_data.addView(view);
    }

    //=====================================================
    public void onEvent(HomePageModelResponse homePageModelResponse) {
        HomePageModel homePageModel = homePageModelResponse.getData();
        if (homePageModel != null) {
            Log.d("===homepage onevent=","----");
//            setTopFeatureCategory(homePageModel.getFeatured_category().subList(0,4));
//            setSlider(homePageModel.getSilder_image());
//            setBottomFeatureCategory(homePageModel.getFeatured_category().subList(4, 8));
        }

    }

//    private void setTopFeatureCategory(List<FeaturedCategory> topFeatureCategory) {
//        featuredCategoryRecyclerViewTop.setLayoutManager(topGridLayoutManager);
//        final FeaturedCategoryAdapter topAdapter = new FeaturedCategoryAdapter(this, topFeatureCategory, sliderLayout);
//        featuredCategoryRecyclerViewTop.setAdapter(topAdapter);
//
//        topAdapter.SetOnItemClickListener(new FeaturedCategoryAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                // FeaturedCategory selectedFeature = topAdapter.getItem(position);
//                Log.d("===Homepage=====","----"+position);
//                gotoNextActivity(topAdapter.getItem(position));
//               // SharedPreferencesHelper.setMenu(con,"99");
//                if(position == 2)
//                    SharedPreferencesHelper.setMenu(con,"8");
//                else
//                    SharedPreferencesHelper.setMenu(con,"99");
//            }
//        });
//    }

//    private void setSlider(List<SliderImage> sliderImageList) {
//        sliderLayout.removeAllSliders();
//        sliderLayout.setCustomIndicator(pagerIndicator);
//        for (final SliderImage sliderImage : sliderImageList) {
//            CustomSliderView defaultSliderView = new CustomSliderView(this);
//            defaultSliderView.empty(R.drawable.placeholder);
//            defaultSliderView.image(sliderImage.getImg())
//                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
//
//            SharedPreferencesHelper.setMenu(con,"99");
//
//
//
//            defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
//                @Override public void onSliderClick(BaseSliderView slider) {
//                    SharedPreferencesHelper.setMenu(con,"99");
//                    if (sliderImage.getType().equals("1")) {
//                        Log.d("expandable frag pos--1",">>>");
//                        Category category = new Category();
//                        category.setCategory_id(sliderImage.getId());
//                        category.setName(sliderImage.getTitle());
//                        ProductListActivity.category = category;
//                        gotoNewActivity(ProductListActivity.class);
//                    }
//
//                    if (sliderImage.getType().equals("2")) {
//                        Log.d("expandable frag pos--2",">>>");
//                        Product product = new Product();
//                        product.setProductId(sliderImage.getId());
//                        product.setName(sliderImage.getTitle());
//                        ProductDetailsActivity.product = product;
//                        gotoNewActivity(ProductDetailsActivity.class);
//                    }
//                }
//            });
//
//            sliderLayout.addSlider(defaultSliderView);
//
//
//        }
//    }

//    private void setBottomFeatureCategory(List<FeaturedCategory> bottomFeatureCategory) {
//        Log.d("size", bottomFeatureCategory.size()+"");
//        featuredCategoryRecyclerViewBottom.setLayoutManager(bottomGridLayoutManager);
//        final FeaturedCategoryAdapter bottomAdapter = new FeaturedCategoryAdapter(this, bottomFeatureCategory, sliderLayout);
//        featuredCategoryRecyclerViewBottom.setAdapter(bottomAdapter);
//        bottomAdapter.SetOnItemClickListener(new FeaturedCategoryAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                Log.d("get pos >>", bottomAdapter.getItem(position).getType()+"***");
//                gotoNextActivity(bottomAdapter.getItem(position));
//            }
//        });
//    }

    private void gotoNextActivity(FeaturedCategory selectedFeature) {
        if (selectedFeature.getType().equals("1")) {

            Category category = new Category();
            category.setCategory_id(selectedFeature.getId());
            category.setName(selectedFeature.getTitle());
            ProductListActivity.category = category;
            gotoNewActivity(ProductListActivity.class);

        }

        if (selectedFeature.getType().equals("2")) {

            Product product = new Product();
            product.setProductId(selectedFeature.getId());
            product.setName(selectedFeature.getTitle());
            ProductDetailsActivity.product = product;
            gotoNewActivity(ProductDetailsActivity.class);
        }

        if (selectedFeature.getType().equals("3")) {
            gotoNewActivity(UserRegisterActivity.class);
        }


    }

    private void getHeightWidth() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        screenWidth = displaymetrics.widthPixels;
    }
    /*private void showDialog()
    {
        CategoryDialogFragment categoryDialogFragment=new CategoryDialogFragment();
        categoryDialogFragment.category=category;
        categoryDialogFragment.show(getSupportFragmentManager(),"dialog");
    }*/

    protected void drawerSetup() {
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        ) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                setToolbarTitle("Kiksha");
                invalidateOptionsMenu();
                syncState();
//                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)drawerLayout.getLayoutParams();
//                params.setMargins(0, 0, 0, 0);
//                drawerLayout.setLayoutParams(params);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setToolbarTitle("Categories");

                Log.d("--drawer--",">>>");
                invalidateOptionsMenu();
                syncState();
//                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)drawerLayout.getLayoutParams();
//                params.setMargins(0, 0, -124, 0);
//                drawerLayout.setLayoutParams(params);
            }

        };

        // drawerLayout.setDrawerListener(mDrawerToggle);
        drawerLayout.addDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle.syncState();

        Log.d("----drawer-->>","---value--");

    }

    @Override
    protected void onResume() {
        value = 0;
        super.onResume();
        if (getCartId() == -1) {
            callCartCreateApi();
        } else if (needToMerge) {
            //callCartMargeApi();
        } else {
            callCartDetailsApi();
        }

    }


    private void callCartDetailsApi(int cartId) {
        Call<CartDetailsResponse> callback = RestClient.get().getCartDetails(String.valueOf(cartId),"");
        NetworkServiceHandler.processCallBack(callback, this);
    }

    @Override
    protected void setBackButton() {
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        Button btn = (Button) categoryCardView.findViewById(R.id.btn);
        categoryCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (category != null) {
                    openDrawer();
                }
            }
        });
    }


    private void openDrawer() {

        drawerLayout.openDrawer(GravityCompat.START);

    }

    private void closeDrawer() {

        drawerLayout.closeDrawers();
        viewPagerauto.invalidate();
    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            this.finish();
        }

    }

    @Override
    protected void onStop() {
   //     sliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    protected void registerBroadcastReceiver() {
    }

    public void onEvent(CloseDrawer Drawerclose) {
        closeDrawer();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void unregisterBroadcast() {

    }

    private void callCartCreateApi() {
        Call<CartCreateResponse> callback = RestClient.get().createNewCart("");
        NetworkServiceHandler.processCallBack(callback, this);
    }


    public void onEvent(CartCreateResponse cartCreateResponse) {
        if (cartCreateResponse.getResponse_code() == 100) {
            int cartID = cartCreateResponse.getData().getCart_id();
            preferenceService.SetPreferenceValue(PreferenceService.CART_KEY, cartID);

            badgeCount = 0;
            updateCartItemCount(badgeCount);
        }
    }

    private void callCartDetailsApi() {
        int cartId = getCartId();
        Log.d("cartId","cartId=="+cartId);

        if (cartId != -1) {
            Call<CartDetailsResponse> callback = RestClient.get().getCartDetails(String.valueOf(cartId),"");
            NetworkServiceHandler.processCallBackWithoutProgressDialog(callback, this);
        }
    }

    public void onEvent(CartDetailsResponse cartDetailsResponse) {
        if (cartDetailsResponse.getResponse_code() == 100 && cartDetailsResponse.getData() != null) {
            badgeCount = Integer.valueOf(cartDetailsResponse.getData().getItems_count());
            updateCartItemCount(badgeCount);
        }
    }

//    @Override
//    public void onRefresh() {
//        onViewCreated();
//    }

}
  /* private void replacfragment()
    {
        DrawerFragment drawerFragment=new DrawerFragment();
        drawerFragment.category=category;
        getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment_navigation_drawer,drawerFragment).addToBackStack(null).commit();
    }*/
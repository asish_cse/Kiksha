package com.bs.ecommerce.kiksha.ui.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.Url;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_web)
public class WebActivity extends BaseActivity {
    @InjectView(R.id.web_view)
    WebView webView;
    ProgressDialog pDialog;

    public static final String INTENT_EXTRA_WEB_TYPE = "intentExtraWebType";
    public static final int TYPE_HOME_DELIVERY = 1;
    public static final int TYPE_RETURN_REFUND = 2;
    public static final int TYPE_PRIVACY_POLICY = 3;
    public static final int TYPE_APP_INTRO = 4;
    public static final int TYPE_FAQ = 5;
    public static final int TYPE_CONTACT_US = 6;
    public static final int TYPE_TERMS = 7;

    private int webType = -1;
    private String url;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onViewCreated() {
        super.onViewCreated();

        webType = getIntent().getIntExtra(INTENT_EXTRA_WEB_TYPE, webType);

        if (webType == TYPE_HOME_DELIVERY) {
            setToolbarTitle("Home Delivery");
            url = Url.HOME_DELIVERY;
        } else if (webType == TYPE_RETURN_REFUND) {
            setToolbarTitle("Return & Refund");
            url = Url.RETURN_REFUND;
        } else if (webType == TYPE_APP_INTRO) {
            setToolbarTitle("App Intro");
            url = Url.APP_INTRO;
        } else if(webType == TYPE_FAQ) {
            setToolbarTitle("Help");
            url = Url.FAQ;
        } else if(webType == TYPE_CONTACT_US) {
            setToolbarTitle("Contact Us");
            url = Url.CONTACT_US;
        } else if(webType == TYPE_PRIVACY_POLICY) {
            setToolbarTitle("Privacy Policy");
            url = Url.PRIVACY;
        } else if(webType == TYPE_TERMS) {
            setToolbarTitle("Terms & Conditions");
            url = Url.TERMS;
        }


        webView.getSettings().setJavaScriptEnabled(true);
        webView.getProgress();
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pDialog = new ProgressDialog(WebActivity.this);
                pDialog.setMessage("Loading...");
                pDialog.setCancelable(true);
                pDialog.setCanceledOnTouchOutside(true);
                pDialog.show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    startActivity(intent);
                    return true;
                } else if (url.startsWith("mailto:")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri data = Uri.parse(url);
                    intent.setData(data);
                    startActivity(intent);
                    return true;
                }
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if ((pDialog != null) && pDialog.isShowing()) {
                    pDialog.dismiss();
                }

                pDialog = null;
            }
        });


        webView.loadUrl(url);

    }
}

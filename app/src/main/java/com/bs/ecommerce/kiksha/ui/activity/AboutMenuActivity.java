package com.bs.ecommerce.kiksha.ui.activity;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/28/2016.
 */
@ContentView(R.layout.activity_about)
public class AboutMenuActivity extends BaseActivity {

    @InjectView(R.id.tv_privacy_policy)
    TextView privacyPolicyTextView;

    @InjectView(R.id.tv_term_condition)
    TextView termConditionTextView;

    @InjectView(R.id.tv_view_app_intro)
    TextView viewAppIntroTextView;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("About");
    }


    @Override
    public void setClicklistener() {
        super.setClicklistener();
        privacyPolicyTextView.setOnClickListener(this);
        viewAppIntroTextView .setOnClickListener(this);
        termConditionTextView .setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int resourceId=v.getId();
        if(resourceId==R.id.tv_privacy_policy) {
            Intent intent = new Intent(this, WebActivity.class);
            intent.putExtra(WebActivity.INTENT_EXTRA_WEB_TYPE, WebActivity.TYPE_PRIVACY_POLICY);
            startActivity(intent);
        }
        else if(resourceId==R.id.tv_term_condition) {
            Intent intent = new Intent(this, WebActivity.class);
            intent.putExtra(WebActivity.INTENT_EXTRA_WEB_TYPE, WebActivity.TYPE_TERMS);
            startActivity(intent);
        }
        else if(resourceId == viewAppIntroTextView.getId()) {
            Intent intent = new Intent(this, WebActivity.class);
            intent.putExtra(WebActivity.INTENT_EXTRA_WEB_TYPE, WebActivity.TYPE_APP_INTRO);
            startActivity(intent);
        }
    }
}

package com.bs.ecommerce.kiksha.ui.activity;

import android.view.View;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/21/2016.
 */
@ContentView(R.layout.activity_personal_detail)
public class PersonalDetailsActivity extends BaseActivity {

    @InjectView(R.id.tv_change_email)
    TextView changeEmailTextView;

    @InjectView(R.id.tv_change_password)
    TextView changePasswordTextView;

    @InjectView(R.id.tv_about_you)
    TextView aboutYouTextView;

    @InjectView(R.id.tv_my_address)
    TextView myAddressTextView;



    @Override
    protected void onViewCreated() {
        super.onViewCreated();
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        changeEmailTextView.setOnClickListener(this);
        changePasswordTextView.setOnClickListener(this);
        aboutYouTextView.setOnClickListener(this);
        myAddressTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resourceId=v.getId();

        if(resourceId==R.id.tv_change_email)
        {
            gotoNewActivity(ChangeEmailActivity.class);
        }

        else if(resourceId==R.id.tv_change_password)
        {
            gotoNewActivity(ChangePasswordActivity.class);
        } else if(resourceId == aboutYouTextView.getId()) {
            gotoNewActivity(AboutYouActivity.class);
        }

        else if(resourceId==R.id.tv_my_address)
        {
            gotoNewActivity(AddressBookActivity.class);
        }

    }
}

package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 5/4/2016.
 */
public class BaseAddressModel {

    private String firstname;
    private String lastname;
    private String middlename;
    private String telephone;
    private String fax;
    private String company;
    private String postcode;
    private String city;
    private String street[] ;
    private boolean isDefaultBillingAddress;
    private boolean isDefaultShippingAddress;
    private String region;
    private String area;

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String[] getStreet() {
        return street;
    }

    public void setStreet(String[] street) {
        this.street = street;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    public boolean isDefaultBillingAddress() {
        return isDefaultBillingAddress;
    }

    public void setDefaultBillingAddress(boolean defaultBillingAddress) {
        isDefaultBillingAddress = defaultBillingAddress;
    }

    public boolean isDefaultShippingAddress() {
        return isDefaultShippingAddress;
    }

    public void setDefaultShippingAddress(boolean defaultShippingAddress) {
        isDefaultShippingAddress = defaultShippingAddress;
    }




    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}

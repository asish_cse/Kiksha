package com.bs.ecommerce.kiksha.ui.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.CartDetails;
import com.bs.ecommerce.kiksha.model.CustomerSetRequest;
import com.bs.ecommerce.kiksha.model.CustomerSetResponse;
import com.bs.ecommerce.kiksha.model.Message;
import com.bs.ecommerce.kiksha.model.MessageResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.adapter.CheckoutAdapter;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 5/3/2016.
 */
@ContentView(R.layout.activity_checkout)
public class CheckoutActivity extends BaseActivity {

    @InjectView(R.id.rclv_checkout_product)
    RecyclerView checkoutProductRecyclerView;

    @InjectView(R.id.btn_checkout)
    Button checkoutBtn;

    @InjectView(R.id.btn_call_to_order)
    Button calltToOrderBtn;


    public static CartDetails cartDetails;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Checkout");
        populateRecyclerView();
    }

    private void populateRecyclerView() {
        checkoutProductRecyclerView.setLayoutManager
                (getLinearLayoutManager(LinearLayoutManager.VERTICAL));

        CheckoutAdapter checkoutAdapter = new CheckoutAdapter(this, cartDetails.getItems());
        checkoutProductRecyclerView.setAdapter(checkoutAdapter);
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        checkoutBtn.setOnClickListener(this);
        calltToOrderBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int resourceId = v.getId();

        if (resourceId == R.id.btn_call_to_order) {
        } else if (resourceId == R.id.btn_checkout) {
            if(isLoggedIn()) {
                callSetCustomerApi();
            } else {
                gotoNewActivity(UserRegisterActivity.class);
            }
        }
    }

    private void callSetCustomerApi() {
        int cartId = getCartId();

        if (cartId != -1) {
            Call<CustomerSetResponse> callback = RestClient.get().setCustomer(getCustomerSetReqObject(cartId));
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
           // showInfoSnackBar(getResources().getString(R.string.cart_not_found));
            toastMsg(getResources().getString(R.string.cart_not_found));
        }
    }

    private void toastMsg(String str){
        Toast toast = Toast.makeText(this, Html.fromHtml("<font size='12' color='#FFFFFF'  ><b>" + str + "</b></font>"), Toast.LENGTH_LONG);
        // toast.setGravity(Gravity.TOP, 0, 0);
        // toast.setGravity(Gravity.CENTER, 50, 50);
        toast.getView().setPadding(20, 25, 20, 25);
        toast.getView().setBackgroundColor(this.getResources().getColor(R.color.listdivider));
        toast.show();
    }

    private CustomerSetRequest getCustomerSetReqObject(int cartId) {
        CustomerSetRequest customerSetRequest = new CustomerSetRequest();
        customerSetRequest.setCartId(cartId);
        customerSetRequest.setCustomerId(Integer.valueOf(preferenceService.GetPreferenceValue(PreferenceService.ENTITY_KEY)));
        return customerSetRequest;
    }

    public void onEvent(CustomerSetResponse customerSetResponse) {
        if (customerSetResponse.getResponse_code() == 100) {
            gotoNewActivity(BillingAddressActivity.class);
        }
    }

}

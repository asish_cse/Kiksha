package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 4/20/2016.
 */
public class HomePageModelResponse extends BaseResponse {
    private HomePageModel data;

    public HomePageModel getData() {
        return data;
    }

    public void setData(HomePageModel data) {
        this.data = data;
    }

}

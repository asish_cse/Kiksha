package com.bs.ecommerce.kiksha.ui.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.Url;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/28/2016.
 */
@ContentView(R.layout.activity_follow_us)
public class FollowUsActivity extends BaseActivity {
    @InjectView(R.id.tv_facebook)
    View facebookView;

    @InjectView(R.id.tv_google_plus)
    TextView googlePlusTextView;

    @InjectView(R.id.tv_linkedin)
    TextView linkedInTextView;

    @InjectView(R.id.tv_twitter)
    TextView twitterTextView;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Follow Us");
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        facebookView.setOnClickListener(this);
        googlePlusTextView.setOnClickListener(this);
        linkedInTextView.setOnClickListener(this);
        twitterTextView.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int resourceId = v.getId();
        if (resourceId == facebookView.getId()) {
            /*Uri uriUrl = Uri.parse(Url.FACEBOOK);
            Intent intent = new Intent(Intent.ACTION_VIEW, uriUrl);
            startActivity(intent);*/
            String url = getFacebookPageURL();
            Intent facebookIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(facebookIntent);
        } else if (resourceId == R.id.tv_google_plus) {

        } else if (resourceId == R.id.tv_linkedin) {

        } else if (resourceId == R.id.tv_twitter) {

        }
    }

    private String getFacebookPageURL() {
        PackageManager packageManager = getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + Url.KIKSHA_FB;
            } else { //older versions of fb app
                return "fb://page/" + Url.FB_NAME;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return Url.KIKSHA_FB;
        }
    }
}

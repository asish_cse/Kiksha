package com.bs.ecommerce.kiksha.event;

import android.util.Log;

/**
 * Created by Ashraful on 4/20/2016.
 */
public class NewActivityTransition {
    public NewActivityTransition()
    {

    }
    public NewActivityTransition(Class<?>activityClass)
    {
        this.activityClass=activityClass;

    }
    public Class<?>activityClass;
}

package com.bs.ecommerce.kiksha.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.bs.ecommerce.kiksha.R;

import roboguice.inject.ContentView;

@ContentView(R.layout.activity_faqs)
public class FaqsActivity extends BaseActivity {

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Faqs");
    }
}

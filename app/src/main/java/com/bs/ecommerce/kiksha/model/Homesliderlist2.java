package com.bs.ecommerce.kiksha.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Homesliderlist2 {

    @SerializedName("data")
    @Expose
    private ArrayList<HomeSlider> contacts = new ArrayList<>();

    /**
     * @return The contacts
     */
    public ArrayList<HomeSlider> getimageList() {
        return contacts;
    }

    /**
     * @param contacts The contacts
     */
    public void setContacts(ArrayList<HomeSlider> contacts) {
        this.contacts = contacts;
    }
}
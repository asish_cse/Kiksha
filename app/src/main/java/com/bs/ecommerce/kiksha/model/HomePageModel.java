package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by Ashraful on 4/20/2016.
 */
public class HomePageModel {
    private List<FeaturedCategory> featured_category;
    private List<SliderImage> silder_image;

    public List<FeaturedCategory> getFeatured_category() {
        return featured_category;
    }

    public void setFeatured_category(List<FeaturedCategory> featured_category) {
        this.featured_category = featured_category;
    }

    public List<SliderImage> getSilder_image() {
        return silder_image;
    }

    public void setSilder_image(List<SliderImage> silder_image) {
        this.silder_image = silder_image;
    }
}

package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/9/2016.
 */
public class CartCreateResponse extends BaseResponse {
    private CartCreateResponseData data;

    public CartCreateResponseData getData() {
        return data;
    }

    public void setData(CartCreateResponseData data) {
        this.data = data;
    }
}

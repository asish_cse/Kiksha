package com.bs.ecommerce.kiksha.ui.custom_view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.bs.ecommerce.kiksha.R;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;

/**
 * Created by BS62 on 22-Sep-16.
 */

public class CustomSliderView extends BaseSliderView {
     public CustomSliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.custom_slider_layout, null);
        ImageView target = (ImageView) v.findViewById(R.id.daimajia_slider_image);
        bindEventAndShow(v, target);
        return v;
    }
}

package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/16/2016.
 */
public class OrderReviewResponse extends BaseResponse {
    private OrderReviewData data;

    public OrderReviewData getData() {
        return data;
    }

    public void setData(OrderReviewData data) {
        this.data = data;
    }
}

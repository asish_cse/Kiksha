package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by BS62 on 5/9/2016.
 */
public class CartAddRequest {
    private int cart_id;
    private int product_id;
    private int qty;
    private List<Attribute> attribute;

    public int getCart_id() {
        return cart_id;
    }

    public void setCart_id(int cart_id) {
        this.cart_id = cart_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public List<Attribute> getAttribute() {
        return attribute;
    }

    public void setAttribute(List<Attribute> attribute) {
        this.attribute = attribute;
    }
}

package com.bs.ecommerce.kiksha.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BestsellerList2 {

    @SerializedName("data")
    @Expose
    private ArrayList<BestSeller> contacts3 = new ArrayList<>();

    /**
     * @return The contacts
     */
    public ArrayList<BestSeller> getimageList() {
        return contacts3;
    }

    /**
     * @param contacts The contacts
     */
    public void setContacts(ArrayList<BestSeller> contacts) {
        this.contacts3 = contacts;
    }
}
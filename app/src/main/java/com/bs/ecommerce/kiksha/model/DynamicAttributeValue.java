package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 4/15/2016.
 */
public class DynamicAttributeValue {

    private String product_super_attribute_id;
    private String label;
    private String default_label;
    private String store_label;
    private boolean use_default_value;
    private String value_index;

    public String getValue_index() {
        return value_index;
    }

    public void setValue_index(String value_index) {
        this.value_index = value_index;
    }

    public String getDefault_label() {
        return default_label;
    }

    public void setDefault_label(String default_label) {
        this.default_label = default_label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getProduct_super_attribute_id() {
        return product_super_attribute_id;
    }

    public void setProduct_super_attribute_id(String product_super_attribute_id) {
        this.product_super_attribute_id = product_super_attribute_id;
    }

    public String getStore_label() {
        return store_label;
    }

    public void setStore_label(String store_label) {
        this.store_label = store_label;
    }

    public boolean isUse_default_value() {
        return use_default_value;
    }

    public void setUse_default_value(boolean use_default_value) {
        this.use_default_value = use_default_value;
    }

}

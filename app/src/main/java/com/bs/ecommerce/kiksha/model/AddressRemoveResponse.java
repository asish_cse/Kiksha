package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 13-Oct-16.
 */

public class AddressRemoveResponse extends BaseResponse {
    private Message data;

    public Message getData() {
        return data;
    }

    public void setData(Message data) {
        this.data = data;
    }
}

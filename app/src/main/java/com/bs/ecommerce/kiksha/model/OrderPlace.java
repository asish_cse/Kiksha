package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/30/2016.
 */
public class OrderPlace {
    private String order_id;
    private String msg;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

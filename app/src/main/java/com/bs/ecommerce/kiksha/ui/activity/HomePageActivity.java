package com.bs.ecommerce.kiksha.ui.activity;

import android.content.Context;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ScrollView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.SharedPreferencesHelper;
import com.bs.ecommerce.kiksha.event.CloseDrawer;
import com.bs.ecommerce.kiksha.model.CartCreateResponse;
import com.bs.ecommerce.kiksha.model.CartDetailsResponse;
import com.bs.ecommerce.kiksha.model.CartMargeRequest;
import com.bs.ecommerce.kiksha.model.CartMargeResponse;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.model.CategoryOfferzoneResponse;
import com.bs.ecommerce.kiksha.model.CategoryResponse;
import com.bs.ecommerce.kiksha.model.FeaturedCategory;
import com.bs.ecommerce.kiksha.model.HomePageModel;
import com.bs.ecommerce.kiksha.model.HomePageModelResponse;
import com.bs.ecommerce.kiksha.model.MessageResponse;
import com.bs.ecommerce.kiksha.model.Product;
import com.bs.ecommerce.kiksha.model.SliderImage;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.adapter.FeaturedCategoryAdapter;
import com.bs.ecommerce.kiksha.ui.custom_view.CustomSliderView;
import com.bs.ecommerce.kiksha.ui.fragment.CategoryExpandableFragment;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;

import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/4/2016.
 */
@ContentView(R.layout.activity_home_page)
public class HomePageActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @InjectView(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @InjectView(R.id.drawer_layout)
    public DrawerLayout drawerLayout;

    @InjectView(R.id.cv_category)
    CardView categoryCardView;

    @InjectView(R.id.fragment_navigation_drawer)
    FrameLayout navigationDrawer;

    @InjectView(R.id.featured_category_rclv_top)
    RecyclerView featuredCategoryRecyclerViewTop;

    @InjectView(R.id.featured_category_rclv_bottom)
    RecyclerView featuredCategoryRecyclerViewBottom;

    @InjectView(R.id.custom_indicator)
    PagerIndicator pagerIndicator;

    @InjectView(R.id.slider)
    SliderLayout sliderLayout;

    @InjectView(R.id.sv_home_page)
    ScrollView scrollView;

    ActionBarDrawerToggle mDrawerToggle;
    GridLayoutManager topGridLayoutManager;
    GridLayoutManager bottomGridLayoutManager;

    Category category;
    int screenWidth;
    private int viewWidth;
    public static final String MERGE_EXTRA = "needToMerge";
    public static boolean needToMerge = false;
    private Context con;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();

        try {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setLogo(R.mipmap.ic_launcher);
        } catch (Exception ex) {
            //
        }

        swipeRefreshLayout.setOnRefreshListener(this);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY();
                if (scrollY == 0) {
                    swipeRefreshLayout.setEnabled(true);
                } else {
                    swipeRefreshLayout.setEnabled(false);
                }
            }
        });
        topGridLayoutManager = new GridLayoutManager(this, 2);
        bottomGridLayoutManager = new GridLayoutManager(this, 2);

        con = this;
        drawerSetup();
        getHeightWidth();
        callCategoryListApi();
        callHomePageApi();

        //callCartDetailsApi();
        checkNotificationBundle();
        swipeRefreshLayout.setRefreshing(false);
    }

    private void checkNotificationBundle() {
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                String value = getIntent().getExtras().getString(key);
                Log.d("HomePageActivity", key + " : " + value);

            }
        }
    }

    private void callCategoryListApi() {
        NetworkServiceHandler.processCallBack(RestClient.get().getCategories(""), this);

//       // NetworkServiceHandler.processCallBack(RestClient.get().getOfferzonemenu(), this);
//        Call<CategoryOfferzoneResponse> callback = RestClient.get().getOfferzonemenu();
//        NetworkServiceHandler.processCallBack(callback, this);
    }

    public void onEvent(CategoryResponse categoryResponse) {
        category = categoryResponse.getData();
        replacfragment();
    }


    private void replacfragment() {
        CategoryExpandableFragment drawerFragment = new CategoryExpandableFragment();
        drawerFragment.categoryList = category;
        getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment_navigation_drawer, drawerFragment).commit();

    }


    private void callHomePageApi() {
        NetworkServiceHandler.processCallBack(
                RestClient.get().getHomePageData(), this);

    }

    public void onEvent(HomePageModelResponse homePageModelResponse) {
        HomePageModel homePageModel = homePageModelResponse.getData();
        if (homePageModel != null) {
            Log.d("===homepage onevent=","----");
            setTopFeatureCategory(homePageModel.getFeatured_category().subList(0,4));
            setSlider(homePageModel.getSilder_image());
            setBottomFeatureCategory(homePageModel.getFeatured_category().subList(4, 8));
        }

    }

    private void setTopFeatureCategory(List<FeaturedCategory> topFeatureCategory) {
        featuredCategoryRecyclerViewTop.setLayoutManager(topGridLayoutManager);
        final FeaturedCategoryAdapter topAdapter = new FeaturedCategoryAdapter(this, topFeatureCategory, sliderLayout);
        featuredCategoryRecyclerViewTop.setAdapter(topAdapter);

        topAdapter.SetOnItemClickListener(new FeaturedCategoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // FeaturedCategory selectedFeature = topAdapter.getItem(position);
                Log.d("===Homepage=====","----"+position);
                gotoNextActivity(topAdapter.getItem(position));
               // SharedPreferencesHelper.setMenu(con,"99");
                if(position == 2)
                    SharedPreferencesHelper.setMenu(con,"8");
                else
                    SharedPreferencesHelper.setMenu(con,"99");
            }
        });
    }

    private void setSlider(List<SliderImage> sliderImageList) {
        sliderLayout.removeAllSliders();
        sliderLayout.setCustomIndicator(pagerIndicator);
        for (final SliderImage sliderImage : sliderImageList) {
            CustomSliderView defaultSliderView = new CustomSliderView(this);
            defaultSliderView.empty(R.drawable.placeholder);
            defaultSliderView.image(sliderImage.getImg())
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop);

            SharedPreferencesHelper.setMenu(con,"99");



            defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override public void onSliderClick(BaseSliderView slider) {
                    SharedPreferencesHelper.setMenu(con,"99");
                    if (sliderImage.getType().equals("1")) {
                        Log.d("expandable frag pos--1",">>>");
                        Category category = new Category();
                        category.setCategory_id(sliderImage.getId());
                        category.setName(sliderImage.getTitle());
                        ProductListActivity.category = category;
                        gotoNewActivity(ProductListActivity.class);

                    }

                    if (sliderImage.getType().equals("2")) {
                        Log.d("expandable frag pos--2",">>>");
                        Product product = new Product();
                        product.setProductId(sliderImage.getId());
                        product.setName(sliderImage.getTitle());
                        ProductDetailsActivity.product = product;

                        gotoNewActivity(ProductDetailsActivity.class);
                    }
                }
            });

            sliderLayout.addSlider(defaultSliderView);


        }
    }

    private void setBottomFeatureCategory(List<FeaturedCategory> bottomFeatureCategory) {
        Log.d("size", bottomFeatureCategory.size()+"");
        featuredCategoryRecyclerViewBottom.setLayoutManager(bottomGridLayoutManager);
        final FeaturedCategoryAdapter bottomAdapter = new FeaturedCategoryAdapter(this, bottomFeatureCategory, sliderLayout);
        featuredCategoryRecyclerViewBottom.setAdapter(bottomAdapter);
        bottomAdapter.SetOnItemClickListener(new FeaturedCategoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d("get pos >>", bottomAdapter.getItem(position).getType()+"***");
                gotoNextActivity(bottomAdapter.getItem(position));
            }
        });
    }

    private void gotoNextActivity(FeaturedCategory selectedFeature) {
        if (selectedFeature.getType().equals("1")) {

            Category category = new Category();
            category.setCategory_id(selectedFeature.getId());
            category.setName(selectedFeature.getTitle());
            ProductListActivity.category = category;
            gotoNewActivity(ProductListActivity.class);

        }

        if (selectedFeature.getType().equals("2")) {

            Product product = new Product();
            product.setProductId(selectedFeature.getId());
            product.setName(selectedFeature.getTitle());
            ProductDetailsActivity.product = product;
            gotoNewActivity(ProductDetailsActivity.class);
        }

        if (selectedFeature.getType().equals("3")) {
            gotoNewActivity(UserRegisterActivity.class);
        }


    }

    private void getHeightWidth() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        screenWidth = displaymetrics.widthPixels;
    }
    /*private void showDialog()
    {
        CategoryDialogFragment categoryDialogFragment=new CategoryDialogFragment();
        categoryDialogFragment.category=category;
        categoryDialogFragment.show(getSupportFragmentManager(),"dialog");
    }*/

    protected void drawerSetup() {
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        ) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                setToolbarTitle("Kiksha");
                invalidateOptionsMenu();
                syncState();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setToolbarTitle("Categories");

                Log.d("--drawer--",">>>");
                invalidateOptionsMenu();
                syncState();
            }

        };

        // drawerLayout.setDrawerListener(mDrawerToggle);
        drawerLayout.addDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle.syncState();

        Log.d("----drawer-->>","---value--");

    }

    @Override
    protected void onResume() {
        value = 0;
        super.onResume();
        if (getCartId() == -1) {
            callCartCreateApi();
        } else if (needToMerge) {
            //callCartMargeApi();
        } else {
            callCartDetailsApi();
        }

    }


    private void callCartDetailsApi(int cartId) {
        Call<CartDetailsResponse> callback = RestClient.get().getCartDetails(String.valueOf(cartId),"");
        NetworkServiceHandler.processCallBack(callback, this);
    }

    @Override
    protected void setBackButton() {
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        Button btn = (Button) categoryCardView.findViewById(R.id.btn);
        categoryCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (category != null) {
                    openDrawer();
                }
            }
        });
    }


    private void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    private void closeDrawer() {
        drawerLayout.closeDrawers();
    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            this.finish();
        }

    }

    @Override
    protected void onStop() {
        sliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    protected void registerBroadcastReceiver() {
    }

    public void onEvent(CloseDrawer Drawerclose) {
        closeDrawer();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void unregisterBroadcast() {

    }

    private void callCartCreateApi() {
        Call<CartCreateResponse> callback = RestClient.get().createNewCart("");
        NetworkServiceHandler.processCallBack(callback, this);
    }


    public void onEvent(CartCreateResponse cartCreateResponse) {
        if (cartCreateResponse.getResponse_code() == 100) {
            int cartID = cartCreateResponse.getData().getCart_id();
            preferenceService.SetPreferenceValue(PreferenceService.CART_KEY, cartID);

            badgeCount = 0;
            updateCartItemCount(badgeCount);
        }
    }

    private void callCartDetailsApi() {
        int cartId = getCartId();
        Log.d("cartId","cartId=="+cartId);

        if (cartId != -1) {
            Call<CartDetailsResponse> callback = RestClient.get().getCartDetails(String.valueOf(cartId),"");
            NetworkServiceHandler.processCallBackWithoutProgressDialog(callback, this);
        }
    }

    public void onEvent(CartDetailsResponse cartDetailsResponse) {
        if (cartDetailsResponse.getResponse_code() == 100 && cartDetailsResponse.getData() != null) {
            badgeCount = Integer.valueOf(cartDetailsResponse.getData().getItems_count());
            updateCartItemCount(badgeCount);
        }
    }

    @Override
    public void onRefresh() {
        onViewCreated();
    }

}
  /* private void replacfragment()
    {
        DrawerFragment drawerFragment=new DrawerFragment();
        drawerFragment.category=category;
        getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment_navigation_drawer,drawerFragment).addToBackStack(null).commit();
    }*/
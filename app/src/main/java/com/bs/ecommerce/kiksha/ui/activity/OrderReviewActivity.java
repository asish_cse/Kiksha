package com.bs.ecommerce.kiksha.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.CartId;
import com.bs.ecommerce.kiksha.model.CartItems;
import com.bs.ecommerce.kiksha.model.MessageResponse;
import com.bs.ecommerce.kiksha.model.OrderPlaceResponse;
import com.bs.ecommerce.kiksha.model.OrderReviewData;
import com.bs.ecommerce.kiksha.model.OrderReviewItem;
import com.bs.ecommerce.kiksha.model.OrderReviewResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.ui.adapter.OrderReviewAdapter;
import com.sslwireless.sdk.Activity.BankList;
import com.sslwireless.sdk.ErrorKeys;
import com.sslwireless.sdk.JsonModel.TransactionInfo;
import com.sslwireless.sdk.Model.InputKeys;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import static org.acra.ACRA.log;

@ContentView(R.layout.activity_order_review)
public class OrderReviewActivity extends BaseActivity {
    @InjectView(R.id.lv_order_item)
    ListView orderItemLv;
    @InjectView(R.id.tv_order_subtotal)
    TextView orderSubtotalTv;
    @InjectView(R.id.tv_shipping_cost_title)
    TextView shippingCostTitleTv;

    @InjectView(R.id.tv_order_shipping_amount)
    TextView shippingAmount;

    @InjectView(R.id.tv_order_discount)
    TextView shippingCostTv;
    @InjectView(R.id.tv_grand_total)
    TextView grandTotalTv;
    @InjectView(R.id.btn_place_order)
    Button placeOrderBtn;
    @InjectView(R.id.et_coupon_code)
    EditText couponCodeEt;
    private OrderReviewAdapter orderReviewAdapter;
    private OrderReviewData orderReviewData;
    private List<OrderReviewItem> orderReviewItemList = new ArrayList<>();

    private static String paymentMethodCode = "";
    private final int REQUEST_CODE = 1;

    public String successMsg = "";
    public String orderId;
    public float discount_amount = 0;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Order Review");
        callOrderReviewApi();
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        placeOrderBtn.setOnClickListener(this);
    }

    private void callOrderReviewApi() {
        int cartId = getCartId();
        if (cartId != -1) {
            Call<OrderReviewResponse> callback = RestClient.get().getOrderData(cartId + "", "");
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }
    }

    public void onEvent(OrderReviewResponse orderReviewResponse) {
        orderReviewItemList = new ArrayList<>();
        if (orderReviewResponse.getResponse_code() == 100) {
            orderReviewData = orderReviewResponse.getData();
            paymentMethodCode = orderReviewResponse.getData().getPayment_method();
            orderReviewItemList = orderReviewResponse.getData().getItems();
            orderReviewAdapter = new OrderReviewAdapter(this, orderReviewItemList);
            orderItemLv.setAdapter(orderReviewAdapter);

            orderSubtotalTv.setText("\u09f3 " + orderReviewResponse.getData().getSubtotal());
//            shippingCostTitleTv.setText("Shipping & Handling");
            if (orderReviewResponse.getData().getShipping_cost() != null) {
                shippingAmount.setText("৳ " + orderReviewResponse.getData().getShipping_cost());
            } else {
                shippingAmount.setText("৳ " + 0);
            }

            grandTotalTv.setText("৳ " + orderReviewResponse.getData().getGrand_total());
            for (OrderReviewItem cart : orderReviewItemList) {
                discount_amount = discount_amount + Float.valueOf(cart.getDiscount_amount());
            }
            shippingCostTv.setText("-৳ " + discount_amount);
            discount_amount=0;
            log.d("-->>" + orderReviewResponse.getData().getCoupon_code(), "==||==");

            couponCodeEt.setText(orderReviewResponse.getData().getCoupon_code());
            couponCodeEt.setEnabled(false);
            couponCodeEt.setFocusableInTouchMode(false);

        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int resId = v.getId();
        if (resId == R.id.btn_place_order) {
            int cartId = getCartId();
            if (cartId != -1) {
                Call<OrderPlaceResponse> callback = RestClient.get().placeOrder(getCartIdObject(cartId));    // changed
                NetworkServiceHandler.processCallBack(callback, this);
            } else {
                showInfoSnackBar(getResources().getString(R.string.cart_not_found));
            }
        }
    }

    public void onEvent(OrderPlaceResponse orderPlaceResponse) {
        if (orderPlaceResponse.getResponse_code() == 100) {
            showInfoSnackBar("Order placed successfully");

            successMsg = orderPlaceResponse.getData().getMsg();
            orderId = orderPlaceResponse.getData().getOrder_id();
            Log.d(")))>>" + successMsg, "===>>");

            if (paymentMethodCode.equalsIgnoreCase("Sslcommerz")) {
                Intent intent = new Intent(OrderReviewActivity.this, BankList.class);
                intent.putExtra(InputKeys.STORE_ID, "kiksha001live");
                intent.putExtra(InputKeys.STORE_PASSWORD, "kiksha001live14939");
                intent.putExtra(InputKeys.TOTAL_AMOUNT, orderReviewData.getGrand_total());
                intent.putExtra(InputKeys.SDK_TYPE, "LIVE");
                intent.putExtra(InputKeys.SDK_CATEGORY, InputKeys.SDK_BANK_LIST);
                intent.putExtra(InputKeys.CURRENCY, orderReviewData.getStore_currency_code());
                intent.putExtra(InputKeys.TRANSACTION_ID, orderPlaceResponse.getData().getOrder_id());

                startActivityForResult(intent, REQUEST_CODE);
            } else {
                OrderSuccessActivity.orderId = orderId;
                OrderSuccessActivity.successMsg = successMsg;
                gotoNewActivity(OrderSuccessActivity.class);
            }
            /*OrderSuccessActivity.successMsg = orderPlaceResponse.getData().getMsg();
            gotoNewActivity(OrderSuccessActivity.class);*/
        }
    }

    private CartId getCartIdObject(int cartId) {
        CartId cartIdObject = new CartId();
        cartIdObject.setCartId(cartId);
        return cartIdObject;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            /*Bundle bundle = data.getExtras();
            for (String key : bundle.keySet()) {
                Object value = bundle.get(key);
                Log.d("tag", String.format("%s %s (%s)", key,
                        value.toString(), value.getClass().getName()));
            }*/

            if (resultCode == RESULT_OK) {
                Log.d("tag", "OK");
                TransactionInfo returnInfo = (TransactionInfo) data.getExtras().getSerializable("transaction_info");
                //showInfoSnackBar(returnInfo.getStatus());
                Toast.makeText(getApplicationContext(), returnInfo.getStatus(), Toast.LENGTH_SHORT).show();
            }

            if (resultCode == RESULT_CANCELED) {
                /*Log.d("error_key",""+data.getIntExtra("error_key",0));
                if(data.getIntExtra("error_key",0) == ErrorKeys.USER_INPUT_ERROR)
                    Log.e("Check Error","User Input Error");*/
                if (data.getIntExtra("error_key", 0) == ErrorKeys.USER_INPUT_ERROR) {
                    //showInfoSnackBar("User Input Error");
                    Toast.makeText(getApplicationContext(), "User Input Error", Toast.LENGTH_SHORT).show();
                } else if (data.getIntExtra("error_key", 0) == ErrorKeys.CANCEL_TRANSACTION_ERROR) {
                    //showInfoSnackBar("Transaction Canceled");
                    Toast.makeText(getApplicationContext(), "Transaction Canceled", Toast.LENGTH_SHORT).show();
                } else if (data.getIntExtra("error_key", 0) == ErrorKeys.DATA_PARSING_ERROR) {
                    //showInfoSnackBar("Data Parsing Error");
                    Toast.makeText(getApplicationContext(), "Data Parsing Error", Toast.LENGTH_SHORT).show();
                } else if (data.getIntExtra("error_key", 0) == ErrorKeys.INTERNET_CONNECTION_ERROR) {
                    //showInfoSnackBar("Internet Not Connected");
                    Toast.makeText(getApplicationContext(), "Internet Not Connected", Toast.LENGTH_SHORT).show();
                }
            }

            OrderSuccessActivity.orderId = orderId;
            OrderSuccessActivity.successMsg = successMsg;
            gotoNewActivity(OrderSuccessActivity.class);
        }
    }
}

package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by Ashraful on 5/9/2016.
 */
public class ProductSortParameterResponse extends BaseResponse {

    public List<NameValuePair> getSort_params() {
        return sort_params;
    }

    public void setSort_params(List<NameValuePair> sort_params) {
        this.sort_params = sort_params;
    }

    private List<NameValuePair> sort_params;
}

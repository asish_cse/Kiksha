package com.bs.ecommerce.kiksha.ui.activity;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.CartId;
import com.bs.ecommerce.kiksha.model.AllShippingMethodResponse;
import com.bs.ecommerce.kiksha.model.MessageResponse;
import com.bs.ecommerce.kiksha.model.ShippingMethod;
import com.bs.ecommerce.kiksha.model.ShippingMethodSetRequest;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;

import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_shipping_method)
public class ShippingMethodActivity extends BaseActivity {
    @InjectView(R.id.rg_shipping_methods)
    RadioGroup shippingMethodsGroup;
    @InjectView(R.id.btn_continue)
    Button continueBtn;

    private static LayoutInflater inflater = null;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Shipping Method");
        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        getShippingMethodApi();
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        continueBtn.setOnClickListener(this);
    }

    private void getShippingMethodApi() {
        int cartId = getCartId();
        if (cartId != -1) {
            Call<AllShippingMethodResponse> callback = RestClient.get().getAllShippingMethod(getCartIdObject(cartId)); // changed
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }
    }

    private CartId getCartIdObject(int cartId) {
        CartId cartIdRequest = new CartId();
        cartIdRequest.setCartId(cartId);
        return cartIdRequest;
    }

    public void onEvent(AllShippingMethodResponse allShippingMethodResponse) {
        if (allShippingMethodResponse.getResponse_code() == 100) {
            setUpRadioButton(allShippingMethodResponse.getData());
        }
    }

    private void setUpRadioButton(List<ShippingMethod> data) {
        for (int i=0; i<data.size()-1; i++) {
            View radioButtonView = inflater.inflate(R.layout.item_radio_button, null);
            RadioButton radioButton = (RadioButton) radioButtonView.findViewById(R.id.radio_button);
            radioButton.setText(data.get(i).getMethod_title() + " \u09f3 " + data.get(i).getPrice());
            radioButton.setId(i);
            radioButton.setTag(data.get(i));
            if (i == 0) {
                radioButton.setChecked(true);
            }
            shippingMethodsGroup.addView(radioButton);
        }
    }

    private void setShippingMethodApi() {
        int cartId = getCartId();
        if (cartId != -1) {
            Call<MessageResponse> callback = RestClient.get().setShippingMethod(setShippingMethodRequest(cartId));
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar("Cart not found");
        }

    }

    private ShippingMethodSetRequest setShippingMethodRequest(int cartId) {
        int id = shippingMethodsGroup.getCheckedRadioButtonId();
        View radioButton = shippingMethodsGroup.findViewById(id);
        int radioId = shippingMethodsGroup.indexOfChild(radioButton);
        RadioButton rbtn = (RadioButton) shippingMethodsGroup.getChildAt(radioId);
        ShippingMethod shippingMethod = (ShippingMethod) rbtn.getTag();

        ShippingMethodSetRequest shippingMethodSetRequest = new ShippingMethodSetRequest();
        shippingMethodSetRequest.setCartId(cartId);
        Log.d("shippingMethod","cartId--"+cartId);
        Log.d("shippingMethod","Code--"+shippingMethod.getCode());
        shippingMethodSetRequest.setMethod_code(shippingMethod.getCode());
        return shippingMethodSetRequest;
    }

    public void onEvent(MessageResponse messageResponse) {
        if (messageResponse.getResponse_code() == 100) {
            showInfoSnackBar(messageResponse.getData().getMsg());
            gotoNewActivity(PaymentMethodActivity.class);
        }

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resId = v.getId();
        if (resId == R.id.btn_continue) {
            setShippingMethodApi();
        }
    }
}

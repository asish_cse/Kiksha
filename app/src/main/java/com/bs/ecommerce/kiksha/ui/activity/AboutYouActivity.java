package com.bs.ecommerce.kiksha.ui.activity;

import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.UserAuthentication;
import com.bs.ecommerce.kiksha.model.UserAuthenticationResponse;
import com.bs.ecommerce.kiksha.model.UserInfoUpdateRequest;
import com.bs.ecommerce.kiksha.model.UserInfoUpdateResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.view.FormViews;

import retrofit2.Call;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/21/2016.
 */
public class AboutYouActivity extends UserRegisterActivity {

    @InjectView(R.id.tv_header)
    TextView headerTextView;
    @InjectView(R.id.tv_password_title)
    TextView passwordTitlleTextView;
    @InjectView(R.id.tv_email_title)
    TextView emailTitlleTextView;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("About You");
        callCustomerDetail();
        signInBtn.setVisibility(View.GONE);
        passwordTitlleTextView.setVisibility(View.GONE);
        passwordEdittextText.setVisibility(View.GONE);
        emailEditText.setVisibility(View.GONE);
        emailTitlleTextView.setVisibility(View.GONE);
        headerTextView.setText("About You");

    }

    protected  void validateForm()
    {
        boolean isValid=true;
        if(!FormViews.isValidWithMark(firstNameEditText,"First Name"))
            isValid=false;
        if(!FormViews.isValidWithMark(lastNameEditText,"Last Name"))
            isValid=false;
      /*  if(!FormViews.isValidWithMark(emailEditText,"Email"))
            isValid=false;*/
     
        if(isValid)
            updateUserInfo();
    }

    private void updateUserInfo() {
        String entity_id = preferenceService.GetPreferenceValue(PreferenceService.ENTITY_KEY);
        UserInfoUpdateRequest registrationRequest=new UserInfoUpdateRequest();
        registrationRequest.setFirstname(FormViews.getTexBoxFieldValue(firstNameEditText));
        registrationRequest.setLastname(FormViews.getTexBoxFieldValue(lastNameEditText));
        registrationRequest.setEntity_id(entity_id);

        Call<UserInfoUpdateResponse> callback= RestClient.get().updateUserInfo(registrationRequest);
        NetworkServiceHandler.processCallBack(callback,this);

    }

    private void callCustomerDetail() {
        String entity_id = preferenceService.GetPreferenceValue(PreferenceService.ENTITY_KEY);
        Call<UserAuthenticationResponse> callback = RestClient.get().getCustomerDetail(entity_id);
        NetworkServiceHandler.processCallBack(callback, this);
    }
    public void onEvent(UserAuthenticationResponse baseResponse)
    {
        if(baseResponse.getResponse_code()==100)
        {
            UserAuthentication userAuthentication=baseResponse.getData();
            firstNameEditText.setText(userAuthentication.getFirstname());
            lastNameEditText.setText(userAuthentication.getLastname());
            emailEditText.setText(userAuthentication.getEmail());
        }
    }
    public void onEvent(UserInfoUpdateResponse baseResponse)
    {
        if(baseResponse.getResponse_code()==100)
        {
          //  Toast.makeText(getApplication(), "Information Updated Successfully", Toast.LENGTH_LONG).show();
            toastMsg("Information Updated Successfully");
            this.finish();
          //  sendBroadcast(BroadCastUtils.RECREATE_VIEW);
        }
    }

    private void toastMsg(String str){
        Toast toast = Toast.makeText(this, Html.fromHtml("<font size='12' color='#FFFFFF'  ><b>" + str + "</b></font>"), Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM|Gravity.FILL_HORIZONTAL, -20, -20);
        // toast.setGravity(Gravity.CENTER, 50, 50);
        toast.getView().setPadding(10, 25, 10, 35);

//        toast.getView().setBackgroundColor(this.getResources().getColor(R.color.listdivider));
        toast.show();
    }
}

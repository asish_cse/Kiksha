package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.SharedPreferencesHelper;
import com.bs.ecommerce.kiksha.model.CartItemChangeResponse;
import com.bs.ecommerce.kiksha.model.CartItems;
import com.bs.ecommerce.kiksha.model.MessageResponse;
import com.bs.ecommerce.kiksha.model.Product;
import com.bs.ecommerce.kiksha.model.RemoveItemRequest;
import com.bs.ecommerce.kiksha.model.UpdateQuantityRequest;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.ui.activity.BaseActivity;
import com.bs.ecommerce.kiksha.ui.activity.MyCartActivity;
import com.bs.ecommerce.kiksha.ui.activity.ProductDetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;

/**
 * Created by Ashraful on 5/2/2016.
 */
public class ShoppingCartAdapter extends RecyclerView.Adapter<ShoppingCartAdapter.ShoppingCartViewHolder> {
    List<CartItems> itemsList;
    Context context;
    String cartId;

    public ShoppingCartAdapter(Context context, String cartId, List<CartItems> itemsList) {
        this.itemsList = itemsList;
        this.context = context;
        this.cartId = cartId;
    }

    @Override
    public ShoppingCartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_cart_product, parent, false);
        return new ShoppingCartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShoppingCartViewHolder holder, int position) {
        CartItems cartItems = itemsList.get(position);

        Picasso.with(context).load(cartItems.getThumbnail()).
                fit().centerInside().into(holder.ivProductImage);

        holder.productNameTextView.setText(cartItems.getName());
        if (cartItems.getPrice() != null) {
            holder.productPriceTextView.setText("\u09f3 " + cartItems.getPrice());
        } else {
            holder.productPriceTextView.setText("");
        }
        holder.qtyTextView.setText("Qty:" + cartItems.getQty());
//        MyCartActivity.discount_amount = MyCartActivity.discount_amount + Float.valueOf(cartItems.getDiscount_amount());
//        MyCartActivity.cartGrandDiscount.setText("\u09f3 " + MyCartActivity.discount_amount);
    }

    public void onProductNameClicked(int position) {
        if (context instanceof MyCartActivity) {
            String name = itemsList.get(position).getName();
            String productId = itemsList.get(position).getProduct_id();
            int qty = itemsList.get(position).getQty();
            ((MyCartActivity) context).gotoProductDetailsActivity(name, productId, qty);
        }
    }

    public void onQuantityUpClicked(int position) {
        boolean isUp = true;
        Log.d("==>>", "test");
        Log.d("==>>" + SharedPreferencesHelper.getMenu(context), "");
        Call<CartItemChangeResponse> callback = RestClient.get().updateQuantity(getUpdateQuantityReqObject(position, isUp));
        NetworkServiceHandler.processCallBack(callback, context);

    }


    private void onQuantityDownClicked(int position) {
        //       Log.d("==>>"+itemsList.get(position).getProduct_id(),"))))"+itemsList.get(position).getPrice().length());
//        if(itemsList.get(position).getParent_item_id()==null && (itemsList.get(position).getQty() <= 2)&& (itemsList.get(position).getPrice().length() < 4)){
//
//           // Toast.makeText(context, "Lowest quantity for any item is 2", Toast.LENGTH_SHORT).show();
//
//
//            toastMsg("Lowest quantity for any item is 2");
//
//        }
//        else
        if (itemsList.get(position).getQty() > 1) {
            boolean isUp = false;
            Call<CartItemChangeResponse> callback = RestClient.get().updateQuantity(getUpdateQuantityReqObject(position, isUp));
            NetworkServiceHandler.processCallBack(callback, context);
        } else {
            // Toast.makeText(context, "Lowest quantity for any item is 1", Toast.LENGTH_SHORT).show();
            toastMsg("Lowest quantity for any item is 1");
        }
    }

    private void toastMsg(String str) {
        Toast toast = Toast.makeText(context, Html.fromHtml("<font color='#FFFFFF' ><b>" + str + "</b></font>"), Toast.LENGTH_LONG);
        // toast.setGravity(Gravity.TOP, 0, 0);
        // toast.setGravity(Gravity.CENTER, 50, 50);
        toast.getView().setPadding(10, 10, 10, 10);
        toast.getView().setBackgroundColor(context.getResources().getColor(R.color.msg_bg3));
        toast.show();
    }

    private UpdateQuantityRequest getUpdateQuantityReqObject(int position, boolean isUp) {
        CartItems cartItems = itemsList.get(position);

        UpdateQuantityRequest updateQuantityRequest = new UpdateQuantityRequest();
        updateQuantityRequest.setCartId(Integer.valueOf(cartId));
        if (isUp) {
            updateQuantityRequest.setQty(cartItems.getQty() + 1);
        } else {
            updateQuantityRequest.setQty(cartItems.getQty() - 1);
        }

        if (cartItems.getParent_item_id() != null) {
            updateQuantityRequest.setItemId(Integer.valueOf(cartItems.getParent_item_id()));
        } else {
            updateQuantityRequest.setItemId(Integer.valueOf(cartItems.getItem_id()));
        }
        return updateQuantityRequest;
    }


    private void onRemoveItemClicked(int position) {
        Call<CartItemChangeResponse> callback = RestClient.get().removeItemFromCart(getRemoveItemReqObject(position));
        NetworkServiceHandler.processCallBack(callback, context);
    }

    private RemoveItemRequest getRemoveItemReqObject(int position) {
        RemoveItemRequest removeItemRequest = new RemoveItemRequest();
        removeItemRequest.setCartId(Integer.valueOf(cartId));
        CartItems cartItems = itemsList.get(position);

        if (cartItems.getParent_item_id() != null) {
            removeItemRequest.setItemId(Integer.valueOf(cartItems.getParent_item_id()));
        } else {
            removeItemRequest.setItemId(Integer.valueOf(cartItems.getItem_id()));
        }

        return removeItemRequest;
    }


    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    protected class ShoppingCartViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView ivProductImage;
        ImageView quantityDownImageView;
        ImageView quantityupImageView;
        ImageView removeItemImageView;
        TextView productNameTextView;
        TextView productPriceTextView;
        TextView qtyTextView;


        public ShoppingCartViewHolder(View itemView) {
            super(itemView);
            ivProductImage = (ImageView) itemView.findViewById(R.id.iv_product_image);
            quantityDownImageView = (ImageView) itemView.findViewById(R.id.iv_quantity_down);
            quantityupImageView = (ImageView) itemView.findViewById(R.id.iv_quantity_up);
            removeItemImageView = (ImageView) itemView.findViewById(R.id.iv_remove_product);
            productNameTextView = (TextView) itemView.findViewById(R.id.tv_product_name);
            productPriceTextView = (TextView) itemView.findViewById(R.id.tv_product_price);
            qtyTextView = (TextView) itemView.findViewById(R.id.tv_qty);

            quantityDownImageView.setOnClickListener(this);
            quantityupImageView.setOnClickListener(this);
            removeItemImageView.setOnClickListener(this);
            productNameTextView.setOnClickListener(this);
            ivProductImage.setOnClickListener(this);
            productPriceTextView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int resourceId = v.getId();
            if (resourceId == R.id.iv_quantity_up) {
                onQuantityUpClicked(getAdapterPosition());
            } else if (resourceId == R.id.iv_quantity_down) {
                onQuantityDownClicked(getAdapterPosition());
            } else if (resourceId == R.id.iv_remove_product) {
                onRemoveItemClicked(getAdapterPosition());
            } else if (resourceId == R.id.tv_product_name) {
                onProductNameClicked(getAdapterPosition());
            } else if (resourceId == R.id.iv_product_image) {
                onProductNameClicked(getAdapterPosition());
            } else if (resourceId == R.id.tv_product_price) {
                onProductNameClicked(getAdapterPosition());
            }
        }
    }


}

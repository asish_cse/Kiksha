package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/12/2016.
 */
public class OrderHistory extends OrderInfo {
    private String cart_id;
    private String ship_to;

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public String getShip_to() {
        return ship_to;
    }

    public void setShip_to(String ship_to) {
        this.ship_to = ship_to;
    }
}

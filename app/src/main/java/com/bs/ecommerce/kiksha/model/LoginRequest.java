package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 4/21/2016.
 */
public class LoginRequest {
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String email;
    private String password;
}

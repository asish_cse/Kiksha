package com.bs.ecommerce.kiksha.model;

/**
 * Created by Asish on 12/11/2017.
 */
public class FeatureProduct {

  //  @SerializedName("id")

    private String category_id;
    private String parent_id;
    private String name;
    private String is_active;
    private String get_include_menu;
    private String level;
    private String title;
    private String image_file;
    private String banner_type;

    private String id;

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }



    public String getBanner_type() {
        return banner_type;
    }

    public void setBanner_type(String banner_type) {
        this.banner_type = banner_type;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getGet_include_menu() {
        return get_include_menu;
    }

    public void setGet_include_menu(String get_include_menu) {
        this.get_include_menu = get_include_menu;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage_file() {
        return image_file;
    }

    public void setImage_file(String image_file) {
        this.image_file = image_file;
    }
}

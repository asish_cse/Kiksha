package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.BestSeller;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.model.HomeSlider;
import com.bs.ecommerce.kiksha.model.Product;
import com.bs.ecommerce.kiksha.ui.activity.ProductDetailsActivity;
import com.bs.ecommerce.kiksha.ui.activity.ProductListActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ViewPagerAdapterBestSeller extends PagerAdapter {
	// Declare Variables
	Context context;
//	String[] rank;
//	String[] country;
//	String[] population;
	int[] flag;
	LayoutInflater inflater;
	ArrayList<String> slider;
	ArrayList<BestSeller> homesliderlist2;


	public ViewPagerAdapterBestSeller(Context context,  ArrayList<BestSeller> homesliderlist2) {
		this.context = context;

		this.homesliderlist2 = homesliderlist2;
	}

	@Override
	public int getCount() {
//		return rank.length;

		return homesliderlist2.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, final int position) {

		// Declare Variables
		TextView title;
		TextView price;
		TextView finalprice;
		ImageView imgflag;
		ImageView imgflag2;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View itemView = inflater.inflate(R.layout.viewpager_item_bestsell, container,
				false);

		final String names[] = {"1092","403","414","1074","515","474"};

		itemView.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v){
				//this will log the page number that was click
				//Log.i("TAG", "This page was clicked: " + pos);
//				Log.d("<<<<", ">>>>"+position);
//				Log.d("<<<<", ">>>>"+slider.get(position));

//				Category category = new Category();
//				category.setCategory_id(homesliderlist2.get(position).getId()); //homesliderlist2.get(position).getCategory_id()
//				category.setName(homesliderlist2.get(position).getName());
//				ProductListActivity.category = category;
//				//gotoNewActivity(ProductListActivity.class);
//				Intent intent = new Intent(context, ProductListActivity.class);
//				context.startActivity(intent);

				Product product = new Product();
				product.setName(homesliderlist2.get(position).getName());
				product.setProductId(homesliderlist2.get(position).getId());
				ProductDetailsActivity.product = product;
				ProductDetailsActivity.qty = 1;
				Intent intent = new Intent(context, ProductDetailsActivity.class);
				context.startActivity(intent);

			}
		});

		Log.d("///cc/", "///"+position);
		Log.d("///cc/", "///"+homesliderlist2.get(position).getName());
			// Locate the TextViews in viewpager_item.xml
			title = (TextView) itemView.findViewById(R.id.title_id);
			price = (TextView) itemView.findViewById(R.id.price_id);
			finalprice = (TextView) itemView.findViewById(R.id.final_price_id);
//
//		// Capture position and set to the TextViews
			title.setText("" + homesliderlist2.get(position).getName());
			price.setText("BDT " + homesliderlist2.get(position).getPrice());
			price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			if (homesliderlist2.get(position).getSpecial_price().trim().equalsIgnoreCase("0")) {
				price.setText(" ");
				finalprice.setText("BDT " + homesliderlist2.get(position).getPrice());
			} else
				finalprice.setText("BDT " + homesliderlist2.get(position).getSpecial_price());
//		txtpopulation.setText(population[position]);
			Log.d("==best=" + homesliderlist2.get(position).getPrice(), ">>>>" + homesliderlist2.get(position).getName());

			// Locate the ImageView in viewpager_item.xml
			imgflag = (ImageView) itemView.findViewById(R.id.flag);
			// Capture position and set to the ImageView
			//	imgflag.setImageResource(slider.get(position));
			Picasso.with(context).load(homesliderlist2.get(position).getImage()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(imgflag);

		Log.d("==best=", ">>>>"+homesliderlist2.get(position).getImage());

//		imgflag2 = (ImageView) itemView.findViewById(R.id.flag2);
//		// Capture position and set to the ImageView
//		//	imgflag.setImageResource(slider.get(position));
//		if(position+1<homesliderlist2.size()-1)
//		 Picasso.with(context).load(homesliderlist2.get(position+1).getImage()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(imgflag2);


		// Add viewpager_item.xml to ViewPager
		((ViewPager) container).addView(itemView);

		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// Remove viewpager_item.xml from ViewPager
		((ViewPager) container).removeView((RelativeLayout) object);

	}
}

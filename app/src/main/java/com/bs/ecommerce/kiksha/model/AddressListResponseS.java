package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by Ashraful on 5/4/2016.
 */
public class AddressListResponseS extends BaseResponse {

    public List<Address> getData() {
        return data;
    }

    public void setData(List<Address> data) {
        this.data = data;
    }

    private List<Address> data;
}

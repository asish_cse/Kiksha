package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 13-Oct-16.
 */

public class AddressRemoveRequest {
    private String addressId;

    public AddressRemoveRequest(String addressId) {
        this.addressId = addressId;
    }

    public String getAddressId() {
        return addressId;
    }
}

package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.CartItems;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ashraful on 5/11/2016.
 */
public class CheckoutAdapter  extends RecyclerView.Adapter {
    List<CartItems> itemsList;
    Context context;

    public CheckoutAdapter(Context context, List<CartItems> itemsList) {
        this.itemsList = itemsList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_checkout, parent, false);
        return new CheckoutCartViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder  holder, int position) {

        CheckoutCartViewHolder ViewHolder=(CheckoutCartViewHolder) holder;
        CartItems cartItems = itemsList.get(position);

        Picasso.with(context).load(cartItems.getThumbnail()).
                fit().centerInside().into(ViewHolder.ivProductImage);

        ViewHolder.productNameTextView.setText(cartItems.getName());
        if (cartItems.getPrice() != null) {
            ViewHolder.productPriceTextView.setText
                    (context.getResources().getString(R.string.MoneySymbol) + cartItems.getPrice());
        } else {
            ViewHolder.productPriceTextView.setText("");
        }
        ViewHolder.quantityTextView.setText("Qty:" + cartItems.getQty());

    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    protected static class CheckoutCartViewHolder extends RecyclerView.ViewHolder {
        ImageView ivProductImage;
        TextView productNameTextView;
        TextView productPriceTextView;
        TextView quantityTextView;


        public CheckoutCartViewHolder(View itemView) {
            super(itemView);
            ivProductImage = (ImageView) itemView.findViewById(R.id.iv_product_image);
            productNameTextView = (TextView) itemView.findViewById(R.id.tv_product_name);
            productPriceTextView = (TextView) itemView.findViewById(R.id.tv_product_price);
            quantityTextView = (TextView) itemView.findViewById(R.id.tv_product_quantity);
        }
    }
}
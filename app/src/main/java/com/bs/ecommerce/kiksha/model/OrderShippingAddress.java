package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/12/2016.
 */
public class OrderShippingAddress extends Address {
    private String shipping_email;

    public String getShipping_email() {
        return shipping_email;
    }

    public void setShipping_email(String shipping_email) {
        this.shipping_email = shipping_email;
    }
}

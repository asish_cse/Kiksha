package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 11/6/2015.
 */
public class BaseResponse  {

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    private int response_code;

    public String[] getErrors() {
        return errors;
    }

    public void setErrors(String[] errors) {
        this.errors = errors;
    }

    private String[]errors;
}

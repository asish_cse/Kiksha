package com.bs.ecommerce.kiksha.network;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ashraful on 11/5/2015.
 */

public abstract class RestCallBack<T> implements Callback<T> {
    @Override
    public void onFailure(Call<T> call, Throwable t) {

    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {

    }
}

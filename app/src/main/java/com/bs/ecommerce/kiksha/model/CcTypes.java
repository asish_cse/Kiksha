package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/13/2016.
 */
public class CcTypes {
    private String AE;
    private String VI;
    private String MC;
    private String DI;

    public String getAE() {
        return AE;
    }

    public void setAE(String AE) {
        this.AE = AE;
    }

    public String getVI() {
        return VI;
    }

    public void setVI(String VI) {
        this.VI = VI;
    }

    public String getMC() {
        return MC;
    }

    public void setMC(String MC) {
        this.MC = MC;
    }

    public String getDI() {
        return DI;
    }

    public void setDI(String DI) {
        this.DI = DI;
    }
}

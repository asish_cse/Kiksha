package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/16/2016.
 */
public class OrderReviewItemsDynamicAttribute {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

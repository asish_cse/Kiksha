package com.bs.ecommerce.kiksha.ui.activity;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.Address;
import com.bs.ecommerce.kiksha.model.AddressListResponse;
import com.bs.ecommerce.kiksha.model.AddressListResponseS;
import com.bs.ecommerce.kiksha.model.AllShippingMethodResponse;
import com.bs.ecommerce.kiksha.model.CartId;
import com.bs.ecommerce.kiksha.model.City;
import com.bs.ecommerce.kiksha.model.CityResponse;
import com.bs.ecommerce.kiksha.model.Country;
import com.bs.ecommerce.kiksha.model.CountryResponse;
import com.bs.ecommerce.kiksha.model.MessageResponse;
import com.bs.ecommerce.kiksha.model.SetAddressRequest;
import com.bs.ecommerce.kiksha.model.ShippingMethod;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.view.FormViews;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_shipping_address)
public class ShippingAddressActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    @InjectView(R.id.sp_addresses)
    Spinner addressesSpinner;

    @InjectView(R.id.et_first_name)
    EditText firstNameEt;

    @InjectView(R.id.et_last_name)
    EditText lastNameEt;

    @InjectView(R.id.et_email)
    EditText emailEt;

    @InjectView(R.id.et_address_1)
    EditText address1Et;

    /*@InjectView(R.id.et_address_2)
    EditText address2Et;*/

    @InjectView(R.id.sp_country)
    Spinner countrySp;

    @InjectView(R.id.et_city)
    Spinner cityEt;

    @InjectView(R.id.et_area)
    Spinner spArea;

    @InjectView(R.id.et_postal_code)
    EditText postalCodeEt;

    @InjectView(R.id.et_telephone)
    EditText telephoneEt;

    @InjectView(R.id.btn_continue)
    Button continueBtn;


    private ArrayAdapter countryAdapter;
    private ArrayAdapter cityAdapter;
    private ArrayAdapter areaAdapter;
    private List<Address> addressLists;
    private int defaultAddressPosition = 0;
    public static Address billingAddress = new Address();
    private List<Country> countryList;
    private List<City> cityList;
    private List<String> areaList;
    private Address address_shipping;
    private List<ShippingMethod> dataShippingMethod;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Shipping Address");
        callCountryListApi();
        callAddressListApi();
        callCityListApi();
        getShippingMethodApi();
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        continueBtn.setOnClickListener(this);
    }

    private void callCountryListApi() {
        NetworkServiceHandler.processCallBack(RestClient.get().getCountryListForDropdown(), this);
    }

    private void callAddressListApi() {
        if (isLoggedIn()) {
            addressesSpinner.setVisibility(View.VISIBLE);
            String customerId = preferenceService.GetPreferenceValue(PreferenceService.ENTITY_KEY);
            Call<AddressListResponse> callback = RestClient.get().getAddressList(customerId, "");
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            addressesSpinner.setVisibility(View.GONE);
        }

    }

    private void callCityListApi() {
        NetworkServiceHandler.processCallBack(RestClient.get().getCityForDropDown(""), this);
    }
    private void getShippingMethodApi() {
        int cartId = getCartId();
        if (cartId != -1) {
            Call<AllShippingMethodResponse> callback = RestClient.get().getAllShippingMethod(getCartIdObject(cartId)); // changed
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }
    }
    public void onEvent(AllShippingMethodResponse allShippingMethodResponse) {
        if (allShippingMethodResponse.getResponse_code() == 100) {
            dataShippingMethod = allShippingMethodResponse.getData();
        }
    }
    public void onEvent(CountryResponse countryResponse) {
        countryList = new ArrayList<>();

        if (countryResponse.getResponse_code() == 100) {
            countryList = countryResponse.getData();

            countryAdapter = new ArrayAdapter
                    (this, android.R.layout.simple_spinner_item, countryResponse.getStringList());

            countryAdapter.setDropDownViewResource
                    (android.R.layout.simple_spinner_dropdown_item);

            countrySp.setAdapter(countryAdapter);
        }

    }
    private CartId getCartIdObject(int cartId) {
        CartId cartIdRequest = new CartId();
        cartIdRequest.setCartId(cartId);
        return cartIdRequest;
    }
    public void onEvent(CityResponse countryResponse) {
        cityList = new ArrayList<>();
        Log.d("respose_f", "==" + countryResponse.toString());
        if (countryResponse.getResponse_code() == 100) {
            cityList = countryResponse.getData();

            cityAdapter = new ArrayAdapter
                    (this, android.R.layout.simple_spinner_item, countryResponse.getStringList());

            cityAdapter.setDropDownViewResource
                    (android.R.layout.simple_spinner_dropdown_item);

            cityEt.setAdapter(cityAdapter);//address_billing

        }

    }

    public void onEvent(AddressListResponse addressListResponse) {
        addressLists = new ArrayList<>();
        boolean hasAddress = false;

        if (addressListResponse.getResponse_code() == 100) {
            addressLists = addressListResponse.getData();
            if (addressListResponse.getData().size() > 0) {
                hasAddress = true;
            }
        }
        populateAddressSpinner(hasAddress);
    }

    private void populateAddressSpinner(boolean hasAddress) {
        List<String> addressSpinnerList = new ArrayList<>();
        if (hasAddress) {
            for (int i = 0; i < addressLists.size(); i++) {
                if (addressLists.get(i).isDefaultShippingAddress()) {
                    defaultAddressPosition = i;
                }
                String spinnerAddressLabel = addressLists.get(i).getFirstname()
                        + " "
                        + addressLists.get(i).getLastname()
                        + ", "
                        + addressLists.get(i).getTelephone()
                        + ", "
                        + addressLists.get(i).getCity();
                addressSpinnerList.add(spinnerAddressLabel);
            }
        }

        addressSpinnerList.add("Add New Address");


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, addressSpinnerList);
        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        addressesSpinner.setAdapter(dataAdapter);

        if (hasAddress) {
            addressesSpinner.setSelection(defaultAddressPosition);
            populateAddressField(defaultAddressPosition);
            enabledView(false);
        } else {
            addressesSpinner.setSelection(0);
            enabledView(true);
            emptyAddressField();
        }

        addressesSpinner.setOnItemSelectedListener(this);
        cityEt.setOnItemSelectedListener(this);
    }

    public void enabledView(boolean isEditable) {
        firstNameEt.setEnabled(isEditable);
        firstNameEt.setFocusableInTouchMode(isEditable);

        lastNameEt.setEnabled(isEditable);
        lastNameEt.setFocusableInTouchMode(isEditable);

        emailEt.setEnabled(isEditable);
        emailEt.setFocusableInTouchMode(isEditable);

        address1Et.setEnabled(isEditable);
        address1Et.setFocusableInTouchMode(isEditable);

        /*address2Et.setEnabled(isEditable);
        address2Et.setFocusableInTouchMode(isEditable);*/

        countrySp.setEnabled(isEditable);
        countrySp.setFocusableInTouchMode(isEditable);

        cityEt.setEnabled(isEditable);
        cityEt.setFocusableInTouchMode(isEditable);

        postalCodeEt.setEnabled(isEditable);
        postalCodeEt.setFocusableInTouchMode(isEditable);

        telephoneEt.setEnabled(isEditable);
        telephoneEt.setFocusableInTouchMode(isEditable);
    }

    public void emptyAddressField() {
        firstNameEt.setText(preferenceService.GetPreferenceValue(PreferenceService.FIRST_NAME_KEY));
        lastNameEt.setText(preferenceService.GetPreferenceValue(PreferenceService.LAST_NAME_KEY));
        emailEt.setText(preferenceService.GetPreferenceValue(PreferenceService.EMAIL_KEY));
        address1Et.setText("");
        //address2Et.setText("");
        countrySp.setSelection(0);
//        cityEt.setText("");//Update
        postalCodeEt.setText("");
        telephoneEt.setText("");
    }

    private void populateAddressField(int position) {
        Address address = addressLists.get(position);
        address_shipping = address;
        firstNameEt.setText(address.getFirstname());
        lastNameEt.setText(address.getLastname());
        emailEt.setText(preferenceService.GetPreferenceValue(PreferenceService.EMAIL_KEY));
        /*if (address.getStreet().length > 1) {
            address1Et.setText(address.getStreet()[1]);
        }*/

        address1Et.setText(address.getStreet()[0]);

        try {
            countrySp.setSelection(countryAdapter.getPosition(address.getCountry()));
            cityEt.setSelection(cityAdapter.getPosition(address.getCity()));
        } catch (NullPointerException ex) {

        }
//        cityEt.setText(address.getCity());//Update
        postalCodeEt.setText(address.getPostcode());
        telephoneEt.setText(address.getTelephone());
    }

    private void validateForm() {
        boolean isValid = true;

        if (!FormViews.isValidWithMark(firstNameEt, "First Name")) {
            isValid = false;
        }
        if (!FormViews.isValidWithMark(lastNameEt, "Last Name")) {
            isValid = false;
        }
        if (!FormViews.isValidEmail(emailEt, "Email")) {
            isValid = false;
        }

        if (!FormViews.isValidWithMark(address1Et, "Address")) {
            isValid = false;
        }
        /*if (!FormViews.isValidWithMark(address2Et, "Address")) {
            isValid = false;
        }*/
        if (!FormViews.isValidWithMark(countrySp, "Select Country")) {
            isValid = false;
        }
        if (!FormViews.isValidWithMark(cityEt, "City")) {
            isValid = false;
        }
        if (!FormViews.isValidWithMark(postalCodeEt, "Postal Code")) {
            isValid = false;
        }
        if (!FormViews.isValidWithMarkMobile(telephoneEt, "Phone Number")) {
            isValid = false;
        }
        if (isValid) {
            callSetAddressApi(true);
            // gotoNewActivity(ShippingMethodActivity.class);
        }


    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int resId = v.getId();
        if (resId == R.id.btn_continue) {
            if (addressesSpinner.getSelectedItemPosition() < addressLists.size()) {
                callSetAddressApi(false);
            } else {
                validateForm();
            }
        }
    }

    private void callSetAddressApi(boolean isNewAddress) {
        int cartId = getCartId();
        if (cartId != -1) {
            Call<MessageResponse> callback = RestClient.get().setCheckoutAddress(getAddressReqObject(cartId, isNewAddress));
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }
    }


    public void onEvent(MessageResponse messageResponse) {
        if (messageResponse.getResponse_code() == 100) {
            showInfoSnackBar(messageResponse.getData().getMsg());
            gotoNewActivity(PaymentMethodActivity.class);
        }
    }

    private SetAddressRequest getAddressReqObject(int cartId, boolean isNewAddress) {
        SetAddressRequest setAddressRequest = new SetAddressRequest();
        setAddressRequest.setCartId(cartId);


        if (isNewAddress) {
            Address address = new Address();
            address.setFirstname(firstNameEt.getText().toString());
            address.setLastname(lastNameEt.getText().toString());
            // email nai
            String[] street = {address1Et.getText().toString()};
            address.setStreet(street);
            address.setCountry_code(countryList.get(countrySp.getSelectedItemPosition()).getValue());
//            address.setCity(cityEt.getText().toString());//Update
            address.setCity(cityList.get(cityEt.getSelectedItemPosition()).getCity());
            address.setRegion(areaList.get(spArea.getSelectedItemPosition()));
            address.setPostcode(postalCodeEt.getText().toString());
            address.setTelephone(telephoneEt.getText().toString());

            setAddressRequest.setBillingAddress(billingAddress);
            setAddressRequest.setShippingAddress(address);





        } else {
            setAddressRequest.setBillingAddress(billingAddress);
            setAddressRequest.setShippingAddress(addressLists.get(addressesSpinner.getSelectedItemPosition()));
        }
//------------------------------------------------
        String inSide = preferenceService.GetPreferenceValue(PreferenceService.SHIPPIN_IN_KEY);
        String outSide = preferenceService.GetPreferenceValue(PreferenceService.SHIPPING_OUT_KEY);
        Log.d("shipping_chargeAA", inSide + "===" + outSide);
        for (ShippingMethod shp : dataShippingMethod) {
            Log.d("shipping_chargeAA", "==="+shp.getPrice());

            if (shp.getCarrier_title().equals("Dhaka"))
                setAddressRequest.setShipping_code(shp.getCode());
            if (Float.valueOf(inSide) > 40) {//if (Float.valueOf(inSide) > Float.valueOf(shp.getPrice()))
                setAddressRequest.setShipping_price(inSide);
            } else {
                setAddressRequest.setShipping_price("40.00");
            }
        }
        if (setAddressRequest.getShippingAddress().getCity().equals("Dhaka")) {
            for (ShippingMethod shp : dataShippingMethod) {
                if (shp.getCarrier_title().equals("Dhaka City"))
                    setAddressRequest.setShipping_code(shp.getCode());
                if (Float.valueOf(inSide) > 40) {
                    setAddressRequest.setShipping_price(inSide);
                } else {
                    setAddressRequest.setShipping_price("40.00");
                }
            }

        } else {
            for (ShippingMethod shp : dataShippingMethod) {
                if (shp.getCarrier_title().equals("Outside Dhaka City"))
                    setAddressRequest.setShipping_code(shp.getCode());
                if (Float.valueOf(outSide) >60) {// Float.valueOf(shp.getPrice()
                    setAddressRequest.setShipping_price(outSide);
                } else {
                    setAddressRequest.setShipping_price("60.00");
                }
            }
        }
        //---------------------------------------------
        Log.d("Billing_add", "address==" + setAddressRequest.getShipping_code());
        Log.d("Billing_add_amount", "address==" + setAddressRequest.getShipping_price());
        preferenceService.SetPreferenceValue(PreferenceService.SHIPPING_CHARGE_KEY, setAddressRequest.getShipping_price());


        return setAddressRequest;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int resId = parent.getId();

        if (resId == R.id.sp_addresses) {
            if (position < addressLists.size()) {
                populateAddressField(position);
                enabledView(false);
            } else {
                enabledView(true);
                emptyAddressField();
            }
        } else if (resId == R.id.et_city) {

//            cityEt.setSelection(cityAdapter.getPosition(address_billing.getCity()));

            areaList = new ArrayList<>();
            List<String> area = Arrays.asList(cityList.get(position).getArea_list().split(","));
            for (String ar : area) {

                String aread = ar.substring(0, 1).toUpperCase() + ar.substring(1);

                areaList.add(aread);
            }

            areaAdapter = new ArrayAdapter
                    (this, android.R.layout.simple_spinner_item, areaList);

            areaAdapter.setDropDownViewResource
                    (android.R.layout.simple_spinner_dropdown_item);

            spArea.setAdapter(areaAdapter);
//            Toast.makeText(BillingAddressActivity.this, address_billing.getArea() + "===" + city_cl + "====" + address_billing.getCity(),
//                    Toast.LENGTH_SHORT).show();

            spArea.setSelection(areaAdapter.getPosition(address_shipping.getArea()));

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

package com.bs.ecommerce.kiksha;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by BS62 on 5/19/2016.
 */
public class SearchSuggestionsProvider extends SearchRecentSuggestionsProvider {
    public static final String AUTHORITY = SearchSuggestionsProvider.class.getName();
    public static final int MODE = DATABASE_MODE_QUERIES;

    public SearchSuggestionsProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}

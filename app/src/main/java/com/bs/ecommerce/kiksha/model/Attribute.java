package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/9/2016.
 */
public class Attribute {
    private int attribute_id;
    private int attribute_value;

    public int getAttribute_id() {
        return attribute_id;
    }

    public void setAttribute_id(int attribute_id) {
        this.attribute_id = attribute_id;
    }

    public int getAttribute_value() {
        return attribute_value;
    }

    public void setAttribute_value(int attribute_value) {
        this.attribute_value = attribute_value;
    }
}

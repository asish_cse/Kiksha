package com.bs.ecommerce.kiksha.network;

import android.util.Log;

import com.bs.ecommerce.kiksha.constant.Url;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Ashraful on 11/5/2015.
 */
public class RestClient {

    public static ApiService apiService;

    public static ApiService get() {
        //  if (apiService == null)
        initializeApiservice();
        return apiService;

    }

    public static ApiService get(String baseUrl) {
        //  if (apiService == null)
        initializeApiservice(baseUrl);
        return apiService;

    }

    public static void initializeApiservice(String baseUrl) {
        /*Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();*/
        if (baseUrl.isEmpty())
            baseUrl = Url.BASE_URL;
        Gson gson = new GsonBuilder().serializeNulls().create();
        OkHttpClient okHttpClient = getOkhttp();
        //  okHttpClient.interceptors().add(getInterceptor());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
        Log.d("rect_request", "==11" + retrofit.toString());
        Log.d("rect_request", "==22" + gson.toString());
        apiService = retrofit.create(ApiService.class);
    }

    public static OkHttpClient getOkhttp() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        Builder okhttpBuilder = new Builder();
        okhttpBuilder.readTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(60, TimeUnit.SECONDS);
        okhttpBuilder.addInterceptor(interceptor);
        okhttpBuilder.addInterceptor(getInterceptor());
        OkHttpClient client = okhttpBuilder.build();

        return client;
    }

    public static void initializeApiservice() {
        initializeApiservice("");
    }

    private static String bodyToString(final RequestBody request){
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if(request==null) return "did not work";
            else copy.writeTo(buffer);
            return buffer.readUtf8();
        }
        catch (final IOException e) {
            return "did not work";
        }
    }

    private static Interceptor getInterceptor() {
        return new Interceptor() {


            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder = original.newBuilder();
                //Log.d("headers",new Gson().toJson(builder));
                Request request = builder/*.header("DeviceId", NetworkUtilities.getHeader())*/
                        .addHeader("Content-Type", "application/json")
                        .addHeader("auth_token", NetworkServiceHandler.AuthToken)
                        .method(original.method(), original.body())
                        .build();
                Log.d("rect_request", "==33" + bodyToString(original.body()));
//                Log.d("rect_request", "==44" + new Gson().toJson(original.body()));
                return chain.proceed(request);
            }


        };
    }
}

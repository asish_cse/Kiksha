package com.bs.ecommerce.kiksha.ui.activity;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/28/2016.
 */

@ContentView(R.layout.activity_more_menu)
public class MoreMenuActivity extends BaseActivity {

    @InjectView(R.id.tv_my_account)
    TextView myAccountTextView;

    @InjectView(R.id.tv_help)
    TextView helpTextView;

    @InjectView(R.id.tv_contact_us)
    TextView contactUsTextView;

    @InjectView(R.id.tv_about)
    TextView aboutTextView;

    @InjectView(R.id.tv_follow_us)
    TextView followUsTextView;

    @InjectView(R.id.tv_notification_setting)
    TextView notificationSettingTextView;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("More");
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();

        myAccountTextView.setOnClickListener(this);
        helpTextView.setOnClickListener(this);
        contactUsTextView.setOnClickListener(this);
        aboutTextView.setOnClickListener(this);
        followUsTextView.setOnClickListener(this);
        notificationSettingTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resourceId = v.getId();

        if (resourceId == myAccountTextView.getId()) {
            gotoNewActivity(MyAccountActivity.class);
        } else if (resourceId == helpTextView.getId()) {
            Intent intent = new Intent(this, WebActivity.class);
            intent.putExtra(WebActivity.INTENT_EXTRA_WEB_TYPE, WebActivity.TYPE_FAQ);
            startActivity(intent);
        } else if (resourceId == R.id.tv_contact_us) {
            gotoNewActivity(ContactUsActivity.class);
        } else if (resourceId == R.id.tv_about) {
            gotoNewActivity(AboutMenuActivity.class);

        } else if (resourceId == R.id.tv_follow_us) {
            gotoNewActivity(FollowUsActivity.class);

        } else if (resourceId == R.id.tv_notification_setting) {

        }

    }
}

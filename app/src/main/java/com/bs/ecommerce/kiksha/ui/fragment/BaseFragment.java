package com.bs.ecommerce.kiksha.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.SharedPreferencesHelper;
import com.bs.ecommerce.kiksha.ui.activity.ProductListActivityOfferzone;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.NoSubscriberEvent;
import roboguice.fragment.RoboFragment;

/**
 * Created by Ashraful on 4/4/2016.
 */
public class BaseFragment extends RoboFragment {
   /* @javax.annotation.Nullable
    @InjectView(R.id.app_toolbar)
   */

    Toast mToast;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setHasOptionsMenu(true);


    }


    @Override
    public void onResume() {
        checkEventBusRegistration();
        super.onResume();


    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    protected LayoutInflater getLayoutInflater()
    {
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater;
    }
    public void checkEventBusRegistration()
    {
        if(!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this);
        }
    }

    public void onEvent( NoSubscriberEvent noSubscriberEvent)
    {

    }
    public void onEvent( ClassCastException noSubscriberEvent)
    {

    }



    protected void gotoNewFragment(android.support.v4.app.Fragment fragment)
    {

    }

    protected void gotoNewActivity(Class<?> activityClass) {

        String sr = SharedPreferencesHelper.getMenu(getContext());
       // Log.d("=====menu activity===","----"+sr);
        if(sr.equalsIgnoreCase("9")) {
            Log.d("=====menu activity===","----"+sr);

            Intent i = new Intent(getActivity(), ProductListActivityOfferzone.class);
            startActivity(i);
            getActivity().overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
        }

        else
            if(!getActivity().getClass().equals(activityClass)) {

            Intent intent = new Intent(getActivity(), activityClass);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);

        }

    }

    public void showSnack(String message){
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }


    public void showToast(String msg){
        if(mToast != null){
            mToast.cancel();
        }
        mToast = Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG);
        mToast.show();
    }
}

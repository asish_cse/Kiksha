package com.bs.ecommerce.kiksha.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;

import com.bs.ecommerce.kiksha.R;

import java.util.ArrayList;

/**
 * Created by Ashraful on 4/20/2016.
 */
public class MaterialUtils {
    public static void fixToolbarOverflowButton(final Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            // the problem occurs only on LOLLIPOP when using native Toolbar and Action Bar
            try {
                final int abId = activity.getResources().getIdentifier("android:id/action_bar", null, null);
                final Toolbar toolbar = (Toolbar) activity.getWindow().getDecorView().findViewById(abId);
                final int moreId = activity.getResources().getIdentifier("android:string/action_menu_overflow_description", "string", null);

// copy the drawable from appcompat-v7 library if you don't use it OR use the framework one if it's available
                final Drawable d = getDrawableWithColorControlNormal(toolbar.getContext(), R.drawable.ic_more);

                toolbar.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        final String moreString = activity.getString(moreId);
                        final ArrayList<View> out = new ArrayList<>();
                        toolbar.findViewsWithText(out, moreString, View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
                        if (out.isEmpty()) return;
                        final ImageButton moreButton = (ImageButton) out.get(0);
                        moreButton.setImageDrawable(d);
                        //Util.View.removeOnGlobalLayoutListener(toolbar, this);
                    }
                });
            } catch (Exception ex) {
                // this is a pretty hacky solution, anything can go wrong really
                ex.printStackTrace();
            }
        }
    }

    /**
     * Read colorControlNormal (on API < 21) or android:colorControlNormal (on API 21) from theme and apply it to specified drawable.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static Drawable getDrawableWithColorControlNormal(Context context, @DrawableRes int drawableId) {
       /* TypedArray ta = context.obtainStyledAttributes(new int[]{API_21 ? android.R.attr.colorControlNormal : R.attr.colorControlNormal});
        int c = ta.getColor(0, Color.BLACK);
        ta.recycle();
        Drawable d = context.getApplicationContext().getResources().getDrawable(drawableId);
        return tintDrawable(d, c);*/
        return null;
    }

    /**
     * Paint supplied Drawable with supplied color.
     */
    public static Drawable tintDrawable(Drawable d, int c) {
        PorterDuffColorFilter cf = new PorterDuffColorFilter(c, PorterDuff.Mode.SRC_IN);
        d.mutate().setColorFilter(cf);
        return d;

    }
}

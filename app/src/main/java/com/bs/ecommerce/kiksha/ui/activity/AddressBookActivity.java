package com.bs.ecommerce.kiksha.ui.activity;

import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.event.AddressEditEvent;
import com.bs.ecommerce.kiksha.model.Address;
import com.bs.ecommerce.kiksha.model.AddressListResponse;
import com.bs.ecommerce.kiksha.model.AddressRemoveRequest;
import com.bs.ecommerce.kiksha.model.AddressRemoveResponse;
import com.bs.ecommerce.kiksha.model.City;
import com.bs.ecommerce.kiksha.model.CityResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.adapter.AddressAdapter;
import com.bs.ecommerce.kiksha.ui.custom_view.FullHeightListView;
import com.bs.ecommerce.kiksha.util.BroadCastUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 5/4/2016
 */

@ContentView(R.layout.activity_address_book)
public class AddressBookActivity extends BaseActivity {

    @InjectView(R.id.lv_address)
    FullHeightListView  addressListView;

    @InjectView(R.id.btn_add_new_address)
    Button addressAddBtn;

    @InjectView(R.id.vg_billing_address)
    ViewGroup billingAddressViewGroup;

    @InjectView(R.id.vg_shipping_address)
    ViewGroup shippingAddressViewGroup;

    @InjectView(R.id.vg_default_address)
    ViewGroup defaultAddressViewGroup;
    protected AddressAdapter addressAdapter;

    private List<City> cityList;
    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Address Book");
        callAddressListApi();
        callCityListApi();

    }

    @Override
    protected void registerBroadcastReceiver() {
        super.registerBroadcastReceiver();
        broadCastClass.setRecreateBroadCast(BroadCastUtils.ADDRESS_BOOK_REFRESH);
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        addressAddBtn.setOnClickListener(this);
    }

    private void callAddressListApi()
    {
        String customerId=preferenceService.GetPreferenceValue(PreferenceService.ENTITY_KEY);
        Log.d("customer_id","cus=="+customerId);
        NetworkServiceHandler.processCallBack(RestClient.get().getAddressList(customerId,""),this);
    }
    private void callCityListApi() {
        NetworkServiceHandler.processCallBack(RestClient.get().getCityForDropDown(""), this);
    }
    public void onEvent(CityResponse countryResponse) {
        cityList = new ArrayList<>();

        if (countryResponse.getResponse_code() == 100) {
            cityList = countryResponse.getData();


        }

    }

    public void onEvent(AddressEditEvent addressEditEvent)
    {
        gotoNewActivity(AddressUpdateActivity.class);
    }

    public void onEvent(AddressListResponse addressListResponse)
    {
        List<Address>addressList=new ArrayList<>();
        List<Address>additionalAddressList=new ArrayList<>();

        addressList=addressListResponse.getData();
        if(addressListResponse.getResponse_code()==100) {


            for (Address address : addressList) {
                if (address.isDefaultBillingAddress())
                {
                    setDefaultAddress(address, billingAddressViewGroup, true);
                }

                if (address.isDefaultShippingAddress())
                {
                    setDefaultAddress(address, shippingAddressViewGroup, false);
                }
                if(!address.isDefaultBillingAddress() && !address.isDefaultShippingAddress())
                {
                    additionalAddressList.add(address);
                }
            }

            addressAdapter = new AddressAdapter(additionalAddressList);
            addressListView.setAdapter(addressAdapter);
            addEmptyFooter(addressListView);
        }


    }

    private void addEmptyFooter(ListView listView)
    {
        TextView empty = new TextView(this);
        empty.setHeight(50);
        addressListView.addFooterView(empty);
    }


    private void setDefaultAddress(  final Address address,ViewGroup convertView,boolean isBillingAddress)
    {

        TextView addressChangeTextView=(TextView)convertView.findViewById(R.id.tv_address_chenge);
        addressChangeTextView.setPaintFlags(addressChangeTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        defaultAddressViewGroup.setVisibility(View.VISIBLE);
        if(isBillingAddress)
        {
            billingAddressViewGroup.setVisibility(View.VISIBLE);
        }
        else
        {
            shippingAddressViewGroup.setVisibility(View.VISIBLE);
            TextView headerTextView=(TextView)convertView.findViewById(R.id.tv_header);
            headerTextView.setText("Default Shipping Address");
            addressChangeTextView.setText("Change Shipping Address");

        }

        addressChangeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddressUpdateActivity.address = address;
//                AddressUpdateActivity.cityList = cityList;
                AddressUpdateActivity.area_ch=0;
                gotoNewActivity(AddressUpdateActivity.class);
            }
        });

        TextView nameTextView=(TextView)convertView.findViewById(R.id.tv_name);
        nameTextView.setText("" + address.getFirstname()+" "+address.getLastname());

        TextView companyTextView=(TextView)convertView.findViewById(R.id.tv_company);
        setText("", address.getCompany(),companyTextView);

        TextView streetAddress1TextView=(TextView)convertView.findViewById(R.id.tv_street_address1);
        setText("", address.getStreet()[0], streetAddress1TextView);

        /*TextView streetAddress2TextView=(TextView)convertView.findViewById(R.id.tv_street_address2);
        if (address.getStreet().length > 1) {
            setText(address.getStreet()[1], streetAddress2TextView);
        }*/

        TextView countryTextView=(TextView)convertView.findViewById(R.id.tv_country);
        setText(address.getCountry(),countryTextView);

        TextView cityTextView=(TextView)convertView.findViewById(R.id.tv_city_and_postal_code);
        String str=address.getArea()+","+address.getCity()+","+address.getPostcode();
        setText("", str,cityTextView);

        TextView telephoneTextView=(TextView)convertView.findViewById(R.id.tv_telephone);
        setText("T:", address.getTelephone(),telephoneTextView);

        TextView faxTextView=(TextView)convertView.findViewById(R.id.tv_fax);
        setText("", address.getFax(),faxTextView);
    }

    private void setText(String str,TextView textView )
    {
        if(isValidString(str)) {
            textView.setText(str);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }

    }

    private void setText(String prefixStr,String str,TextView textView )
    {
        if(isValidString(str)) {
            textView.setText(prefixStr + str);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }

    }

    private boolean isValidString(String str ) {
        return str != null && str.length() != 0;
    }

    private String getStringValue(String str)
    {
        if(isValidString(str))
            return str;
        else
            return "";

    }

    public void onEvent(AddressRemoveRequest addressRemoveRequest) {
        if (addressRemoveRequest.getAddressId() != null && addressRemoveRequest.getAddressId().length() != 0) {
            Call<AddressRemoveResponse> callback = RestClient.get().removeAddress(addressRemoveRequest.getAddressId(),"");
            NetworkServiceHandler.processCallBack(callback, this);
        }
    }

    public void onEvent(AddressRemoveResponse addressRemoveResponse) {
        if (addressRemoveResponse.getResponse_code() == 100) {
            showInfoSnackBar("Address deleted.");
            if (addressAdapter != null) {
                addressAdapter.removeAddress();
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resourceId=v.getId();

        if(resourceId==R.id.btn_add_new_address)
        {
         gotoNewActivity(AddNewAdressActivity.class);
        }
    }

    @Override
    protected void unregisterBroadcast() {
        super.unregisterBroadcast();
        broadCastClass.unregisterReceiver();
    }
}

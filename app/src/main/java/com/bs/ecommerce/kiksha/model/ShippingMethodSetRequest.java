package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/13/2016.
 */
public class ShippingMethodSetRequest {
    private int cartId;
    private String method_code;

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public String getMethod_code() {
        return method_code;
    }

    public void setMethod_code(String method_code) {
        this.method_code = method_code;
    }
}

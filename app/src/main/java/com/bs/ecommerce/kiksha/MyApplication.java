package com.bs.ecommerce.kiksha;

import android.app.Application;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import roboguice.RoboGuice;

/**
 * Created by Ashraful on 4/25/2016.
 */
@ReportsCrashes(
        formUri = "https://collector.tracepot.com/957a3407"

)
public class MyApplication extends Application {
    static {
        RoboGuice.setUseAnnotationDatabases(false);
    }



    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
    }
}

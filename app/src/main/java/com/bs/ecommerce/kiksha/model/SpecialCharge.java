package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/4/2016.
 */
public class SpecialCharge {
    private String category_name;
    private String category_id;
    private String inside_dhaka;
    private String outside_dhaka;

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getInside_dhaka() {
        return inside_dhaka;
    }

    public void setInside_dhaka(String inside_dhaka) {
        this.inside_dhaka = inside_dhaka;
    }

    public String getOutside_dhaka() {
        return outside_dhaka;
    }

    public void setOutside_dhaka(String outside_dhaka) {
        this.outside_dhaka = outside_dhaka;
    }
}

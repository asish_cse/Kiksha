package com.bs.ecommerce.kiksha.constant;

import android.support.v4.view.ViewPager;
import android.view.View;

import com.bs.ecommerce.kiksha.R;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import static org.acra.ACRA.log;

/**
 * Created by Asish on 19/10/2017
 */
public class PageTransformer implements ViewPager.PageTransformer {
    public void transformPage(View view, float position) {
        view.setAlpha(1 - Math.abs(position));
        if (position < 0) {
            view.setScrollX((int)((float)(view.getWidth()) * position));
        } else if (position > 0) {
            view.setScrollX(-(int) ((float) (view.getWidth()) * -position));
        } else {
            view.setScrollX(0);
        }
    }
}

package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 4/22/2016.
 */
public class UserAuthenticationResponse extends BaseResponse {
    public UserAuthentication getData() {
        return data;
    }

    public void setData(UserAuthentication data) {
        this.data = data;
    }

    private UserAuthentication data;
}

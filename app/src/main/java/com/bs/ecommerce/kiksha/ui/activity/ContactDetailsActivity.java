package com.bs.ecommerce.kiksha.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.bs.ecommerce.kiksha.R;

import roboguice.inject.ContentView;

@ContentView(R.layout.activity_contact_details)
public class ContactDetailsActivity extends BaseActivity {

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Contact Details");
    }
}

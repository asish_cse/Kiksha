package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 5/13/2016.
 */
public class SearchRequest  {

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    private String query;
    private int  page;
}

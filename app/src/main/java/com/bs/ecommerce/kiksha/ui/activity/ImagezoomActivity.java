package com.bs.ecommerce.kiksha.ui.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bs.ecommerce.kiksha.R;

import uk.co.senab.photoview.PhotoViewAttacher;


public class ImagezoomActivity extends AppCompatActivity {

    ImageView imageView ;
    PhotoViewAttacher photoViewAttacher ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imagezoom);

        imageView = (ImageView)findViewById(R.id.imageView);

        Drawable drawable = getResources().getDrawable(R.drawable.btn_galleryclose);

        imageView.setImageDrawable(drawable);

        photoViewAttacher = new PhotoViewAttacher(imageView);

        photoViewAttacher.update();
    }
}

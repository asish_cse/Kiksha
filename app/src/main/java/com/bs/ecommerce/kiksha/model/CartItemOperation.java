package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/25/2016.
 */
public class CartItemOperation {
    private String msg;
    private CartDetailsResponse cart;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public CartDetailsResponse getCart() {
        return cart;
    }

    public void setCart(CartDetailsResponse cart) {
        this.cart = cart;
    }
}

package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 4/27/2016.
 */
public class EmailChangeRequest {
    private  String currentEmail ;
    private  String  newEmail;

    public String getCurrentEmail() {
        return currentEmail;
    }

    public void setCurrentEmail(String currentEmail) {
        this.currentEmail = currentEmail;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }
}

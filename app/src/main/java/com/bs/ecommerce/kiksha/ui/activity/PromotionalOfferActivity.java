package com.bs.ecommerce.kiksha.ui.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.model.Product;
import com.bs.ecommerce.kiksha.ui.adapter.interface_pack.OnItemClickListener;
import com.bs.ecommerce.kiksha.model.PromotionalOffer;
import com.bs.ecommerce.kiksha.model.PromotionalOfferResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.ui.adapter.PromotionalOfferAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_promotional_offer)
public class PromotionalOfferActivity extends BaseActivity {

    @InjectView(R.id.rclv_promo_offer)
    RecyclerView promoOfferRecyclerView;

    List<PromotionalOffer> promotionalOfferList = new ArrayList<>();
    private PromotionalOfferAdapter promotionalOfferAdapter;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Offers");
        initRecyclerView();
        callPromotionalOfferApi();
    }

    private void initRecyclerView() {
        promoOfferRecyclerView.setLayoutManager(getLinearLayoutManager(LinearLayoutManager.VERTICAL));
    }

    private void callPromotionalOfferApi() {
        Call<PromotionalOfferResponse> callback = RestClient.get().getPromotionalOffer();
        NetworkServiceHandler.processCallBack(callback, this);
    }

    public void onEvent(PromotionalOfferResponse promotionalOfferResponse) {
        if (promotionalOfferResponse.getResponse_code() == 100) {
            promotionalOfferList = new ArrayList<>();
            promotionalOfferList = promotionalOfferResponse.getData();
            setupPromoOfferList();
        }
    }

    private void setupPromoOfferList() {
        promotionalOfferAdapter = new PromotionalOfferAdapter(this, promotionalOfferList);
        promoOfferRecyclerView.setAdapter(promotionalOfferAdapter);

        promotionalOfferAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                PromotionalOffer promotionalOffer = promotionalOfferAdapter.getItem(position);

                // 1 for what?

                if (promotionalOffer.getType().equals("1")) {
                    Category category = new Category();
                    category.setCategory_id(promotionalOffer.getId());
                    category.setName(promotionalOffer.getTitle());
                    ProductListActivity.category = category;
                    gotoNewActivity(ProductListActivity.class);
                } else if (promotionalOffer.getType().equals("2")) {
                    Product product = new Product();
                    product.setProductId(promotionalOffer.getId());
                    product.setName(promotionalOffer.getTitle());
                    ProductDetailsActivity.product = product;
                    gotoNewActivity(ProductDetailsActivity.class);
                }


            }
        });
    }


}

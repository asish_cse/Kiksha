package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 5/4/2016.
 */
public class Address extends BaseAddressModel {

    private String address_id;

    private String country_code;
    private String country;


    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }



    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }



}

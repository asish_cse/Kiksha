package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.Category;

import java.util.List;

/**
 * Created by Ashraful on 4/12/2016.
 */
public class CategoryAdapter extends ArrayAdapter<Category> {
    List<Category>categoryList;
    Context context;
    public CategoryAdapter(Context context, int resource, List<Category> categoryList) {
        super(context, resource, categoryList);
        this.context=context;
        this.categoryList=categoryList;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView= LayoutInflater.from(context).inflate(R.layout.item_category,parent,false);
        TextView nameTextView=(TextView) convertView.findViewById(R.id.tv_category_name);
        nameTextView.setText(getItem(position).getName());
        Log.d("========","----getview");
        Log.d("========","----getview"+getItem(position).getName());
        return convertView;
    }
}

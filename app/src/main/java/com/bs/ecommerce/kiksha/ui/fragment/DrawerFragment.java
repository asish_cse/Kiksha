package com.bs.ecommerce.kiksha.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.ui.activity.ProductListActivity;
import com.bs.ecommerce.kiksha.ui.adapter.CategoryAdapter;

import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/12/2016.
 */
public class DrawerFragment extends BaseFragment {

         @InjectView(R.id.lv_category)
          ListView listView;

          public Category category;



        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.dialog_fragment_category,container,false);
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            listView=(ListView)view.findViewById(R.id.lv_category);
            final CategoryAdapter categoryAdapter=new CategoryAdapter(getActivity(),0,category.getChildren());
            listView.setAdapter(categoryAdapter);

            Log.d("------------>>>>" , "||");

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Category category=categoryAdapter.getItem(position);
                    if(category.getChildren().size()>0)
                        replacfragment(category);
                    else
                    {
                        ProductListActivity.category= category;
                        gotoNewActivity(ProductListActivity.class);
                    }

                }
            });

        }

       private void callProductListApi()
       {
         //  NetworkServiceHandler.processCallBack(RestClient.get().getProductList(),getActivity());
       }

        private void showDialog(Category category)
        {
            CategoryDialogFragment categoryDialogFragment=new CategoryDialogFragment();
            categoryDialogFragment.category=category;
            categoryDialogFragment.show(getFragmentManager(),"dialog");
        }

    private void replacfragment(Category category)
    {
        DrawerFragment drawerFragment=new DrawerFragment();
        drawerFragment.category=category;
        getActivity().getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment_navigation_drawer,drawerFragment).addToBackStack(null).commit();
    }
}


package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/12/2016.
 */
public class OrderDetailsItem {
    private String product_type;
    private String order_id;
    private String product_id;
    private String item_id;
    private String item_name;
    private String item_sku;
    private String item_price;
    private String item_parent_id;
    private String parent_type;
    private String parent_order_id;
    private String parent_product_id;
    private String parent_item_id;
    private String parent_item_price;
    private int qty;
    private String thumbnail;

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_sku() {
        return item_sku;
    }

    public void setItem_sku(String item_sku) {
        this.item_sku = item_sku;
    }

    public String getItem_price() {
        return item_price;
    }

    public void setItem_price(String item_price) {
        this.item_price = item_price;
    }

    public String getItem_parent_id() {
        return item_parent_id;
    }

    public void setItem_parent_id(String item_parent_id) {
        this.item_parent_id = item_parent_id;
    }

    public String getParent_type() {
        return parent_type;
    }

    public void setParent_type(String parent_type) {
        this.parent_type = parent_type;
    }

    public String getParent_order_id() {
        return parent_order_id;
    }

    public void setParent_order_id(String parent_order_id) {
        this.parent_order_id = parent_order_id;
    }

    public String getParent_product_id() {
        return parent_product_id;
    }

    public void setParent_product_id(String parent_product_id) {
        this.parent_product_id = parent_product_id;
    }

    public String getParent_item_id() {
        return parent_item_id;
    }

    public void setParent_item_id(String parent_item_id) {
        this.parent_item_id = parent_item_id;
    }

    public String getParent_item_price() {
        return parent_item_price;
    }

    public void setParent_item_price(String parent_item_price) {
        this.parent_item_price = parent_item_price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}

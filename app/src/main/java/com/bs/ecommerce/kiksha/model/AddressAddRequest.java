package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 5/4/2016.
 */
public class AddressAddRequest extends BaseAddressModel {

    private String customer_id;

    private String country_id;

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }


}

package com.bs.ecommerce.kiksha.ui.activity;

import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.AddressAddRequest;
import com.bs.ecommerce.kiksha.model.AddressAddResponse;
import com.bs.ecommerce.kiksha.model.City;
import com.bs.ecommerce.kiksha.model.CityResponse;
import com.bs.ecommerce.kiksha.model.Country;
import com.bs.ecommerce.kiksha.model.CountryResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.view.FormViews;
import com.bs.ecommerce.kiksha.util.BroadCastUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 5/4/2016.
 */
@ContentView(R.layout.activity_new_address_add)
public class AddNewAdressActivity extends BaseActivity {

    @InjectView(R.id.et_first_name)
    EditText firstNameEditText;

    @InjectView(R.id.et_last_name)
    EditText lastNameEditText;

    @InjectView(R.id.et_company)
    EditText companyEditText;

    @InjectView(R.id.et_telephone)
    EditText telephoneEditText;

    @InjectView(R.id.et_fax)
    EditText faxEditText;

    @InjectView(R.id.btn_save)
    Button saveBtn;

    @InjectView(R.id.et_street_address1)
    EditText streetAddress1EditText;

    /*@InjectView(R.id.et_street_address2)
    EditText streetAddress2EditText;*/

    @InjectView(R.id.et_city)
    Spinner cityEditText;

    @InjectView(R.id.et_state)
    Spinner stateEditText;

    @InjectView(R.id.et_postal_code)
    EditText postalCodeEditText;

    @InjectView(R.id.spinner_country)
    Spinner countrySpinner;

    @InjectView(R.id.cb_add_as_default_billing)
    CheckBox addAsDefaultBillingCheckBox;

    @InjectView(R.id.cb_add_as_default_shipping)
    CheckBox addAsDefaultShippingCheckBox;

    protected List<Country> countryList;
    protected ArrayAdapter countryAdapter;
    protected ArrayAdapter cityAdapter;
    protected ArrayAdapter areaAdapter;
    protected List<String> areaList;
    protected List<City> cityList;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Add New Address");
        setClicklistener();
        callCountryListApi();
        callCityListApi();
        firstNameEditText.setText(preferenceService.GetPreferenceValue(PreferenceService.FIRST_NAME_KEY));
        lastNameEditText.setText(preferenceService.GetPreferenceValue(PreferenceService.LAST_NAME_KEY));

    }

    private void callCountryListApi() {
        NetworkServiceHandler.processCallBack(RestClient.get().getCountryListForDropdown(), this);
    }

    private void callCityListApi() {
        NetworkServiceHandler.processCallBack(RestClient.get().getCityForDropDown(""), this);
    }

    public void onEvent(CountryResponse countryResponse) {
        countryList = new ArrayList<>();

        if (countryResponse.getResponse_code() == 100) {
            countryList = countryResponse.getData();

            countryAdapter = new ArrayAdapter
                    (this, android.R.layout.simple_spinner_item, countryResponse.getStringList());

            countryAdapter.setDropDownViewResource
                    (android.R.layout.simple_spinner_dropdown_item);

            countrySpinner.setAdapter(countryAdapter);
        }

    }

    public void onEvent(CityResponse countryResponse) {
        cityList = new ArrayList<>();

        if (countryResponse.getResponse_code() == 100) {
            cityList = countryResponse.getData();

            cityAdapter = new ArrayAdapter
                    (this, android.R.layout.simple_spinner_item, countryResponse.getStringList());

            cityAdapter.setDropDownViewResource
                    (android.R.layout.simple_spinner_dropdown_item);

            cityEditText.setAdapter(cityAdapter);
            areaDataLoad();
        }

    }

    public void onEvent(AddressAddResponse addressAddResponse) {
        if (addressAddResponse.getResponse_code() == 100) {
            String msg = addressAddResponse.getData().getMessages().getMsg();
           // Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            toastMsg(msg);
            sendBroadcast(BroadCastUtils.ADDRESS_BOOK_REFRESH);
            finish();
        }
    }


    private void toastMsg(String str){
        Toast toast = Toast.makeText(this, Html.fromHtml("<font size='12' color='#FFFFFF'  ><b>" + str + "</b></font>"), Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM|Gravity.FILL_HORIZONTAL, -20, -20);
        // toast.setGravity(Gravity.CENTER, 50, 50);
        toast.getView().setPadding(10, 25, 10, 35);

//        toast.getView().setBackgroundColor(this.getResources().getColor(R.color.listdivider));
        toast.show();
    }

    //====================================================
    public void areaDataLoad() {
        cityEditText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                areaList = new ArrayList<>();
                areaList.clear();
                areaList.add("Select Area");

                if (cityList.get(position).getArea_list().length() > 2) {
                    List<String> area = Arrays.asList(cityList.get(position).getArea_list().split(","));
                    for (String ar : area) {
                        String aread = ar.substring(0, 1).toUpperCase() + ar.substring(1);
                        areaList.add(aread);
                    }
                }
                areaAdapter = new ArrayAdapter
                        (AddNewAdressActivity.this, android.R.layout.simple_spinner_item, areaList);

                areaAdapter.setDropDownViewResource
                        (android.R.layout.simple_spinner_dropdown_item);

                stateEditText.setAdapter(areaAdapter);

                if (cityList.get(position).getArea_list().length() > 2) {
                    if (AddressUpdateActivity.area_ch < 2) {

//                    if (AddressUpdateActivity.area.length() < 1) {
//                        Toast.makeText(AddNewAdressActivity.this, AddressUpdateActivity.area + "=This is " + AddressUpdateActivity.area_ch,
//                                Toast.LENGTH_LONG).show();
                        stateEditText.setSelection(areaAdapter.getPosition(AddressUpdateActivity.area));
//                    }
                        AddressUpdateActivity.area_ch++;

                    }
                }

                //

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    //====================================================
    protected void validateForm() {
        boolean isValid = true;

        if (!FormViews.isValidWithMark(firstNameEditText, "First Name")) {
            isValid = false;
        }

        if (!FormViews.isValidWithMark(lastNameEditText, "Last Name")) {
            isValid = false;
        }

        if (!FormViews.isValidWithMarkMobile(telephoneEditText, "Telephone")) {
            isValid = false;
        }

        if (!FormViews.isValidWithMark(streetAddress1EditText, "Street Address")) {
            isValid = false;
        }
        if (!FormViews.isValidWithMark(cityEditText, "Select City")) {
            isValid = false;
        }

        if (!FormViews.isValidWithMark(stateEditText, "Select Area")) {
            isValid = false;
        }
        /*if (!FormViews.isValidWithMark(streetAddress2EditText, "Street Address")) {
            isValid = false;
        }*/

        if (!FormViews.isValidWithMark(cityEditText, "City")) {
            isValid = false;
        }

        if (!FormViews.isValidWithMark(postalCodeEditText, "Zip/Postal Code")) {
            isValid = false;
        }

        if (!FormViews.isValidWithMark(countrySpinner, "Select Country")) {
            isValid = false;
        }

        if (isValid) {
            sendNewAddressRequest();
        }
    }

    protected void sendNewAddressRequest() {
        Call<AddressAddResponse> callback = RestClient.get().addNewAddress(getAddressRequestObject());
        NetworkServiceHandler.processCallBack(callback, this);
    }

    private AddressAddRequest getAddressRequestObject() {
        AddressAddRequest addressAddRequest = new AddressAddRequest();
        addressAddRequest.setFirstname(FormViews.getTexBoxFieldValue(firstNameEditText));
        addressAddRequest.setLastname(FormViews.getTexBoxFieldValue(lastNameEditText));
        addressAddRequest.setCompany(FormViews.getTexBoxFieldValue(companyEditText));
        addressAddRequest.setTelephone(FormViews.getTexBoxFieldValue(telephoneEditText));
        addressAddRequest.setFax(FormViews.getTexBoxFieldValue(faxEditText));

        String[] streetAddress = new String[1];
        streetAddress[0] = FormViews.getTexBoxFieldValue(streetAddress1EditText);
        //streetAddress[1] = FormViews.getTexBoxFieldValue(streetAddress2EditText);
        addressAddRequest.setStreet(streetAddress);
//................update.....................
//        addressAddRequest.setCity(FormViews.getTexBoxFieldValue(cityEditText));
        // addressAddRequest.setRegion(FormViews.getTexBoxFieldValue(stateEditText));
        addressAddRequest.setCity(cityList.get(cityEditText.getSelectedItemPosition()).getCity());
//        addressUpdateRequest.setRegion(FormViews.getTexBoxFieldValue(stateEditText));
        addressAddRequest.setArea(areaList.get(stateEditText.getSelectedItemPosition()));
        addressAddRequest.setPostcode(FormViews.getTexBoxFieldValue(postalCodeEditText));

        /*String countryLabel = countrySpinner.getSelectedItem().toString();
        addressAddRequest.setCountry_id(countryMap.get(countryLabel));*/

        addressAddRequest.setCountry_id(countryList.get(countrySpinner.getSelectedItemPosition()).getValue());

        addressAddRequest.setCustomer_id(preferenceService.GetPreferenceValue(PreferenceService.ENTITY_KEY));

        addressAddRequest.setDefaultBillingAddress(addAsDefaultBillingCheckBox.isChecked());
        addressAddRequest.setDefaultShippingAddress(addAsDefaultShippingCheckBox.isChecked());

        return addressAddRequest;

    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        saveBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resourceId = v.getId();

        if (resourceId == R.id.btn_save) {
            validateForm();
        }
    }
}

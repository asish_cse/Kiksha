package com.bs.ecommerce.kiksha.model;

import org.apache.commons.lang.WordUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BS62 on 5/4/2016.
 */
public class CityResponse extends BaseResponse {
    private List<City> data = new ArrayList<>();

    public List<City> getData() {
        return data;
    }

    public void setData(List<City> data) {
        this.data = data;
    }

    public List<String> getStringList() {
        List<String> list = new ArrayList<>();
        for (City country : data) {
//            list.add(WordUtils.capitalize(country.getCity().toString().trim()));
            list.add(country.getCity());
        }
        return list;
    }
}

package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by Ashraful on 4/15/2016.
 */
public class ProductDetail {
    private String name;
    private String id;
    private String short_description;
    private String description;
    private String type_id;
    private String price;
    private String special_price;
    private String final_price;
    private String sku;
    private String shipping_information;
    private String thumbnail;
    private List<String>images ;
    private List<Product> related_product;
    private int qty;

    public List<DynamicAttribute> getDynamic_attribute() {
        return dynamic_attribute;
    }

    public void setDynamic_attribute(List<DynamicAttribute> dynamic_attribute) {
        this.dynamic_attribute = dynamic_attribute;
    }

    private List<DynamicAttribute>dynamic_attribute;
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public List<Product> getRelated_product() {
        return related_product;
    }

    public void setRelated_product(List<Product> related_product) {
        this.related_product = related_product;
    }

    public String getShipping_information() {
        return shipping_information;
    }

    public void setShipping_information(String shipping_information) {
        this.shipping_information = shipping_information;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getSpecial_price() {
        return special_price;
    }

    public void setSpecial_price(String special_price) {
        this.special_price = special_price;
    }

    public String getFinal_price() {
        return final_price;
    }

    public void setFinal_price(String final_price) {
        this.final_price = final_price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}

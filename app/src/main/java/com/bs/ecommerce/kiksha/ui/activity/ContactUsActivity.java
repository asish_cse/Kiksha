package com.bs.ecommerce.kiksha.ui.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/28/2016.
 */
@ContentView(R.layout.activity_contact_us)
public class ContactUsActivity extends BaseActivity {
    @InjectView(R.id.tv_contact_details)
     TextView contactDetailsTextView;

    @InjectView(R.id.tv_rate_in_app_store)
    TextView rateInAppStoreTextView;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Contact Us");
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        contactDetailsTextView.setOnClickListener(this);
        rateInAppStoreTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int resourceId=v.getId();
        if(resourceId == contactDetailsTextView.getId()) {
            Intent intent = new Intent(this, WebActivity.class);
            intent.putExtra(WebActivity.INTENT_EXTRA_WEB_TYPE, WebActivity.TYPE_CONTACT_US);
            startActivity(intent);
        }
        else if(resourceId==rateInAppStoreTextView.getId()) {
            rateTheApp();
        }
        else if(resourceId==R.id.tv_view_app_intro)
        {

        }
    }

    private void rateTheApp() {
        Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + this.getPackageName())));
        }

    }
}

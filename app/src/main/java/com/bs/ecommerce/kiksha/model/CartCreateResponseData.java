package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/9/2016.
 */
public class CartCreateResponseData {
    private int cart_id;

    public int getCart_id() {
        return cart_id;
    }

    public void setCart_id(int cart_id) {
        this.cart_id = cart_id;
    }
}

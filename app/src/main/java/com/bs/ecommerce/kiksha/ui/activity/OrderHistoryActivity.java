package com.bs.ecommerce.kiksha.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.OrderHistory;
import com.bs.ecommerce.kiksha.model.OrderHistoryResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.adapter.OrderHistoryAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_order_history)
public class OrderHistoryActivity extends BaseActivity {
    @InjectView(R.id.lv_order_history)
    ListView orderHistoryLv;
    List<OrderHistory> orderHistoryList;
    OrderHistoryAdapter orderHistoryAdapter;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Order History");
        callOrderHistoryApi();
    }

    private void callOrderHistoryApi() {
        String customer_id = preferenceService.GetPreferenceValue(PreferenceService.ENTITY_KEY);
        Log.d("customer_id","customer_id=="+customer_id);
        Call<OrderHistoryResponse> callback = RestClient.get().getOrderHistory(customer_id,"");
        NetworkServiceHandler.processCallBack(callback, this);
    }

    public void onEvent(OrderHistoryResponse orderHistoryResponse) {
        if (orderHistoryResponse.getResponse_code() == 100) {
            orderHistoryList = new ArrayList<>();
            orderHistoryList = orderHistoryResponse.getData();
            initListView();
        }
    }

    private void initListView() {
        orderHistoryAdapter = new OrderHistoryAdapter(this, orderHistoryList);
        orderHistoryLv.setAdapter(orderHistoryAdapter);
        orderHistoryLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                OrderDetailsActivity.order_id = orderHistoryList.get(position).getOrder_id();
                gotoNewActivity(OrderDetailsActivity.class);
            }
        });
    }
}

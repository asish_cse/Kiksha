package com.bs.ecommerce.kiksha;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.bs.ecommerce.kiksha.util.BroadCastUtils;

/**
 * Created by Ashraful on 5/6/2016.
 */
public class BroadCastClass {
    Activity activity;
    RecreateBroadCast recreateBroadCast;

    public BroadCastClass(Activity activity)
    {
        this.activity=activity;
    }

    public void unregisterReceiver()
    {
        try {
            if (recreateBroadCast != null)
                activity.unregisterReceiver(recreateBroadCast);
        }
        catch(IllegalArgumentException ex)
        {

        }
    }

    public void setRecreateBroadCast(String broadCastString )
    {
        recreateBroadCast=new RecreateBroadCast();
        this.activity.registerReceiver(recreateBroadCast,new IntentFilter(broadCastString));
    }


    protected BroadcastReceiver clear_history_broadcast_reciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BroadCastUtils.CLEAR_ACTIVITY_STACK)) {
                activity.finish();
                activity.unregisterReceiver(this);
            }
        }
    };


    private class RecreateBroadCast extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

                activity.recreate();
                activity.unregisterReceiver(this);

        }
    }
}

package com.bs.ecommerce.kiksha.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.event.CloseDrawer;
import com.bs.ecommerce.kiksha.event.NewActivityTransition;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.ui.activity.ProductListActivity;
import com.bs.ecommerce.kiksha.ui.adapter.CategoryExpandableListAdapter;
import com.commonsware.cwac.merge.MergeAdapter;

import java.util.List;

import de.greenrobot.event.EventBus;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/19/2016.
 */
public class CategoryFragment extends BaseFragment {

    @InjectView(R.id.lv_all_category)
    ListView categoryListView;

    MergeAdapter mergeAdapter;

    public static Category categoryList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_category,container,false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mergeAdapter=new MergeAdapter();

        populateCategory();
        categoryListView.setAdapter(mergeAdapter);
    }

    private void populateCategory() {
        try {


            for (Category category : categoryList.getChildren()) {
                Log.d(".category.", ".."  );
                Log.d(".category.", ".." +category.getChildren().size() );
                if (category.getChildren().size() > 0) {
                    populateSubcategory(category);
                } else {
                    //noChild
                    populateStandAloneCategory(category);
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    private void populateStandAloneCategory(Category category) {
        LinearLayout categoryLinearLayout=(LinearLayout)getActivity().getLayoutInflater().
                inflate(R.layout.textview_standalone_category,null);
        TextView categoryTextView=(TextView)categoryLinearLayout.findViewById(R.id.tv_standaloneCategory);
        categoryTextView.setText(category.getName());
        categoryTextView.setTag(category.getCategory_id());
        onCategoryItemClick(category,categoryTextView);
        mergeAdapter.addView(categoryLinearLayout);
    }

    private void populateSubcategory(Category category) {
        LinearLayout categoryLinearLayout=(LinearLayout)getActivity().getLayoutInflater().
                inflate(R.layout.textview_parent_category,null);
        TextView categoryTextView=(TextView)categoryLinearLayout.findViewById(R.id.tv_parent_category);
        categoryTextView.setText(category.getName());
        categoryTextView.setTag(category.getCategory_id());
        //   categoryItemClickListener(expandableListCategory.getParentCategory(), categoryTextView);

        mergeAdapter.addView(categoryLinearLayout);

        ExpandableListView expandableListView=(ExpandableListView)getActivity().getLayoutInflater().
                inflate(R.layout.expandable_list_category,null,false);

        intializeExpandableList(expandableListView, category.getChildren());
    }

    private void intializeExpandableList(ExpandableListView exListCategory,List<Category> groupCategories)
    {
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            exListCategory.setIndicatorBounds(exListCategory.getLeft(), exListCategory.getRight());
        } else {
            exListCategory.setIndicatorBoundsRelative(exListCategory.getLeft(), exListCategory.getRight());
        }
        CategoryExpandableListAdapter adapter=new
                CategoryExpandableListAdapter(getActivity(),groupCategories);
        exListCategory.setAdapter(adapter);
        setListViewHeight(exListCategory);
        exListCategory.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int position, long id) {
                setListViewHeight(parent, position);
                return false;
            }
        });
        mergeAdapter.addView(exListCategory);
        //   mergeAdapter.addAdapter(adapter);
        // container.addView(exListCategory);
    }


    private void setListViewHeight(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        AbsListView.LayoutParams params = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT,AbsListView.LayoutParams.WRAP_CONTENT);
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    private void setListViewHeight(ExpandableListView listView , int group) {
        android.widget.ExpandableListAdapter listAdapter = listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();
                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    public void onEvent(NewActivityTransition newActivityTransition)
    {
        EventBus.getDefault().post(new CloseDrawer());
        gotoNewActivity(newActivityTransition.activityClass);

    }


    private void onCategoryItemClick(final Category category, TextView textView)
    {
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new CloseDrawer());

                ProductListActivity.category=category;
                gotoNewActivity(ProductListActivity.class);

            }
        });
    }
}
/*
    public int GetPixelFromDips(float pixels) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (pixels * scale + 0.5f);
    }*/
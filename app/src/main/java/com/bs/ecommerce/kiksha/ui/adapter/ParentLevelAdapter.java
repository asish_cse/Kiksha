package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.SharedPreferencesHelper;
import com.bs.ecommerce.kiksha.event.NewActivityTransition;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.ui.activity.ProductListActivity;
import com.bs.ecommerce.kiksha.ui.custom_view.CustomExpandableListView;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by BS62 on 5/24/2016.
 */
public class ParentLevelAdapter extends BaseExpandableListAdapter {
    private final Context context;
    private final List<Category> categoryListHeader;

    String temp="";

    public ParentLevelAdapter(Context context, List<Category> categoryListHeader) {
        this.context = context;
        this.categoryListHeader = categoryListHeader;
    }

    @Override
    public int getGroupCount() {
        return categoryListHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return (categoryListHeader.get(groupPosition).getChildren().size() > 0) ? 1 : 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return categoryListHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if ("1".equals(categoryListHeader.get(groupPosition).getIs_active())) {
            convertView =
                    (LayoutInflater.from(context)).inflate(R.layout.item_parent_expandable_group, parent, false);
            TextView textView_catName = (TextView) convertView.findViewById(R.id.textView_name);
            TextView category_new = (TextView) convertView.findViewById(R.id.category_new);

            ImageView imageView = (ImageView) convertView.findViewById(R.id.expandableIcon);
            ImageView iconImageView = (ImageView) convertView.findViewById(R.id.iv_icon);
            Category current = categoryListHeader.get(groupPosition);
            textView_catName.setText(current.getName().toUpperCase());
            if(current.getName().equals("Winter Wears")){
                category_new.setVisibility(View.VISIBLE);
            }else category_new.setVisibility(View.GONE);
            if (getChildrenCount(groupPosition) < 1) {
                imageView.setVisibility(View.INVISIBLE);
                onCategoryItemClick(current, textView_catName);
            } else {
                imageView.setVisibility(View.VISIBLE);
                if (isExpanded)
                    imageView.setImageResource(R.drawable.ic_chevron_up);
                else
                    imageView.setImageResource(R.drawable.ic_nxt_arrow);
            }

            temp = "";
            temp = current.getName().toString();
            if(temp.equalsIgnoreCase("FLOWERS & GIFTS")) {
                SharedPreferencesHelper.setMenu(context, "8");
                Log.d("=checkparentlevel===="+current.getName().toString(),"----groupPosition"+groupPosition);   // menu item expanding here
            }

        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        CustomExpandableListView customExpandableListView = new CustomExpandableListView(context);
        customExpandableListView.setGroupIndicator(null);
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            customExpandableListView.setIndicatorBounds(customExpandableListView.getLeft(), customExpandableListView.getRight());
        } else {
            customExpandableListView.setIndicatorBoundsRelative(customExpandableListView.getLeft(), customExpandableListView.getRight());
        }
        CategoryExpandableListAdapter adapter = new CategoryExpandableListAdapter(context, categoryListHeader.get(groupPosition).getChildren());
        customExpandableListView.setAdapter(adapter);
        Log.d("===par_lev_adapter=====","----groupPosition"+groupPosition);  //  menu item child view

        if(groupPosition == 9)
            SharedPreferencesHelper.setMenu(context,""+groupPosition);
        else if(groupPosition == 8 && temp.equalsIgnoreCase("FLOWERS & GIFTS"))
            SharedPreferencesHelper.setMenu(context,"8");
        else
            SharedPreferencesHelper.setMenu(context,""+groupPosition);
        return customExpandableListView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private void onCategoryItemClick(final Category category, TextView textView) {
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductListActivity.category = category;
                EventBus.getDefault().post(new NewActivityTransition(ProductListActivity.class));


            }
        });
    }
}

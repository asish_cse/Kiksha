package com.bs.ecommerce.kiksha.ui.activity;

import android.view.View;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/28/2016.
 */

@ContentView(R.layout.activity_my_order)
public class MyOrderOptionActivity extends BaseActivity{

    @InjectView(R.id.tv_view_orders)
    TextView viewOrderTextView;

    @InjectView(R.id.tv_order_history)
    TextView orderHistoryTextView;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("My Order");
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        viewOrderTextView.setOnClickListener(this);
        orderHistoryTextView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resourceId=v.getId();

        if(resourceId==R.id.tv_view_orders)
        {
            //gotoNewActivity(OrderDetailsActivity.class);
        }
        else if(resourceId==R.id.tv_order_history)
        {
            gotoNewActivity(OrderHistoryActivity.class);
        }

    }
}

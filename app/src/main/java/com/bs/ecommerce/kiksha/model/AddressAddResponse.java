package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/5/2016.
 */
public class AddressAddResponse extends BaseResponse {
    private Messages data;

    public Messages getData() {
        return data;
    }

    public void setData(Messages data) {
        this.data = data;
    }
}

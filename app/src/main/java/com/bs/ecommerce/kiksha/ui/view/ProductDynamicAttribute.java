package com.bs.ecommerce.kiksha.ui.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.Attribute;
import com.bs.ecommerce.kiksha.model.DynamicAttribute;
import com.bs.ecommerce.kiksha.model.DynamicAttributeValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ashraful on 4/19/2016.
 */
public class ProductDynamicAttribute  {

    Context context;
    ViewGroup ContainerLayout;
    List<DynamicAttribute>productDynamicAttributes;
    Map<Integer, Integer> attributeMap;

    public ProductDynamicAttribute(Context context, ViewGroup ContainerLayout,
                                   List<DynamicAttribute>productDynamicAttributes)
    {
        this.context=context;
        this.ContainerLayout=ContainerLayout;
        this.productDynamicAttributes=productDynamicAttributes;
        this.attributeMap = new HashMap<>();
    }

    public void generateView()
    {
        for(DynamicAttribute dynamicAttribute:productDynamicAttributes)
        {
            inflateView(dynamicAttribute);
        }
    }

    public void inflateView(final DynamicAttribute dynamicAttribute)
    {
        if(dynamicAttribute.getValues().size()>0) {
            LinearLayout dynamicAttributeLayout = (LinearLayout) LayoutInflater.from(context).
                    inflate(R.layout.dynamic_attribute_layout, ContainerLayout, false);
            TextView attributeTitleTextView = (TextView) dynamicAttributeLayout.findViewById(R.id.tv_atrribute_name);
            Spinner attributeValueSelctor = (Spinner) dynamicAttributeLayout.findViewById(R.id.sp_atrribute_selector);
            ArrayAdapter<String> adapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item,
                    getAttributeValueList(dynamicAttribute));
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            attributeValueSelctor.setAdapter(adapter);
            attributeValueSelctor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    int key = Integer.valueOf(dynamicAttribute.getAttribute_id());
                    String sr = dynamicAttribute.getValues().get(position).getValue_index();
                    int value = 0;
                    if(sr.equalsIgnoreCase("False"))
                        value = 0;
                    else
                        value = Integer.valueOf(dynamicAttribute.getValues().get(position).getValue_index());
                    attributeMap.put(key, value);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            attributeTitleTextView.setText(dynamicAttribute.getLabel());

            ContainerLayout.addView(dynamicAttributeLayout);
        }

    }

    private List<String> getAttributeValueList(DynamicAttribute dynamicAttribute)
    {
        List<String>data=new ArrayList<>();
        for(DynamicAttributeValue dynamicAttributeValue:dynamicAttribute.getValues())
        {
            data.add(dynamicAttributeValue.getLabel());
        }
        return data;
    }

    public List<Attribute> getAttributeList() {
        List<Attribute> attributeList = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : attributeMap.entrySet()) {
            Attribute attribute = new Attribute();
            attribute.setAttribute_id(entry.getKey());
            attribute.setAttribute_value(entry.getValue());

            attributeList.add(attribute);
        }

        return attributeList;
    }
}

package com.bs.ecommerce.kiksha.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.BroadCastClass;
import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.util.BroadCastUtils;
import com.google.inject.Inject;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import de.greenrobot.event.EventBus;
import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/1/2016.
 */

// toolbar control

public class BaseActivity extends RoboActionBarActivity implements View.OnClickListener {

    @InjectView(R.id.app_toolbar)
      Toolbar toolbar;

    @InjectView(R.id.image_btn_home)
    ImageButton homeBtn;

    @InjectView(R.id.image_btn_offer)
    ImageButton offerBtn;

    @InjectView(R.id.image_btn_profile)
    ImageButton profileBtn;

    @Inject
    PreferenceService preferenceService;

    BroadCastClass broadCastClass;

    TextView ui_badge_count;

    int value=0;
    public static int badgeCount = 0;

    @InjectView(R.id.search_view)
    MaterialSearchView searchView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_base,menu);

        MenuItem item = menu.findItem(R.id.menu_item_search);
        searchView.setMenuItem(item);

        refreshCartMenuItem(menu);
        return true;
    }

    /*private void initSearch(Menu menu)
    {
        MenuItem searchItem = menu.findItem(R.id.menu_item_search_);
        SearchView searchView = (SearchView) searchItem.getActionView();


        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if(null!=searchManager ) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(true);
    }*/



   /* @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        try {
            MenuItem searchViewMenuItem = menu.findItem(R.id.menu_item_search);
            SearchView mSearchView = (SearchView) MenuItemCompat.getActionView(searchViewMenuItem);
            int searchImgId = getResources().getIdentifier("android:id/search_button", null, null);
            ImageView v = (ImageView) mSearchView.findViewById(searchImgId);
            v.setImageResource(R.drawable.ic_menu_search);
        }
        catch (Exception ex)
        {

        }

        return super.onPrepareOptionsMenu(menu);
    }*/
    protected void refreshCartMenuItem(Menu menu) {
        final View menu_hotlist = menu.findItem(R.id.menu_item_cart).getActionView();
        ui_badge_count = (TextView) menu_hotlist.findViewById(R.id.hotlist_hot);
        menu_hotlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                /*if (preferenceService.GetPreferenceBooleanValue(PreferenceService.LOGGED_PREFER_KEY))
                    gotoNewActivity(MyCartActivity.class);
                else
                    gotoNewActivity(UserLoginActivity.class);*/
            gotoNewActivity(MyCartActivity.class);


            }
        });
        updateCartItemCount(badgeCount);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int menuItemId=item.getItemId();

        if(menuItemId==R.id.menu_item_more)
            gotoNewActivity(MoreMenuActivity.class);
        else if(menuItemId==R.id.menu_item_search) {
            //gotoNewActivity(SearchableActivity.class);

        }

        return true;
    }
    protected void updateCartItemCount(final int badgeCount)
    {
        if (ui_badge_count == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (badgeCount == 0)
                    ui_badge_count.setVisibility(View.INVISIBLE);
                else {
                    ui_badge_count.setVisibility(View.VISIBLE);
                    ui_badge_count.setText(Long.toString(badgeCount));
                }
            }
        });
    }



    @Override
    protected void  onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        value++;
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        registerEventBus();

        setToolbar();

        setBackButton();

        onViewCreated();

        setClicklistener();

        registerBroadcastReceiver();

        setMaterialSearch();

    }

    private void setMaterialSearch() {
        //------------------
        searchView.setVoiceSearch(false);
        searchView.setEllipsize(true);
        searchView.setSuggestions(getResources().getStringArray(R.array.query_suggestions));

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                Intent intent = new Intent(getApplicationContext(), SearchableActivity.class);
                intent.putExtra("query", query);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);

                //searchView.closeSearch();
                return false;
            }

            @Override
              public boolean onQueryTextChange(String query) {
                // Toast.makeText(getApplicationContext(), query, Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

                searchView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            }

            @Override
            public void onSearchViewClosed() {
                //
            }
        });
    }

    protected void onViewCreated() {

    }

    public void setClicklistener()
    {
        homeBtn.setOnClickListener(this);
        offerBtn.setOnClickListener(this);
        profileBtn.setOnClickListener(this);

    }
    protected void registerBroadcastReceiver()
    {
            broadCastClass=new BroadCastClass(this);
            registerReceiver(clear_history_broadcast_reciever, new IntentFilter(BroadCastUtils.CLEAR_ACTIVITY_STACK));


    }

    protected void gotoNewActivity(Class<?> activityClass) {

        if(!this.getClass().equals(activityClass)) {
            Intent intent = new Intent(this, activityClass);
            startActivity(intent);
            overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);

        }

    }

    protected void gotoHomeActivity(Class<?> activityClass) {

        if(!this.getClass().equals(activityClass)) {
            Intent intent = new Intent(this, activityClass);
            startActivity(intent);
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

            sendBroadcast(BroadCastUtils.CLEAR_ACTIVITY_STACK);

        }

    }

    public void showInfoSnackBar(String info)
    {
        // Snackbar.make(getActivityContentView(),info, Snackbar.LENGTH_SHORT).show();

//        Snackbar snackbar = Snackbar.make(getActivityContentView(), info, Snackbar.LENGTH_SHORT);
//        View view = snackbar.getView();
//        TextView textView = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(Color.WHITE);
//        textView.setBackgroundColor(getResources().getColor(R.color.msg_bg2));
//
//        snackbar.show();
        toastMsg(info);
    }

    private void toastMsg(String str){
        Toast toast = Toast.makeText(this, Html.fromHtml("<font size='12' color='#FFFFFF'  ><b>" + str + "</b></font>"), Toast.LENGTH_LONG);
         toast.setGravity(Gravity.BOTTOM|Gravity.FILL_HORIZONTAL, 0, 0);
        // toast.setGravity(Gravity.CENTER, 50, 50);
        toast.getView().setPadding(10, 25, 10, 35);

//        toast.getView().setBackgroundColor(this.getResources().getColor(R.color.listdivider));
        toast.show();
    }
    protected View getActivityContentView() {
        return this.getWindow().getDecorView().findViewById(android.R.id.content);

    }

    private void setToolbar() {
        try {
            setSupportActionBar(toolbar);
            Log.d("--base activity--",">>>");
        } catch (Exception ex) {

        }
    }
    protected void setBackButton() {

        try {

            android.support.v7.app.ActionBar actionBar = getSupportActionBar();
            final Drawable upArrow = ContextCompat.
                    getDrawable(this, R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent),
                    PorterDuff.Mode.SRC_ATOP);
            toolbar.setNavigationIcon(upArrow);
            actionBar.setHomeAsUpIndicator(upArrow);
            actionBar.setHomeButtonEnabled(true);

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseActivity.this.finish();
                    overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);


                }
            });
        }
        catch (Exception ex)
        {

        }


    }

    protected void setToolbarTitle(String title)
    {
        getSupportActionBar().setTitle(title);
    }



    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
        unRegisterEventBus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerEventBus();
        updateCartItemCount(badgeCount);

        if(value==2)
        {
            value=0;
        //    recreate();
            return;
        }
      else
        {
            value++;
            return;
        }


    }

    @Override
    protected void onDestroy() {
        unregisterBroadcast();
        super.onDestroy();

    }

    protected void unregisterBroadcast()
    {
        try {
            if(clear_history_broadcast_reciever!=null)
            unregisterReceiver(clear_history_broadcast_reciever);

        }
        catch (IllegalArgumentException ex)
        {

        }

    }

    protected void registerEventBus()
    {
        try {
            if (!isEventBusRegistered()) {
                EventBus.getDefault().register(this);
            }
        } catch (Exception ex) {

        }

    }
    protected void unRegisterEventBus()
    {
        if (isEventBusRegistered()) {
            EventBus.getDefault().unregister(this);
        }
    }

    protected boolean isEventBusRegistered()
    {
        return EventBus.getDefault().isRegistered(this);
    }

    public LinearLayoutManager getLinearLayoutManager(int layoutMangerType) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(layoutMangerType);
        layoutManager.setAutoMeasureEnabled(true);
        return layoutManager;
    }

    public LinearLayoutManager getLinearLayoutManager() {

        return getLinearLayoutManager(LinearLayoutManager.HORIZONTAL);
    }

    protected boolean isLoggedIn()
    {
        if(preferenceService.GetPreferenceBooleanValue(PreferenceService.LOGGED_PREFER_KEY))
            return true;
        else
            return false;
    }

    @Override
    public void onClick(View v) {
        int resourceId=v.getId();

        if(resourceId==R.id.image_btn_home)
        {
            gotoHomeActivity(HomePageActivity2.class);

        }
        else if(resourceId==R.id.image_btn_offer)
        {
            gotoNewActivity(PromotionalOfferActivity.class);
        }
        else if(resourceId==R.id.image_btn_profile)
        {
            gotoNewActivity(MyAccountActivity.class);
        }


    }
    protected void sendBroadcast(String message )
    {
        Intent   intent = new Intent();
        intent.setAction(message);
        sendBroadcast(intent);
    }
   protected BroadcastReceiver broadcast_reciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BroadCastUtils.RECREATE_VIEW)) {
                recreate();
                unregisterReceiver(this);
            }
        }
    };

    protected BroadcastReceiver clear_history_broadcast_reciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BroadCastUtils.CLEAR_ACTIVITY_STACK)) {
                finish();
                unregisterReceiver(this);
            }
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    public int getCartId() {
        return preferenceService.GetPreferenceIntValue(PreferenceService.CART_KEY);
        /*if (isLoggedIn()) {
            return preferenceService.GetPreferenceIntValue(PreferenceService.CART_KEY);
        } else {
            return preferenceService.GetPreferenceIntValue(PreferenceService.GUEST_CART_KEY);
        }*/
    }

    /*public void createNewCart() {
        Call<CartCreateResponse> callback = RestClient.get().createNewCart();
        NetworkServiceHandler.processCallBackWithoutProgressDialog(callback, this);
    }

    public void onEvent(CartCreateResponse cartCreateResponse) {
        int cartID = cartCreateResponse.getData().getCart_id();
        if (cartCreateResponse.getResponse_code() == 100) {
            preferenceService.SetPreferenceValue(PreferenceService.CART_KEY, cartID);

            badgeCount = 0;
            updateCartItemCount(badgeCount);
            //preferenceService.SetPreferenceValue(PreferenceService.GUEST_CART_KEY, cartID);

            *//*if (isLoggedIn()) {
                preferenceService.SetPreferenceValue(PreferenceService.CART_KEY, cartID);
            } else {
                preferenceService.SetPreferenceValue(PreferenceService.GUEST_CART_KEY, cartID);
            }*//*
        }
    }*/
}

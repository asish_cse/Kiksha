package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/12/2016.
 */
public class CouponCodeAddRequest {
    private int cartId;
    private String code;

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

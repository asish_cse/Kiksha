package com.bs.ecommerce.kiksha.model;

/**
 * Created by Asish on 12/11/2017.
 */
public class HomeSlider {

  //  @SerializedName("id")

    private String category_id;
    private String position;
    private String url_link;
    private String image_file;
    private String description;
    private String url_target;
    private String title;


    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getUrl_link() {
        return url_link;
    }

    public void setUrl_link(String url_link) {
        this.url_link = url_link;
    }

    public String getImage_file() {
        return image_file;
    }

    public void setImage_file(String image_file) {
        this.image_file = image_file;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl_target() {
        return url_target;
    }

    public void setUrl_target(String url_target) {
        this.url_target = url_target;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.FeaturedCategory;
import com.daimajia.slider.library.SliderLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Ashraful on 4/20/2016.
 */
public class FeaturedCategoryAdapter extends RecyclerView.Adapter {

    Context context;
    List<FeaturedCategory> featuredCategories;
    int width;
    int height;
    SliderLayout sliderLayout;
    protected OnItemClickListener mItemClickListener;


    public FeaturedCategoryAdapter(Context context,
                                   List<FeaturedCategory> featuredCategories, SliderLayout sliderLayout) {
        this.context = context;
        this.featuredCategories = featuredCategories;
        this.sliderLayout = sliderLayout;
        this.width -= convertDpToPixel(30);
        Log.d("realwidth", "" + convertDpToPixel(width));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemview = LayoutInflater.from(context).
                inflate(R.layout.item_feature_category, parent, false);
      /*  RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                width/2, width/2);
           itemview.setLayoutParams(params);*/
        if (height <= 0) {
            getWidthHeight(itemview);
        }
        return new CategoryViewHolder(itemview);
    }

    private void getWidthHeight(final View view) {
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        //   if (viewTreeObserver.isAlive()) {
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < 16) {
                    view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                if (height == 0) {
                    height = view.getWidth();
                    onEvent(height * 2);
                }

            }
        });
    }
    //}

    public void onEvent(int height) {
        android.view.ViewGroup.LayoutParams layoutParams = sliderLayout.getLayoutParams();
        layoutParams.height = height;
        sliderLayout.setLayoutParams(layoutParams);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        CategoryViewHolder viewHolder = (CategoryViewHolder) holder;
        FeaturedCategory featuredCategory = featuredCategories.get(position);

        Picasso.with(context).load(featuredCategory.getImg()).fit()
                .placeholder(R.drawable.placeholder)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(viewHolder.categoryImageView);
        //viewHolder.categoryName.setText(featuredCategory.getTitle());

    }

    @Override
    public int getItemCount() {
        return featuredCategories.size();
    }

    public FeaturedCategory getItem(int position) {
        return featuredCategories.get(position);
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    protected class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView categoryImageView;
        //protected TextView categoryName;

        public CategoryViewHolder(View itemView) {
            super(itemView);

            categoryImageView = (ImageView) itemView.findViewById(R.id.iv_feature_category);
            //categoryName = (TextView) itemView.findViewById(R.id.tv_feature_category_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public float convertDpToPixel(float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }
}

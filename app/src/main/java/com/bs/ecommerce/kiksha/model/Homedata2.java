package com.bs.ecommerce.kiksha.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Homedata2 {



    @SerializedName("category_id")
    @Expose
    private String category_id;
    @SerializedName("parent_id")
    @Expose
    private String parent_id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("is_active")
    @Expose
    private String is_active;
    @SerializedName("get_include_menu")
    @Expose
    private String get_include_menu;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("level")
    @Expose
    private String level;
    @SerializedName("banner")
    @Expose
    private FeatureProduct banner;

    @SerializedName("features_product")
    @Expose
    private List<Featureproductlist2> items;

    public List<Featureproductlist2> getItems() {
        return items;
    }

    public void setItems(List<Featureproductlist2> items) {
        this.items = items;
    }

//    @SerializedName("features_product")
//    @Expose
//    private Featureproductlist2 features_product;
//
//    public Featureproductlist2 getFeatures_product() {
//        return features_product;
//    }
//
//    public void setFeatures_product(Featureproductlist2 features_product) {
//        this.features_product = features_product;
//    }

    public FeatureProduct getBanner() {
        return banner;
    }

    public void setBanner(FeatureProduct banner) {
        this.banner = banner;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getGet_include_menu() {
        return get_include_menu;
    }

    public void setGet_include_menu(String get_include_menu) {
        this.get_include_menu = get_include_menu;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }


}

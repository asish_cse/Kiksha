package com.bs.ecommerce.kiksha.service;

import android.content.SharedPreferences;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Created by Ashraful on 11/25/2015.
 */
@Singleton
public class PreferenceService {
    public static String SHARED_PREF_KEY = "PREFER_KEY";
    public static String LOGGED_PREFER_KEY = "isLoggedIn";
    public static String TOKEN_KEY = "token";
    public static String ENTITY_KEY = "ENTITY_ID";
    public static String EMAIL_KEY = "EMAIL_ID";
    public static String FIRST_NAME_KEY = "FIRST_NAME";
    public static String LAST_NAME_KEY = "LAST_NAME";


    public static String SENT_TOKEN_TO_SERVER = "SENT_TOKEN_TO_SERVER_KEY";
    public static String REGISTRATION_COMPLETE = "REGISTRATION_COMPLETE_KEY";

    public static String GUEST_CART_KEY = "GUEST_CART_ID";
    public static String CART_KEY = "CART_ID";
    public static String SHIPPIN_IN_KEY = "SHIPPIN_IN";
    public static String SHIPPING_OUT_KEY = "SHIPPING_OUT";
    public static String SHIPPING_CHARGE_KEY = "SHIPPING_CHARGE";
    @Inject
    private SharedPreferences preferences;


    public String GetPreferenceValue(String key) {

        return preferences.getString(key, "");
    }

    public int GetPreferenceIntValue(String key) {

        return preferences.getInt(key, -1);
    }

    public boolean GetPreferenceBooleanValue(String key) {
        return preferences.getBoolean(key, false);
    }

    public void SetPreferenceValue(String key, String value) {
        preferences.edit().putString(key, value).commit();
    }

    public void SetPreferenceValue(String key, boolean value) {
        preferences.edit().putBoolean(key, value).commit();
    }

    public void SetPreferenceValue(String key, int value) {
        preferences.edit().putInt(key, value).commit();
    }


}

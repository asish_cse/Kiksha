package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/25/2016.
 */
public class UpdateQuantityRequest {
    private int cartId;
    private int itemId;
    private int qty;

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}

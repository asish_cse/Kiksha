package com.bs.ecommerce.kiksha.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.event.AddressEditEvent;
import com.bs.ecommerce.kiksha.model.Address;
import com.bs.ecommerce.kiksha.model.AddressRemoveRequest;
import com.bs.ecommerce.kiksha.ui.activity.AddressUpdateActivity;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashraful on 5/4/2016.
 */
public class AddressAdapter extends BaseAdapter {
    List<Address> addressList;
    int removedAddressPosition = -1;

    public AddressAdapter(List<Address> addressList) {
        this.addressList = addressList;
    }

    @Override
    public int getCount() {
        return addressList.size();
    }

    @Override
    public Object getItem(int position) {
        return addressList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_address, parent, false);

        final Address address = (Address) getItem(position);
        TextView nameTextView = (TextView) convertView.findViewById(R.id.tv_name);
        nameTextView.setText("" + address.getFirstname() + " " + address.getLastname());

        TextView companyTextView = (TextView) convertView.findViewById(R.id.tv_company);
        setText("", address.getCompany(), companyTextView);

        TextView streetAddress1TextView =
                (TextView) convertView.findViewById(R.id.tv_street_address1);
        setText("", address.getStreet()[0], streetAddress1TextView);


        /*TextView streetAddress2TextView=(TextView)convertView.findViewById(R.id.tv_street_address2);
        if (address.getStreet().length > 1) {
            setText(address.getStreet()[1], streetAddress2TextView);
        }*/

        TextView countryTextView = (TextView) convertView.findViewById(R.id.tv_country);
        setText(address.getCountry(), countryTextView);

        TextView cityTextView = (TextView) convertView.findViewById(R.id.tv_city);
        String str = address.getCity() + "," + address.getPostcode();
        setText("",address.getArea()+","+ address.getCity(), cityTextView);

        TextView postalTextView = (TextView) convertView.findViewById(R.id.tv_postal);
        setText("", address.getPostcode(), postalTextView);

        TextView telephoneTextView = (TextView) convertView.findViewById(R.id.tv_telephone);
        setText("T: ", address.getTelephone(), telephoneTextView);

        TextView faxTextView = (TextView) convertView.findViewById(R.id.tv_fax);
        setText("", address.getFax(), faxTextView);

        ImageView removeImageView = (ImageView) convertView.findViewById(R.id.iv_remove);
        ImageView editImageView = (ImageView) convertView.findViewById(R.id.iv_edit);

        removeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removedAddressPosition = position;
                EventBus.getDefault().post(new AddressRemoveRequest(address.getAddress_id()));
            }
        });

        editImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddressUpdateActivity.address = address;
                EventBus.getDefault().post(new AddressEditEvent());
            }
        });


        return convertView;
    }

    private void setText(String str, TextView textView) {
        if (isValidString(str)) {
            textView.setText(str);
        } else {
            textView.setVisibility(View.GONE);
        }

    }

    private void setText(String prefixStr, String str, TextView textView) {
        if (isValidString(str)) {
            textView.setText(prefixStr + str);
        } else {
            textView.setVisibility(View.GONE);
        }

    }

    private boolean isValidString(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private String getStringValue(String str) {
        if (isValidString(str)) {
            return str;
        } else {
            return "";
        }

    }

    public void removeAddress() {
        if (removedAddressPosition != -1) {
            addressList.remove(removedAddressPosition);
            notifyDataSetChanged();
        }
        removedAddressPosition = -1;
    }
}

package com.bs.ecommerce.kiksha.network;

import android.content.Context;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;

/**
 * Created by Ashraful on 4/1/2016.
 */
public class NetworkServiceHandler{

    public static String AuthToken="123";

    public static Map<String,String>getHeader()
    {
        Map<String,String>headerKeyValue=new HashMap<>();
        headerKeyValue.put("auth_token",AuthToken);
        return headerKeyValue;
    }
    public static<T> void processCallBack(Call<T> call, Context context)
    {
        call.enqueue(new CustomCallback<T>(context));
    }
    public static<T> void processCallBackWithoutProgressDialog(Call<T> call, Context context)
    {
        CustomCallback<T> customCallback=new CustomCallback<T>();
        customCallback.context=context;
        call.enqueue(customCallback);

    }
}

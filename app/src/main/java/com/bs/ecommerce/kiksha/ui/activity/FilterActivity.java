package com.bs.ecommerce.kiksha.ui.activity;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.model.FilterAndSortRequest;
import com.bs.ecommerce.kiksha.model.FilterItemListResponse;
import com.bs.ecommerce.kiksha.model.FilterOptions;
import com.bs.ecommerce.kiksha.model.NameValuePair;
import com.bs.ecommerce.kiksha.model.Product;
import com.bs.ecommerce.kiksha.model.ProductSortParameterResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.ui.adapter.FilterAdapter;
import com.bs.ecommerce.kiksha.ui.adapter.ProductAdapter;
import com.bs.ecommerce.kiksha.ui.adapter.SortParameterAdapter;

import java.util.ArrayList;
import java.util.List;

import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 5/9/2016.
 */
public class FilterActivity extends BaseActivity {
    @InjectView(R.id.rl_sort_layout)
    ViewGroup sortLayout;

    @InjectView(R.id.vg_filter_layout)
    ViewGroup filterLayout;

    @InjectView(R.id.lv_sort)
    ListView sortListView;

    @InjectView(R.id.btn_apply)
    Button filterApplyBtn;

    @InjectView(R.id.btn_cancel)
    Button filterCancelBtn;

    @InjectView(R.id.exlv_filter)
    ExpandableListView filterExpandableListView;

    @InjectView(R.id.img_btn_filter)
    ImageButton filterImgBtn;

    @InjectView(R.id.img_btn_sort_by)
    ImageButton sortByImgBtn;

    List<Product> productList;

    public static Category category=new Category();
    protected boolean isFilterEnabled;
    FilterAndSortRequest filterAndSortRequest;
    int filterablePageNumber=1;
    ProductAdapter productAdapter;
    FilterAdapter filterAdapter;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        filterAndSortRequest=new FilterAndSortRequest();
        filterAndSortRequest.setCategory_id(category.getCategory_id());
        isFilterEnabled=false;
        callSortListApi();
        callFilterItemListApi();
    }

    public  void callSortListApi() {
        NetworkServiceHandler.processCallBack(RestClient.get().getSortParameter(),this);
    }
    public void callFilterItemListApi() {
        NetworkServiceHandler.processCallBack(RestClient.get().getfilterItemList(category.getCategory_id()),this);
    }

    public void onEvent(ProductSortParameterResponse productSortParameterResponse)
    {

        List<NameValuePair> sortParameterList=productSortParameterResponse.getSort_params();
        Log.d("///==",">>>==::"+ sortParameterList.size());

        final SortParameterAdapter  adapter=new SortParameterAdapter(this,sortParameterList);
        sortListView.setAdapter(adapter);

        sortListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NameValuePair nameValuePair=adapter.getItem(position);
                Log.d("///=|||||||=",">>>==::");
                filterAndSortRequest.setSort_param(nameValuePair.getValue());
                callFilterEnableApi();
                closeSortView();
            }
        });
    }

    public void onEvent(FilterItemListResponse filterItemListResponse)
    {
        if(filterItemListResponse.getResponse_code()==100)
        {
             filterAdapter=new FilterAdapter(this,filterItemListResponse.getData());
            filterExpandableListView.setAdapter(filterAdapter);
        }

    }

    protected void callFilterEnableApi() {
        filterablePageNumber=1;
        isFilterEnabled=true;
        filterAndSortRequest.setPage(filterablePageNumber);
        this.productList=new ArrayList<>();
      //  productAdapter.clearData();
        //productAdapter.notifyDataSetChanged();
        NetworkServiceHandler.processCallBack(RestClient.get().getProductListBySortAndFilter
                (filterAndSortRequest),this);
    }

    protected void callFilterEnableMoreProductApi() {
        filterAndSortRequest.setPage(filterablePageNumber);
        NetworkServiceHandler.processCallBackWithoutProgressDialog(RestClient.get().getProductListBySortAndFilter
                (filterAndSortRequest),this);
    }

    public void onEvent(FilterOptions filterOptions)
    {
        closeFilterView();
    }



    @Override
    public void setClicklistener() {
        super.setClicklistener();
        sortByImgBtn.setOnClickListener(this);
        filterImgBtn.setOnClickListener(this);
        filterApplyBtn.setOnClickListener(this);
        filterCancelBtn.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resourceId=v.getId();
        if(resourceId==R.id.img_btn_sort_by)
        {
            processSortBy();

        }
        else if(resourceId==R.id.img_btn_filter)
        {
           processFiltering();
        }

        else if(resourceId==R.id.btn_apply)
        {
            closeFilterView();
            if(filterAdapter!=null)
            {

                filterAndSortRequest.setFilter(filterAdapter.getFilterList());
                callFilterEnableApi();

            }
        }
        else if(resourceId==R.id.btn_cancel)
        {
            closeFilterView();
            if(filterAdapter!=null)

            {

                filterAdapter.resetMapValue();
                filterAdapter.notifyDataSetChanged();
                filterAndSortRequest.setFilter(filterAdapter.getFilterList());
                callFilterEnableApi();


            }
        }
    }
    protected void processSortBy() {
        if(sortLayout.isShown())
        {
            Log.d("///==sort1",">>>==::");
            closeSortView();
        }
        else
        {
            Log.d("///==sort2",">>>==::");
            closeFilterView();
            sortLayout.setVisibility(View.VISIBLE);
            sortByImgBtn.setImageResource(R.drawable.ic_sortby_pressed);
        }


    }

    protected void processFiltering()
    {
        if(filterLayout.isShown())
        {
            filterLayout.startAnimation(AnimationUtils.loadAnimation(this,R.anim.activity_exit));
            closeFilterView();
        }
        else
        {
            closeSortView();

            filterLayout.setVisibility(View.VISIBLE);
            filterImgBtn.setImageResource(R.drawable.ic_filter_pressed);
            filterLayout.startAnimation(AnimationUtils.loadAnimation(this,R.anim.left_to_right));
        }
    }

   private void closeSortView()
    {
        sortLayout.setVisibility(View.GONE);
        sortByImgBtn.setImageResource(R.drawable.ic_sortby);
    }

    private void closeFilterView()
    {
        filterLayout.setVisibility(View.GONE);
        filterImgBtn.setImageResource(R.drawable.ic_filter);
    }



}

package com.bs.ecommerce.kiksha.ui.activity;

import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.BaseResponse;
import com.bs.ecommerce.kiksha.model.PasswordResetRequest;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.ui.view.FormViews;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/28/2016.
 */

@ContentView(R.layout.activity_password_reset)
public class PasswordResetActivity extends BaseActivity {

    @InjectView(R.id.et_email)
    EditText emailEditText;

    @InjectView(R.id.reset_btn)
    Button continueBtn;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        setToolbarTitle("Password Reset");
        continueBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resourceId=v.getId();

        if(resourceId==R.id.reset_btn)
        {
            validateForm();
        }
    }

    private void validateForm() {

        boolean isValid=true;

        if(!FormViews.isValidEmail(emailEditText,"Email"))
            isValid=false;

        if(isValid)
            sendPasswordResetRequest();
    }

    private void sendPasswordResetRequest() {

        PasswordResetRequest passwordResetRequest=new PasswordResetRequest();
        passwordResetRequest.setEmail(FormViews.getTexBoxFieldValue(emailEditText));
        NetworkServiceHandler.processCallBack(RestClient.get().resetPassword(passwordResetRequest),this);
    }

    public void onEvent(BaseResponse baseResponse)
    {
        if(baseResponse.getResponse_code()==100) {
//            Toast.makeText(getApplicationContext(), "Password Reseted Successfully", Toast.LENGTH_LONG)
//                    .show();

            toastMsg("Password Reseted Successfully.");
        }
    }

    private void toastMsg(String str){
        Toast toast = Toast.makeText(this, Html.fromHtml("<font size='12' color='#FFFFFF'  ><b>" + str + "</b></font>"), Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM|Gravity.FILL_HORIZONTAL, -20, -20);
        // toast.setGravity(Gravity.CENTER, 50, 50);
        toast.getView().setPadding(10, 25, 10, 35);

//        toast.getView().setBackgroundColor(this.getResources().getColor(R.color.listdivider));
        toast.show();
    }


}

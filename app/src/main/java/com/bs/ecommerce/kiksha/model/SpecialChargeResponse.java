package com.bs.ecommerce.kiksha.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BS62 on 5/4/2016.
 */
public class SpecialChargeResponse extends BaseResponse {
    private List<SpecialCharge> data = new ArrayList<>();

    public List<SpecialCharge> getData() {
        return data;
    }

    public void setData(List<SpecialCharge> data) {
        this.data = data;
    }

//    public List<String> getStringList() {
//        List<String> list = new ArrayList<>();
//        for (Country country : data) {
//            list.add(country.getLabel());
//        }
//        return list;
//    }
}

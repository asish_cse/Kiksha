package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/9/2016.
 */
public class CartDetailsResponse extends BaseResponse{
    private CartDetails data;

    public CartDetails getData() {
        return data;
    }

    public void setData(CartDetails data) {
        this.data = data;
    }
}

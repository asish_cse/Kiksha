package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.Category;
import com.commonsware.cwac.merge.MergeAdapter;

import java.util.List;

/**
 * Created by Ashraful on 4/25/2016.
 */
public class ParentCategoryExpandableListAdapter  extends BaseExpandableListAdapter{
    List<Category>mainCategoryList;
    Context context;
    MergeAdapter mergeAdapter;

    public ParentCategoryExpandableListAdapter( Context context,List<Category>categories,MergeAdapter adapter)
    {
        this.context=context;
        this.mainCategoryList=categories;
        this.mergeAdapter=adapter;
    }
    @Override
    public int getGroupCount() {
        return mainCategoryList.size() ;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if(mainCategoryList.get(groupPosition).getChildren().size()>0)
        return 1;
        else
            return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {

        return mainCategoryList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        convertView=LayoutInflater.from(context).
                inflate(R.layout.textview_parent_category,parent,false);
        Category category=(Category) getGroup(groupPosition);
        TextView categoryTextView=(TextView)convertView.findViewById(R.id.tv_parent_category);
        categoryTextView.setText(category.getName());
        categoryTextView.setTag(category.getCategory_id());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ExpandableListView expandableListView=(ExpandableListView) LayoutInflater.from(context).
                inflate(R.layout.expandable_list_category,parent,false);
        Category category=(Category) getGroup(groupPosition);
        CategoryExpandableListAdapter adapter=new
                CategoryExpandableListAdapter(context,category.getChildren());
        expandableListView.setAdapter(adapter);
        setListViewHeight(expandableListView);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int position, long id) {
                setListViewHeight(parent, position);
                return false;
            }
        });


        return expandableListView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
    private void setListViewHeight(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        AbsListView.LayoutParams params = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT,AbsListView.LayoutParams.WRAP_CONTENT);
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    private void setListViewHeight(ExpandableListView listView , int group) {
        android.widget.ExpandableListAdapter listAdapter = listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();
                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }
}

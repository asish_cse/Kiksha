package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by BS62 on 5/13/2016.
 */
public class PaymentMethodResponse extends BaseResponse {
    private List<PaymentMethod> data;

    public List<PaymentMethod> getData() {
        return data;
    }

    public void setData(List<PaymentMethod> data) {
        this.data = data;
    }
}

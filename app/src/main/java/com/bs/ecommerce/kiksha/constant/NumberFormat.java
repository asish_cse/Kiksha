package com.bs.ecommerce.kiksha.constant;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import static org.acra.ACRA.log;

/**
 * Created by Asish on 19/10/2017
 */
public class NumberFormat {
    public static String DoubleNumberFormat(String value){
        Double  d = 0.0 ;//Integer.parseInt(my_new_str);
        DecimalFormat formatter = new DecimalFormat("##,##,###.00");
        try {
            d = Double.parseDouble(value.replaceAll(",",""));
        } catch(NumberFormatException nfe) {
            log.d("-->>"+nfe,"==||==");
        }
        return formatter.format(d);
    }

    public static Double Format2Double(String value) {
        Double d = 0.0;//Integer.parseInt(my_new_str);

        try {
            d = Double.parseDouble(value.replaceAll(",", ""));
        } catch (NumberFormatException nfe) {
            log.d("-->>" + nfe, "==||==");
        }
        return d;
    }

    public static String Double2Format(Double value) {
        DecimalFormat formatter = new DecimalFormat("##,##,###.00");

        return formatter.format(value);
    }
    public static String round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.toPlainString();
    }
}

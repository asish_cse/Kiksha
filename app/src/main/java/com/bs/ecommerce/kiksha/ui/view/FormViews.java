package com.bs.ecommerce.kiksha.ui.view;

import android.graphics.Color;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Created by Ashraful on 12/8/2015.
 */
public class FormViews {

    public static View view;
    public static boolean isFormValid = true;


    public static boolean isEmpty(TextView etText) {

        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isEmpty(int resourceId) {
        TextView textView = (TextView) view.findViewById(resourceId);
        if (textView.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static String getTexBoxFieldValue(TextView etText) {
        return etText.getText().toString().trim();
    }

    public static String getTexBoxFieldValue(int resourceId) {
        TextView textView = (TextView) view.findViewById(resourceId);
        return textView.getText().toString().trim();
    }

    public static boolean isEqual(TextView first, TextView second) {
        if (first.getText().toString().trim().equals(second.getText().toString().trim())) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValidEmail(EditText etText, String errorMessage) {
        if (isEmpty(etText)) {
            etText.setError(errorMessage + " is Required");
            isFormValid = false;
            return false;
        } else {
            String email = etText.getText().toString();
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                etText.setError("Email is not valid");
                isFormValid = false;
                return false;
            }
            return true;
        }
    }

    private static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    private static boolean isValidMobileN(String phone) {
        boolean check;
        if (phone.length() < 6 || phone.length() > 13) {
            check = false;

        } else {
            check = true;
        }
        return check;
    }

//gfdgdfg
    public static boolean isValidWithMarkMobile(EditText etText, String errorMessage) {
        if (isEmpty(etText)) {
            etText.setError(errorMessage + " is Required");
            isFormValid = false;

            //etText.setError(displayText+" is Empty");
            return false;
        } else {
//            return true;

            String Regex = "[^\\d]";
            String PhoneDigits = etText.getText().toString().replaceAll(Regex, "");
            if (isValidMobile(PhoneDigits)) {
                if (isValidMobileN(PhoneDigits))
                    return true;
                else {
                    etText.setError(errorMessage + " Invalid mobile number");
                    isFormValid = false;

                    //etText.setError(displayText+" is Empty");
                    return false;
                }
            } else {
                etText.setError(errorMessage + " Invalid mobile number");
                isFormValid = false;

                //etText.setError(displayText+" is Empty");
                return false;
            }

        }
    }



    public static boolean isValidWithMark(EditText etText, String errorMessage) {
        if (isEmpty(etText)) {
            etText.setError(errorMessage + " is Required");
            isFormValid = false;

            //etText.setError(displayText+" is Empty");
            return false;
        } else {
            return true;
            /*
            String Regex = "[^\\d]";
            String PhoneDigits = etText.getText().toString().replaceAll(Regex, "");
            if (isValidMobile(PhoneDigits)) {
                if (isValidMobileN(PhoneDigits))
                    return true;
                else {
                    etText.setError(errorMessage + " Invalid mobile number");
                    isFormValid = false;

                    //etText.setError(displayText+" is Empty");
                    return false;
                }
            } else {
                etText.setError(errorMessage + " Invalid mobile number");
                isFormValid = false;

                //etText.setError(displayText+" is Empty");
                return false;
            }
            */
        }
    }

    public static boolean isValidWithMark(Spinner spinner, String value) {
        if (spinner.getSelectedItem().toString().equals(value)) {
            TextView errorText = (TextView) spinner.getSelectedView();
            errorText.setText("");
            errorText.setTextColor(Color.RED);
            errorText.setText(value);
            isFormValid = false;
            return false;
        } else {
            return true;
        }
    }

    public static boolean isValidWithErrorMeassage(EditText etText, String errorMessage) {
        if (isEmpty(etText)) {
            etText.setError(errorMessage);
            isFormValid = false;

            //etText.setError(displayText+" is Empty");
            return false;
        } else {

            return true;
        }
    }

    public static TextView getTextView(int resourceId) {
        return (TextView) view.findViewById(resourceId);
    }

    public static TextView getTextView(int resourceId, View view) {
        return (TextView) view.findViewById(resourceId);
    }

    public static void setText(int resourceId, String value, View parentView) {

        if (value == null)
            getTextView(resourceId, parentView).setText("");
        else
            getTextView(resourceId, parentView).setText(value);
    }

    public static void setTextOrHideIfEmpty(int resourceId, String value, View parentView) {
        if (value == null || value.length() == 0)
            getTextView(resourceId, parentView).setVisibility(View.GONE);
        else
            getTextView(resourceId, parentView).setText(value);
    }

    public static boolean isValidWithMark(int resourceId, String displayText) {
        if (isEmpty(resourceId)) {
            getTextView(resourceId).setError(displayText + " is Required");
            isFormValid = false;
            return false;
        } else {

            return true;
        }
    }

    public static boolean isValidWithMark(int resourceId) {
        TextView textView = getTextView(resourceId);
        if (isEmpty(textView)) {
            textView.setError(getCamelValue(textView.getTag().toString()) + " is Required");
            isFormValid = false;
            return false;
        } else {

            return true;
        }
    }

    public static String getCamelValue(String str) {
        String text = "";
        for (String w : str.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
            text = text + w + " ";
        }
        return text;
    }

}

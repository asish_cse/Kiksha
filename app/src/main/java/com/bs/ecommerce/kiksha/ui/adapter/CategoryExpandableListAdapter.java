package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.event.NewActivityTransition;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.ui.activity.ProductListActivity;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashraful on 4/19/2016.
 */
public class CategoryExpandableListAdapter extends BaseExpandableListAdapter {
    Context context;
    List<Category>categories;

    public CategoryExpandableListAdapter( Context context,List<Category>categories)
    {
        this.context=context;
        this.categories=categories;
    }
    @Override
    public int getGroupCount() {
        return categories.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return categories.get(groupPosition).getChildren().size();
    }

    @Override
    public Object getGroup(int groupPosition) {

        return categories.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return categories.get(groupPosition).getChildren().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if ("1".equals(categories.get(groupPosition).getIs_active())) {
            convertView =
                    (LayoutInflater.from(context)).inflate(R.layout.item_expandable_list_group, parent, false);
            TextView textView_catName = (TextView) convertView.findViewById(R.id.textView_name);
            TextView category_new = (TextView) convertView.findViewById(R.id.category_new);


            ImageView imageView = (ImageView) convertView.findViewById(R.id.expandableIcon);
            Category current = categories.get(groupPosition);
            textView_catName.setText(current.getName().toUpperCase());

            if(current.getName().equals("Winter Wears")){
                category_new.setVisibility(View.VISIBLE);
            }else category_new.setVisibility(View.GONE);
            if (getChildrenCount(groupPosition) < 1) {
                imageView.setVisibility(View.INVISIBLE);
                onCategoryItemClick(current, textView_catName);
            } else {
                imageView.setVisibility(View.VISIBLE);
                if (isExpanded)
                    imageView.setImageResource(R.drawable.ic_chevron_up);
                else
                    imageView.setImageResource(R.drawable.ic_chevron_down);
            }

            Log.d("========","----");
            Log.d("========","----"+isExpanded);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Category current = (Category)getChild(groupPosition, childPosition);
        if ("1".equals(current.getIs_active())) {
            convertView =
                    (LayoutInflater.from(context)).inflate(R.layout.item_expandable_list_child, parent, false);
            TextView textView_catName = (TextView) convertView.findViewById(R.id.textView_name);
            TextView category_new = (TextView) convertView.findViewById(R.id.category_new);

            textView_catName.setText(current.getName());
            onCategoryItemClick(current, textView_catName);


            if(current.getName().equals("Winter Wears")){
                category_new.setVisibility(View.VISIBLE);
            }else category_new.setVisibility(View.GONE);
            /*if (childPosition == getChildrenCount(groupPosition)-1) {
                convertView.setPadding(convertDptoPx(46), convertDptoPx(14), 0,  convertDptoPx(14));
            }*/
        }

        return convertView;
    }

    protected  int convertDptoPx(int dpValue)
    {
        float density = context.getResources().getDisplayMetrics().density;
        int px = (int)(dpValue * density);
        return px;
    }

    private void onCategoryItemClick(final Category category, TextView textView)
    {
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductListActivity.category=category;
                EventBus.getDefault().post(new NewActivityTransition(ProductListActivity.class));

                Log.d("========","----this is click");
            }
        });
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}

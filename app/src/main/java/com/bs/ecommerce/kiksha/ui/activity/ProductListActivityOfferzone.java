package com.bs.ecommerce.kiksha.ui.activity;

import android.support.v7.widget.GridLayoutManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.ResponseMessage;
import com.bs.ecommerce.kiksha.constant.ViewType;
import com.bs.ecommerce.kiksha.model.Product;
import com.bs.ecommerce.kiksha.model.ProductListResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.ui.adapter.ProductAdapter;
import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.paginate.Paginate;

import java.util.List;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/15/2016.
 */
@ContentView(R.layout.activity_product_list)
public class ProductListActivityOfferzone extends FilterActivity {

    @InjectView(R.id.rclv_product)
    ObservableRecyclerView productRecyclerView;


    @InjectView(R.id.img_btn_three_grid)
    ImageButton threeGridImgBtn;

    @InjectView(R.id.img_btn_two_grid)
    ImageButton twoGridImgBtn;

    @InjectView(R.id.rl_bottom_bar)
    ViewGroup bottomBarView;

    @InjectView(R.id.rl_top_bar)
    RelativeLayout filterBar;


    GridLayoutManager gridLayoutManager;

    int pageNumber=1;

    Paginate paginate;

    private boolean loading;
    private int newSpanCount=2;
    private boolean hasdata=true;


    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setRecyclerview();
    }


    private void setRecyclerview()
    {
        gridLayoutManager=new GridLayoutManager(this,2);
        productRecyclerView.setLayoutManager(gridLayoutManager);
        productRecyclerView.setHasFixedSize(true);
        setObserveScrollListener();
        callProductListApi();
        setToolbarTitle(category.getName());

    }
    private void setObserveScrollListener()
    {
        productRecyclerView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {

            }

            @Override
            public void onDownMotionEvent() {

            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {
                if (scrollState == ScrollState.UP) {
                    if(bottomBarView.isShown())
                        bottomBarView.setVisibility(View.GONE);
                } else if (scrollState == ScrollState.DOWN) {

                    if(!bottomBarView.isShown())
                        bottomBarView.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        threeGridImgBtn.setOnClickListener(this);
        twoGridImgBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int resourceId=v.getId();

        if(resourceId==R.id.img_btn_two_grid)
        {
            if(productAdapter!=null)
                setTwoGridImgBtnGridProductView();
        }
        else if(resourceId==R.id.img_btn_three_grid)
        {
            if(productAdapter!=null)
                setThreeGridImgBtnGridProductView();
        }

    }



    private void setThreeGridImgBtnGridProductView() {
        threeGridImgBtn.setImageResource(R.drawable.ic_three_grid_pressed);
        twoGridImgBtn.setImageResource(R.drawable.ic_two_grid);
        productAdapter.ViewFormat = ViewType.THREE_COLUMN_GRID;
        updateColumninPerRow(3);
    }
    private void setTwoGridImgBtnGridProductView() {
        twoGridImgBtn.setImageResource(R.drawable.ic_two_grid_pressed);
        threeGridImgBtn.setImageResource(R.drawable.ic_three_grid);

        productAdapter.ViewFormat = ViewType.GRID;
        updateColumninPerRow(2);
    }





    private void updateColumninPerRow(int numberofColumPerRow) {
        gridLayoutManager.setSpanCount(numberofColumPerRow);
        gridLayoutManager.requestLayout();
    }

    protected  void callProductListApi()
    {
        NetworkServiceHandler.processCallBack(RestClient.get().getProductListforoffer
                (category.getCategory_id()),this);


    }
    public void callMoreProductListApi()
    {
        NetworkServiceHandler.processCallBackWithoutProgressDialog(RestClient.get().getProductListforoffer
                (category.getCategory_id()),this);


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    public void onEvent(ProductListResponse productListResponse) {
        loading = false;
        filterBar.setVisibility(View.VISIBLE);

        if(productListResponse.getResponse_code() == ResponseMessage.OPERATION_SUCCESSFUL) {
            if (isFilterEnabled) {
                if (filterablePageNumber == 1 && productListResponse.getData().size() == 0) {
                    populateNewDataInAdapter(productListResponse.getData());
                    showInfoSnackBar("No Product Found ");
                    doActionOnNoMoreProduct();
                    return;
                }
            } else {
                if (pageNumber == 1 && productListResponse.getData().size() == 0) {
                    filterBar.setVisibility(View.GONE);
                    showInfoSnackBar("No Product Found ");
                    doActionOnNoMoreProduct();
                    return;
                }
            }



        }

        if(productListResponse.getResponse_code() == 201) {
            doActionOnNoMoreProduct();
        } else if (shouldAddIteminExistingList() ) {
            addMoreDataInAdapter(productListResponse.getData());
        } else if (productListResponse.getData()!= null ) {
            populateNewDataInAdapter(productListResponse.getData());
        }

        productAdapter.SetOnItemClickListener(new ProductAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ProductDetailsActivity.product=productAdapter.getItem(position);
                gotoNewActivity(ProductDetailsActivity.class);
            }
        });

    }

    protected boolean shouldAddIteminExistingList()
    {
        if(productList != null && productList.size()>0 && productAdapter!=null)
            return true;
        else
            return false;
    }

    private void doActionOnNoMoreProduct()
    {
        hasdata=false;
        if(paginate!=null)
            paginate.setHasMoreDataToLoad(false);
    }

    private void populateNewDataInAdapter(List<Product>productList)
    {
        this.productList = productList;
       if(productList.size()>0)
           hasdata=true;

        if(!isFilterEnabled) {
            productAdapter = new ProductAdapter(this, this.productList);
            productRecyclerView.setAdapter(productAdapter);
            //setScrollListener();

        } else {
            productAdapter.swap(this, this.productList);
        }


    }

    private void addMoreDataInAdapter(List<Product>productList)
    {
        int range = productAdapter.getItemCount();
        // productAdapter.products.addAll(productsResponse.getProducts());
        productAdapter.addAll(productList);
        loading = false;
        productAdapter.notifyItemRangeInserted(range,productList.size());
        productAdapter.notifyDataSetChanged();
    }

    private void setScrollListener() {
        int threshold=4;
        paginate = Paginate.with(productRecyclerView, new Paginate.Callbacks() {
            @Override
            public void onLoadMore() {
              //  onLoadMoreCallback();
            }

            @Override
            public boolean isLoading() {
                if(!hasdata)
                    paginate.setHasMoreDataToLoad(false);
                return loading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return !hasdata;
            }
        })
                .setLoadingTriggerThreshold(threshold)
               /* .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                    @Override
                    public int getSpanSize() {
                        return newSpanCount ;
                    }
                })*/
                .build();


    }
    public void onLoadMoreCallback()
    {

        if(hasdata)
        {
            loading=true;
           // ++pageNumber;
            if(isFilterEnabled)
            {
                filterablePageNumber++;
                callFilterEnableMoreProductApi();
                return;
            }
            else
           callMoreProductListApi();
        }
        else
        {
            loading=false;
            paginate.setHasMoreDataToLoad(false);
        }
    }
}

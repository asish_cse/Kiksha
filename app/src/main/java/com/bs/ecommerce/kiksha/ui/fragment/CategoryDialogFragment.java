package com.bs.ecommerce.kiksha.ui.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.ui.adapter.CategoryAdapter;

import roboguice.fragment.RoboDialogFragment;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/12/2016.
 */
public class CategoryDialogFragment extends RoboDialogFragment{

    @InjectView(R.id.lv_category)
    ListView listView;
    public Category category;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);

        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.YELLOW));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        return dialog;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_category,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final CategoryAdapter categoryAdapter=new CategoryAdapter(getActivity(),0,category.getChildren());
        listView.setAdapter(categoryAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Category category=categoryAdapter.getItem(position);
                if(category.getChildren().size()>0)
                    showDialog(category);

            }
        });

    }

    private void showDialog(Category category)
    {
        CategoryDialogFragment categoryDialogFragment=new CategoryDialogFragment();
        categoryDialogFragment.category=category;
        categoryDialogFragment.show(getFragmentManager(),"dialog");
    }
}

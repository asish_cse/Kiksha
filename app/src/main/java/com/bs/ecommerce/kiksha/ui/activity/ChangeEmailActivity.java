package com.bs.ecommerce.kiksha.ui.activity;

import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.EmailChangeRequest;
import com.bs.ecommerce.kiksha.model.EmailChangeResponse;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;
import com.bs.ecommerce.kiksha.ui.view.FormViews;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by Ashraful on 4/27/2016.
 */

@ContentView(R.layout.activity_change_email)
public class ChangeEmailActivity extends BaseActivity {

    @InjectView(R.id.et_new_email)
    EditText newEmailEditText;

    @InjectView(R.id.et_confirm_new_email)
    EditText confirmNewEmailEditText;

    @InjectView(R.id.btn_save)
    Button saveBtn;

    @InjectView(R.id.tv_user_current_email)
    TextView currentEmailTextView;

    String currentEmailStr;

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Change Email");
        saveBtn.setOnClickListener(this);
        showCurrentEmail();
    }

    private void showCurrentEmail()
    {
        currentEmailStr= preferenceService.GetPreferenceValue(PreferenceService.EMAIL_KEY);
        currentEmailTextView.setText("Your Current Email: "+currentEmailStr);
    }

    private void validateForm()
    {
        boolean isValid=true;

        if(!FormViews.isValidEmail(newEmailEditText,"New Email "))
            isValid=false;
        if(!FormViews.isValidEmail(confirmNewEmailEditText,"Confirm New Email "))
            isValid=false;
        if(!FormViews.isEmpty(newEmailEditText) && !FormViews.isEmpty(confirmNewEmailEditText))
        {
            if(!FormViews.isEqual(newEmailEditText,confirmNewEmailEditText))
            {
                confirmNewEmailEditText.setError("Confirm your Email");
                isValid=false;
            }
        }

        if(isValid)
        {
            sendEmailChangeRequest();
        }
    }

    private void sendEmailChangeRequest() {
        EmailChangeRequest emailChangeRequest=new EmailChangeRequest();
        emailChangeRequest.setNewEmail(FormViews.getTexBoxFieldValue(newEmailEditText));
        emailChangeRequest.setCurrentEmail(currentEmailStr);
        NetworkServiceHandler.processCallBack(RestClient.get().changeEmail(emailChangeRequest),this);
    }

    public void onEvent(EmailChangeResponse emailChangeResponse) {
        if(emailChangeResponse.getResponse_code()==100) {
         //   Toast.makeText(getApplication(), "Email Changed Successfully", Toast.LENGTH_LONG).show();
            toastMsg("Email Changed Successfully");
            preferenceService.SetPreferenceValue(PreferenceService.EMAIL_KEY,
                    emailChangeResponse.getData().getCustomer().getEmail());
            finish();
        }
    }

    private void toastMsg(String str){
        Toast toast = Toast.makeText(this, Html.fromHtml("<font size='12' color='#FFFFFF'  ><b>" + str + "</b></font>"), Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM|Gravity.FILL_HORIZONTAL, -20, -20);
        // toast.setGravity(Gravity.CENTER, 50, 50);
        toast.getView().setPadding(10, 25, 10, 35);

//        toast.getView().setBackgroundColor(this.getResources().getColor(R.color.listdivider));
        toast.show();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resourceId=v.getId();

        if(resourceId==R.id.btn_save)
        {
     validateForm();
        }


    }
}

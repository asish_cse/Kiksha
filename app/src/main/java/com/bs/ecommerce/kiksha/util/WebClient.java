package com.bs.ecommerce.kiksha.util;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by BS62 on 5/30/2016.
 */
public class WebClient extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        view.loadUrl("javascript:document.getElementByClassName('std')");
    }
}

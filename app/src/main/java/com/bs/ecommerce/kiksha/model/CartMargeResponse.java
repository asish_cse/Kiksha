package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/31/2016.
 */
public class CartMargeResponse extends BaseResponse {
    private CartMargeData data;

    public CartMargeData getData() {
        return data;
    }

    public void setData(CartMargeData data) {
        this.data = data;
    }
}

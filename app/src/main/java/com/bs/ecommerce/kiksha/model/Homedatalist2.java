package com.bs.ecommerce.kiksha.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Homedatalist2 {

    @SerializedName("data")
    @Expose
    private ArrayList<Homedata2> contacts = new ArrayList<>();

    /**
     * @return The contacts
     */
    public ArrayList<Homedata2> getContacts() {
        return contacts;
    }

    /**
     * @param contacts The contacts
     */
    public void setContacts(ArrayList<Homedata2> contacts) {
        this.contacts = contacts;
    }
}
package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/13/2016.
 */
public class CartShippingCharge {
    private int cartId;
    private String shipping_charge;

    public String getShipping_charge() {
        return shipping_charge;
    }

    public void setShipping_charge(String shipping_charge) {
        this.shipping_charge = shipping_charge;
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }
}

package com.bs.ecommerce.kiksha.ui.activity;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.CartId;
import com.bs.ecommerce.kiksha.model.CartShippingCharge;
import com.bs.ecommerce.kiksha.model.MessageResponse;
import com.bs.ecommerce.kiksha.model.PaymentMethod;
import com.bs.ecommerce.kiksha.model.PaymentMethodResponse;
import com.bs.ecommerce.kiksha.model.PaymentMethodSetRequest;
import com.bs.ecommerce.kiksha.model.ShippingMethodSetRequest;
import com.bs.ecommerce.kiksha.network.NetworkServiceHandler;
import com.bs.ecommerce.kiksha.network.RestClient;
import com.bs.ecommerce.kiksha.service.PreferenceService;

import java.util.List;

import retrofit2.Call;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_payment_method)
public class PaymentMethodActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {
    @InjectView(R.id.rg_payment_methods)
    RadioGroup paymentMethodGroup;
    @InjectView(R.id.btn_continue)
    Button continueBtn;

    @InjectView(R.id.transcation_id)
    EditText transaction_id;

    @InjectView(R.id.transcation_lay)
    LinearLayout transcati_onlayout;


    private Context con;

    private boolean bkash_flag = false;
    LinearLayout bkash_Layout;

    private static LayoutInflater inflater = null;
    private View linearView;
    private int creditCardPosition = 0;
    private Spinner ccTypes;
    private boolean hasCreditCard = false;
    private boolean hasBkash = false;
    public static ShippingMethodSetRequest shippingCode = new ShippingMethodSetRequest();

    @Override
    protected void onViewCreated() {
        super.onViewCreated();
        setToolbarTitle("Payment Method");
        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Log.d("code_data", "code==" + shippingCode.getMethod_code());
        if(shippingCode==null){}else {
            if(shippingCode.getMethod_code()==null){}else {
                if (shippingCode.getMethod_code().length() > 0) setShippingMethodApi(shippingCode);
            }
        }
        con = this;
        bkash_flag = false;
        getPaymentMethodApi();
    }

    @Override
    public void setClicklistener() {
        super.setClicklistener();
        continueBtn.setOnClickListener(this);
    }

    private void setShippingMethodApi(ShippingMethodSetRequest sh) {
        int cartId = getCartId();
        if (cartId != -1) {
            Call<MessageResponse> callback = RestClient.get().setShippingMethodR(sh.getCartId(),sh.getMethod_code(),sh);
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar("Cart not found");
        }

    }

    private void getPaymentMethodApi() {

        int cartId = getCartId();
        if (cartId != -1) {
            Call<PaymentMethodResponse> callback = RestClient.get().getAllPaymentMethod(getCartIdObject(cartId));
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }
    }

    private CartShippingCharge getCartIdObject(int cartId) {
        String shipping_charge = preferenceService.GetPreferenceValue(PreferenceService.SHIPPING_OUT_KEY);
        Log.d("shipping_chargeAA", "===" + shipping_charge);
        CartShippingCharge cartIdRequest = new CartShippingCharge();
        cartIdRequest.setCartId(cartId);
        cartIdRequest.setShipping_charge(shipping_charge);
        return cartIdRequest;
    }

    public void onEvent(PaymentMethodResponse paymentMethodResponse) {
        if (paymentMethodResponse.getResponse_code() == 100 && paymentMethodResponse.getData().size() > 0) {
            List<PaymentMethod> paymentMethodList = paymentMethodResponse.getData();
            Log.d("||>>", "|pay size||==="+paymentMethodList.size());
            for (int i = paymentMethodList.size()-1 ; i > 0; i--) {
                PaymentMethod paymentMethod = paymentMethodList.get(i);
//                if ("sslcommerz".equals(paymentMethod.getCode())) {
//
//                } else
                if ("cashondelivery".equals(paymentMethod.getCode())) {
                    Log.d("||>>", "|pay size||==="+paymentMethod.getCode());
                }
//                else if ("bkash".equals(paymentMethod.getCode())) {
//                    Log.d("||>>", "|pay bkash||==="+paymentMethod.getCode());
//                }
                else {
                    paymentMethodList.remove(i);
                }
            }
//            for (int i = paymentMethodList.size()-1 ; i > 0; i--) {
//                PaymentMethod paymentMethod = paymentMethodList.get(i);
//                Log.d("||>>", "|pay m code||==="+paymentMethod.getCode());
//            }
         //   Log.d("||>>", "|pay size||==="+paymentMethodList.size());
            setUpRadioButton(paymentMethodList);
        }
    }

    private void setUpRadioButton(List<PaymentMethod> data) {
        hasCreditCard = false;
        hasBkash = false;
        for (int i = 1; i < data.size(); i++) {
            View radioButtonView = inflater.inflate(R.layout.item_radio_button, null);
//            View radioButtonView = inflater.inflate(R.layout.item_radio_button, container, false);
            RadioButton radioButton = (RadioButton) radioButtonView.findViewById(R.id.radio_button);
            radioButton.setText(data.get(i).getTitle());



            //////////////

            radioButton.setId(i);
            radioButton.setTag(data.get(i));
            if (data.get(i).getCode().equals("cashondelivery") || data.get(i).getCode().equals("free")) {
                radioButton.setChecked(true);
            }
            paymentMethodGroup.addView(radioButton);

//            if (data.get(i).getCode().equals("ccsave")) {
//                hasCreditCard = true;
//                creditCardPosition = i;
//            }
//            if (data.get(i).getCode().equals("bkash")) {
//                hasBkash = true;
//                Log.d("||>>", "|bkash selected||==="+data.get(i).getCode());
//                creditCardPosition = i;
//            }
        }
        paymentMethodGroup.setOnCheckedChangeListener(this);

        paymentMethodGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                Log.d("||>>", "|radio selected||==="+checkedId);

//                if(checkedId == 1){
//                    transcati_onlayout.setVisibility(View.VISIBLE);
//                    bkash_flag = true;
//                }else{
//                    transcati_onlayout.setVisibility(View.GONE);
//                    bkash_flag = false;
//                }


            }
        });

        if (hasCreditCard) {
            addViewToGroup(creditCardPosition);
        }

//        if (hasBkash) {
//            addViewToGroup1(creditCardPosition);
//        }

    }

    private void addViewToGroup1(int position) {

        linearView = inflater.inflate(R.layout.item_radio_button, paymentMethodGroup, false);
        paymentMethodGroup.addView(linearView, position + 1);
      //  linearView.setVisibility(View.GONE);

    //    bkash_Layout = (LinearLayout) radioButtonView.findViewById(R.id.bkash_layout);
    }


    private void addViewToGroup(int position) {
        linearView = inflater.inflate(R.layout.payment_method_credit_card_details, paymentMethodGroup, false);
        paymentMethodGroup.addView(linearView, position + 1);
        linearView.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);

        int resId = v.getId();
        if (resId == R.id.btn_continue) {

            callSetPaymentMethodApi();
//            if(bkash_flag){
//                String bkas_id = transaction_id.getText().toString();
//                Log.d("||>>bkasid", "||==="+bkas_id);
//                if(bkas_id.length()>6)
//                    callSetPaymentMethodApibkas(bkas_id);
//                else
//                    showInfoSnackBar("Invalid transaction ID");
//            }else {
//                callSetPaymentMethodApi();
//            }
        }
    }

    private void callSetPaymentMethodApibkas(String bkas) {
        int cartId = getCartId();
        if (cartId != -1) {
            Call<MessageResponse> callback = RestClient.get().setPaymentMethod_bkas(bkas,getPaymentMethodSetObject(cartId));
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }

    }

    private void callSetPaymentMethodApi() {
        int cartId = getCartId();
        if (cartId != -1) {
            Call<MessageResponse> callback = RestClient.get().setPaymentMethod(getPaymentMethodSetObject(cartId));
            NetworkServiceHandler.processCallBack(callback, this);
        } else {
            showInfoSnackBar(getResources().getString(R.string.cart_not_found));
        }

    }

    public void onEvent(MessageResponse messageResponse) {
        Log.d("||>>bkasid", "||==code="+messageResponse.getResponse_code());
        if (messageResponse.getResponse_code() == 100) {
            // showInfoSnackBar(messageResponse.getData().getMsg());
            gotoNewActivity(OrderReviewActivity.class);
        }
    }

    private PaymentMethodSetRequest getPaymentMethodSetObject(int cartId) {
        String shipping_charge = preferenceService.GetPreferenceValue(PreferenceService.SHIPPING_CHARGE_KEY);
        Log.d("shipping_chargeAA", "==Main=" + shipping_charge);
        PaymentMethodSetRequest paymentMethodSetRequest = new PaymentMethodSetRequest();
        paymentMethodSetRequest.setCartId(cartId);
        paymentMethodSetRequest.setShipping_charge(shipping_charge);
        Log.d("shipping_chargeAA", "==Main=" + paymentMethodSetRequest.getShipping_charge());

        if (paymentMethodGroup.getChildCount() > 0) {
            RadioButton selectedBtn = (RadioButton) findViewById(paymentMethodGroup.getCheckedRadioButtonId());
            PaymentMethod paymentMethod = (PaymentMethod) selectedBtn.getTag();


            Log.d("||>>getcode p", "||==="+ paymentMethod.getCode());
            if (paymentMethod.getCode().equals("free")) {
                paymentMethodSetRequest.setMethod_code("free");
            }
            else  if (paymentMethod.getCode().equals("bkash")) {
                paymentMethodSetRequest.setMethod_code("bkash");
            }
            else {
                paymentMethodSetRequest.setMethod_code(paymentMethod.getCode());
            }
        } else {
            paymentMethodSetRequest.setMethod_code("cashondelivery");
        }

        return paymentMethodSetRequest;
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int resId = group.getId();

        if (resId == R.id.rg_payment_methods) {
            if (hasCreditCard) {
                if (checkedId == creditCardPosition) {
                    RadioButton selectedBtn = (RadioButton) findViewById(checkedId);
                    PaymentMethod paymentMethod = (PaymentMethod) selectedBtn.getTag();

                    ccTypes = (Spinner) linearView.findViewById(R.id.sp_cc_types);
                    ArrayAdapter ccTypesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, paymentMethod.getCcTypeList());
                    ccTypesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    ccTypes.setAdapter(ccTypesAdapter);

                    linearView.setVisibility(View.VISIBLE);
                } else {
                    linearView.setVisibility(View.GONE);
                }
            }
        }
    }
}

package com.bs.ecommerce.kiksha.model;

import java.util.List;

/**
 * Created by BS62 on 5/12/2016.
 */
public class OrderInfo {
    private String order_entity_id;
    private String order_id;
    private String created_at;
    private String total_item_count;
    private String customer_name;
    private String status;
    private String quote_id;
    private String subtotal;
    private String discount_amount;
    private String grand_total;
    private String shipping_amount;
    private String shipping_method;
    private String shipping_description;
    private String country_code;
    private OrderBillingAddress billing_address;
    private OrderShippingAddress shipping_address;
    private List<OrderDetailsItem> items;
    private String payment_method;

    public String getOrder_entity_id() {
        return order_entity_id;
    }

    public void setOrder_entity_id(String order_entity_id) {
        this.order_entity_id = order_entity_id;
    }

    public String getShipping_amount() {
        return shipping_amount;
    }

    public void setShipping_amount(String shipping_amount) {
        this.shipping_amount = shipping_amount;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTotal_item_count() {
        return total_item_count;
    }

    public void setTotal_item_count(String total_item_count) {
        this.total_item_count = total_item_count;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQuote_id() {
        return quote_id;
    }

    public void setQuote_id(String quote_id) {
        this.quote_id = quote_id;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }

    public String getShipping_method() {
        return shipping_method;
    }

    public void setShipping_method(String shipping_method) {
        this.shipping_method = shipping_method;
    }

    public String getShipping_description() {
        return shipping_description;
    }

    public void setShipping_description(String shipping_description) {
        this.shipping_description = shipping_description;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public OrderBillingAddress getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(OrderBillingAddress billing_address) {
        this.billing_address = billing_address;
    }

    public OrderShippingAddress getShipping_address() {
        return shipping_address;
    }

    public void setShipping_address(OrderShippingAddress shipping_address) {
        this.shipping_address = shipping_address;
    }

    public List<OrderDetailsItem> getItems() {
        return items;
    }

    public void setItems(List<OrderDetailsItem> items) {
        this.items = items;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }
}

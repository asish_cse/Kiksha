package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/19/2016.
 */
public class CustomerSetResponse extends BaseResponse {
    private Message data;

    public Message getData() {
        return data;
    }

    public void setData(Message data) {
        this.data = data;
    }
}

package com.bs.ecommerce.kiksha.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.constant.SharedPreferencesHelper;
import com.bs.ecommerce.kiksha.event.CloseDrawer;
import com.bs.ecommerce.kiksha.event.NewActivityTransition;
import com.bs.ecommerce.kiksha.model.Category;
import com.bs.ecommerce.kiksha.ui.activity.ProductListActivityOfferzone;
import com.bs.ecommerce.kiksha.ui.adapter.ParentLevelAdapter;
import com.commonsware.cwac.merge.MergeAdapter;

import de.greenrobot.event.EventBus;
import roboguice.inject.InjectView;

/**
 * Created by BS62 on 5/24/2016.
 */
public class CategoryExpandableFragment extends BaseFragment {
    @InjectView(R.id.exp_lv_category)
    ExpandableListView categoryExpandable;

    MergeAdapter mergeAdapter;
    public static Category categoryList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_category_exp_listview, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        populateCategory();
    }

    private void populateCategory() {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            categoryExpandable.setIndicatorBounds(categoryExpandable.getLeft(), categoryExpandable.getRight());
        } else {
            categoryExpandable.setIndicatorBoundsRelative(categoryExpandable.getLeft(), categoryExpandable.getRight());
        }
        try {
            ParentLevelAdapter parentLevelAdapter = new ParentLevelAdapter(getContext(), categoryList.getChildren());
            categoryExpandable.setAdapter(parentLevelAdapter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    /*
    menu activity calling from here
     */
    public void onEvent(NewActivityTransition newActivityTransition) {
        EventBus.getDefault().post(new CloseDrawer());

        Log.d("=====menu activity===","----");
        String sr = SharedPreferencesHelper.getMenu(getContext());
        Log.d("=====menu activity===","----"+sr);


//        if(sr.equalsIgnoreCase("9")) {
//           // gotoNewActivity(newActivityTransition.activityClass);
//
//            Intent i = new Intent(getContext(), ProductListActivityOfferzone.class);
//            startActivity(i);
//        }
//        else
           gotoNewActivity(newActivityTransition.activityClass);

    }
}

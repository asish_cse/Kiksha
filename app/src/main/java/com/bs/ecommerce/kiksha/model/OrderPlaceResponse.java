package com.bs.ecommerce.kiksha.model;

/**
 * Created by BS62 on 5/30/2016.
 */
public class OrderPlaceResponse extends BaseResponse {
    private OrderPlace data;

    public OrderPlace getData() {
        return data;
    }

    public void setData(OrderPlace data) {
        this.data = data;
    }
}

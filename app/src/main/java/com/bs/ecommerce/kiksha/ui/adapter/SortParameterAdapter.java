package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.model.NameValuePair;

import java.util.List;

/**
 * Created by Ashraful on 5/9/2016.
 */
public class SortParameterAdapter extends BaseAdapter {

    List<NameValuePair>parameterList;
    Context context;

    public SortParameterAdapter( Context context,List<NameValuePair>parameterList)
    {
        this.parameterList=parameterList;
        this.context=context;
    }
    @Override
    public int getCount() {
        return parameterList.size();
    }

    @Override
    public NameValuePair getItem(int position) {
        return parameterList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView textView= (TextView) LayoutInflater.from(context).inflate(R.layout.item_sort,parent,false);

        textView.setText(getItem(position).getName());

        return textView;
    }
}

package com.bs.ecommerce.kiksha.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bs.ecommerce.kiksha.R;
import com.bs.ecommerce.kiksha.ui.adapter.interface_pack.OnItemClickListener;
import com.bs.ecommerce.kiksha.model.PromotionalOffer;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by BS62 on 5/24/2016.
 */
public class PromotionalOfferAdapter extends RecyclerView.Adapter<PromotionalOfferAdapter.OfferViewHolder> {

    List<PromotionalOffer> promotionalOfferList;
    Context context;
    OnItemClickListener mItemClickListener;

    public PromotionalOfferAdapter(Context context, List<PromotionalOffer> promotionalOfferList) {
        this.context = context;
        this.promotionalOfferList = promotionalOfferList;
    }

    @Override
    public OfferViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_promotional_offer, parent, false);
        return new OfferViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OfferViewHolder holder, int position) {
        String offerText = promotionalOfferList.get(position).getTitle();
        String first = offerText.substring(0, 4);
        String next = offerText.substring(4, offerText.length());

        holder.offerText1.setText(first);
        holder.offerText2.setText(next);
        Picasso.with(context)
                .load(promotionalOfferList.get(position).getImage_url())
                .placeholder(R.drawable.placeholder)
                .fit().centerInside()
                .into(holder.offerImage);
    }

    @Override
    public int getItemCount() {
        return promotionalOfferList.size();
    }

    public PromotionalOffer getItem(int position) {
        return promotionalOfferList.get(position);
    }

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public class OfferViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView offerImage;
        TextView offerText1;
        TextView offerText2;

        public OfferViewHolder(View itemView) {
            super(itemView);
            offerText1 = (TextView) itemView.findViewById(R.id.tv_promo_text1);
            offerText2 = (TextView) itemView.findViewById(R.id.tv_promo_text2);
            offerImage = (ImageView) itemView.findViewById(R.id.iv_promo_image);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }


}

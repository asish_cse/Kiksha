package com.bs.ecommerce.kiksha.model;

/**
 * Created by Ashraful on 4/22/2016.
 */
public class UserAuthentication extends RegistrationRequest {

    public String getEntity_id() {
        return entity_id;
    }

    public void setEntity_id(String entity_id) {
        this.entity_id = entity_id;
    }

    private String entity_id;
}
